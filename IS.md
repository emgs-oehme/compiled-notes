---
title: International Studies Tutorial Notes
author: Marty Oehme
toc: yes
bibliography: /home/marty/documents/library/academia/academia.bib
nocite: "@Acharya2014,@Engel2019,@Martell2007,@James2014,@Pieterse2013,@Ong2006,@Frank1980,@Bach2011,@Austin2007,@Arowosegbe2014,@Sidaway2012,@Hee2010,@Sahni2009,@Batabyal_2011,@Alagappa_2009,@Ugc2010,@Kragelund2016,@Tambo2019,@Baumann2017,@Jensen2016,@Naidoo2011,@Lo2016,@Chaturvedi2007,@Agnew2005,@Brenner1999,@Wallerstein1974,@Stiglitz2007,@Castells2006,@Sassen2005"
output:
  pdf_document:
    latex_engine: xelatex
---

# 1 - What is International Studies?

## Tutorial
Dr. Claudia Baumann

?? - can moodle be updated to 19/20?

requirements:
- reaction papers
- optionally instead: book review (bibliography is online, or propose your own to teacher)
- submit by Sunday after Lecture at latest
- should cover content of lecture and readings of the week
- we can send one trial one (the first) - explicitly state with first paper
- 3 reaction papers are required
- points to be observed:
  - needs academic writing standards / citation criteria
  - choose citation system MLA/Chicago/APA (keep it consistent for reaction papers)
- first person is fine (be cautious with we/they though - point out who you mean [i.e. We in class talked about, Us as humans do...])
- guidelines, see [Moodle PDF](https://moodle2.uni-leipzig.de/mod/resource/view.php?id=704476)

### What *makes* International Studies

- *actors* are always the central point of departure
- be careful with *economization* of higher education
  - esp Acharya poses question of *who* produces knowledge (and gives tentative answer of striving for inclusivity)
- be mindful of the author - *their* background, *their* discipline and general expertise

### Acharya's Pluralistic Universalism

- call against classic IR theory universalisms
- as an example, don't try to fit a (generally accepted) universal theory if it doesn't
- perhaps, however, a theory from a generally unnoticed location fits use that one
- grapple with the idea of 'multiple truths'
  - example of German/French co-contributed book on WWII, which took 40 years to craft and come to an agreement

### Definitions of Regions

- a space in which actors are (reciprocally) influenced either by some form of mobility or its restriction
  - other ways of looking are of course religion, language, water-ways,
  - you can *map* the objects of study - which makes them *regional* phenomena
- what is a space then? how *does* it differ from a space?
  - geography creating space
  - flows as creating space
  - diasporas creating space

### Next Week

balance the knowledge production - RSS


## Acharya - Global International Relations (IR) and Regional Worlds

Indian-American IR scholar (afterwards, Global IR)

laments the 'eurocentricity', or east-west dialectic of IR (IS?)
  - uses example of Sahibs and Munshins under British colonial rule: used as teachers, but essentially relegated to act as informants - tools of language (648)
  - paid less and not setting the agenda - instead agenda is impressed by British rule (648)
  - compares this to IR for which non-west is "place for 'cameras', rather than of 'thinkers'" (648, cf. Shea 1997)

- refutes ideas of 'Democratic Peace Theory', and 'Long Peace' during Cold War - as Anglo-centric lenses, marginalizing other regions' conflicts (648)
- calls on IR to leave behind west/non-west dichotomy in its own discipline (i.e. Global Studies) (649)

- what 'global' IR *should* stand for
  - Universality in a sense of non-"monistic universalism" (i.e. not "applying to all") (649)
  - regions are "socially constructed spaces", "dynamic, purposeful" (650)
  - rejecting exceptionalism in all its facets (651)
  - a new "broad conception of agency"  to include the "ideational as well as material" (651)
    "an agent-oriented narrative in Global IR should tell us how actors (state and nonstate), trough their material, ideational, and interaction capabilities, cunstruct, reject, reconstitute, and transform global and reigonal orders." (651)
    examples of forms of agency (652)

### formulating a research agenda

- discovering new patterns, theories, and methods *in challenge* to current stereotypes (652-3)
- take into account / analyze new global distributions of power and world order (653-4)
- analyze the full interconnectedness of "regional worlds" (654-5)
- use the full spectrum of post/inter-disciplinary approaches to substantively engage with subjects (655)
- take note of the entanglement of local/global(/national/regional) levels (655-6)
- analyze the idea of "mutual learning among cultures", i.e. reciprocal processes (656)

### References

Amitav Acharya (2014) 'Global international relations (ir) and regional worlds. a new agenda for international studies', *International Studies Quarterly* 58, 647--659

## Engel - International Studies

### Overview of history of IS

- IS developed ~1950s as response to institutionalization of International Relations
  - International Studies Association developed as opposition to idea culture of IR (1)
  - explicitly interdisciplinary approach of international, cross-national, transnational phenomena (2)
  - 3 major publications: *International Studies Quarterly*, *International Studies Review*, *International Studies Perspectives*, a number of smaller topical publications (3-4)
- in response to end of Cold War, IS re-oriented itself (5-6)
  - real world changes reflected in focus on multi-lingual, mobility, expanded world areas
  - grapple with the technological advances etc, increasing interconnectedness
  - creation of Global Studies as response

### Epistemological innovations regarding processes of globalization

- new approaches: (6-8)
  - historicization
  - critical area studies
  - 'critical' (new) geography
- but also eye on
  - global culture, ideology, religion

### Leipzig University strand of IS

- methods esp based in (9)
  - reciprocal comparison
  - historicity
  - reflexivity
- "at Leipzig, an attempt is being made to develop an understanding of the field hat is based on th eimplications of post-colonial thinking, critical area studies and new political geography. Combined with a sound training in Global History it adds up to Global Studies." (10)

# References

Engel, Ulf (2019) 'International Studies', 45--60

# 2 - What is Global Studies?

??:

- 1 (Martell) What is meant when first wave globalists see globalization as causal but we heard a lot of times now that actors are the main point with their specific mechanisms (is that not causal)
* 2 (Martell) Why does re-territorialization not go beyond criticism? It can happen *apart* from the national economy (that the skeptical position elucidates)
* 3 (Jones) Are the various theories on globalization really arriving / have arrived at a consensus on its 'newness', and on its overall historical development? (i.e. the starting point of the global condition)

## Tutorial

* The way of ordering globalization research in *waves* has mainly stuck, hyper-globalists/skeptics/transformationalists
* another way to structure it, find key authors and follow their strain of ideas
* a third way to structure is of course, find a concept and follow it through (locations/spaces/fields of study)
* or go through a single univserity's strain of ideas
* try to know the big players on the market :)

## group work for Pieterse:

* when did globalization start? (what is the beginning/origin of globalization)
  * pieterse makes a point that global data existed before strict academic interests
  * then, processes of globalization were studied in specific disciplines with their specific displinary legacies (or baggage)
  * Global studies then tries to go beyond these single discipline approach but, at least in 2013, when the article was written he is still critical of what exists
* discuss the actual theory / how is the concept of globalization dealt with?
  * in global studies, globalization *becomes* the object of analysis to leave behind disciplinary approaches
  * to approach this object we need multicentrism but multicentrism itself only provides more centers
  * multilevel thinking
    * different interaction scales micro/meso/macro
    * across class and status
  * overcome disciplines by actually focusing on integrating the individual studies' perspectives without bringing their disciplinary focus
* position the author - who is he?
  * from netherlands, legacy in cultural studies - moving into global studies
  * written for santa barbara, one of the birth-places
  * affix him right on the cusp of Martell's Third wave

## Held, McGrew - Globalization / Anti-Globalizaiton

### Guiding Questions

1. How do the authors define globalization?
2. How do they demarcate Anti/Globalization, if they do?
3. What is the purpose of the book, in regards to this 'divide', as they call it?
4. Who are the important actors / action / outcomes in their definition of globalization (or globality)?
5. Does the book provide, in hindsight, a good vector of discussion regarding globalization (or sociological discussion in general)?
6. Is populism (esp in its nationalist vein) already a point of discussion for the book?

1. Globalization is a process along 4 axes:

  - stretching of (soci/pol/econ) activies across political frontiers (increased *reach*)
  - intensification of connection - infusing almost every sphere of social existence (increased *magnitude*)
  - accelerated rapidity with which news, goods, information, capital, technology interact across borders (increased *pace*)
  - local and global interact much more closely with one another and can have profound consequences on each other (increased *enmeshment*)

  essentially: "a process of time-space compression",

  certain senses of (relative!) deterritorialization but not necessarily making a statement about "growing interdependence between [...] national states, or internationalization"

2. 'Anti'-globalization arguments happen within a space of 2 axes: Globalists/Skeptics (arguing about the intellectual hegemony of the *concept* of globalization) and Cosmopolitans/Communitarians (arguing about the ethical grounds of the *implementation* of globalization)

    there are three major streams sceptics argue along it being unfit as:
    - "as a description of social reality"
    - as *explanation* of social change
    - and as ideology of social progress

    (argued as inversion of explanans and explanandum - epiphenomenal??)

3. The divide roughly "relates to the evidence and interpretation of actual trends in world inequality, its sources, consequences and remedies." (125) For an example (made by Galbraith, Wade, Davies, among others - cf. 130-33) see my questions down below.

4. The overall resulting outcome: our current global condition. Interpreting the condition is where actors and actions differ.

---

Question:

- The absolute gap between richest and poorest globally is widening. It is argued that the *relative* income gap should thus be used instead. This hinges on the premise that the absolute difference between poorest and richest *skews* the results and should be smoothed / disregarded to grant a more complete picture. But all it does is obscure the *actual* dispersion of income at a smaller level! Regionally, the wealth of what constitutes one *country* might still be concentrated on 3 people and the average income would not be affected. All that is 'lost', or at least obscured (intentionally) as the relative income per country is used as a base for comparison.
- The comparison of a 'global' reduction in poverty equally obscures newly aligning concentrations of poverty. If globally poverty can be reduced to 20% that is good, but does not say where those 20% are located. If over time all 20% living in poverty are situated in Africa (and increasingly, they are), then that is not necessarily the progress in avoiding global poverty a simple number of global reduction seems to speak to.

## Martell - The Third Wave in Globalization Theory

(2007) a review of the discipline and its representatives

argues that:

second and third wave globalizatton researchers have not been able to decide on whether they are. This shatters the assumption that a clear-cut third wave is occuring globally
* is also mentions that transformational (3rd) wave, and skecptical wave (2nd wave) are situated much closer together on the spectrum of globalization's effects, just interpret them differently
  > "third-wave theories reinforce the skepticism they seek to undermine." (178)

historic development of global. waves:

* gives run-down of first wave: (globalists / economic globalization)
  * Globalization is causal ??1
  * leads to homogenization
  * new phenomenon
  * natural unstoppable future of globalization
* second wave as follow-up: (skeptics) - Hirst and Thompson
  * globalization as pure discourse
  * need to be verified empirically
  * leads to clash of culture, re-nationalization
  * another form of (old) inter-nationalization
* begins close comparison with third wave: (transformationalist) - Held, Hay and Marsh
  * sees global transformation but next to processes of differentiation and embeddedness
  * reconstruction of nation-states
  * attempts to differentiate present new forms from older forms of globalization (or connect them)

### Claims by 2nd wave

* internationalization of financial markets, technologies, some manufacturing and services (180)
  * make radical national-level policies hard due to possibility of capital/business mobility (180)
* internationalized economy is not new: more open 1870-1914 (180)
* trade & investment is made within existing sturctures rather than new global structures (180)
* companies are MNCs rather than TNCs (transnational) - no tendency toward global companies (181)

### Claims by 3rd wave, that attach on-to 2nd

* highlights reversibility and embeddedness in other processes (or historic repetition)
* a "tendency with counter-tendencies" (184)
* analysis as heterogenity of globalization, rather than homogenous process
* emphasize the connection of culture and economy (as do Hirst&Thompson, but only mentioning it, without actual analysis) (184)

### Martell analysis

* "The emphasis on de-territorialization and disembeddedness goes beyond the skeptical position but rootedness and re-territorialization do not." (188) ??2
* defends skeptics by saying globalization is not viewed as natural/irreversible - but nation-states have the power to affect it / alter its course: but that is once again only attaching itself to the national level (189)
* always remains on the national level: 191, 190,
* brings forward Scholte who uses term of de-territorialization not as homogenous global relation, but 'supraterritoriality' (which, even if transcending territory, still contains the link to *a* territory)

## Jones - Globalization Key Thinkers

(article written as a structuring of the written literature, that's why he adds the different couplets, to sort and organize ideas)

4 'philosophical couplets':

* time/space
* territory/scale
* system/structure
* process/agency

- their dialectics tend to influence globalization theory

### Overlap in globalization theories
??3

* historical development of globalization
* 'newness' of contemporary globalization
* impact on political units (e.g. nation-states)

### Differences in theories

* homogenous nature of globalization
* value judgement of globalization (if taken as homogenous)
* primary actors/mechanisms (technology/politics/institutions/culture)

## James, Steger - A Genealogy of Globalization

Attempting to trace the word's genealogy to better understand the birth, journey ('carrer'), displinary, and perhaps even social reality of the concept. (420-421)

* rise of globalization as a term is simultaneous with the rise of academic careers built around it (421)
* interestingly, the neologism could hardly be attached to a name, since "academics often struck upon 'globalization' from very different starting points." (421)
* authors interviewed key figures for the term 'globalization' to understand "hermeneutical concern with how people narrate globalization", as "key figures of ideological contestation" (424)

* the symbolic nature / power of the global was already in existence long before globalization was conceptualized (422)

### Levels of meaning formation (423)

* Ideas - elements of thoughts and beliefs
* Ideologies - patterned clusters of normatively imbued ideas&beliefs
* TODO: Imaginaries - patterned 'convocations' (respective group's imaginations) of these ideologies
* Ontologies - more general patterns of meaning formation (not necessarily restricted to single communities; e.g. modern territories through nation-states, or linear time)

* argument is that these patterns were all receptive to understandings of the concept of 'globalization', that's why it became dominant (423)
* it infused as a simple concept, as a political agenda / ideology, as a material process (424)
  * even managed to generate newly attached ideologies (market globalism, justice globalism, ...) (424)

### History of its formation

* 30s+: employed as learning term: 'going from global to particular', global as the universal (425-426)
  * ex of film-studios employing globes but all globes were centered around one area in US (hollywood)
* 1951: Meadows uses concept in modern sense; and connects it to dialectic of 'localization'/'universalization' (429)
* 80s+: later employed in relation to 'modernization of the global world' (431)
* 80s+: very few of those interviewed actually remember first usage

* in France e.g. 'mondialisation' is roughly equal to globalization, but viewed from the outside it is seen  as a *different* concept

## Pieterse - What is Global Studies?

### Sections

* Global Knowledge as a database *independent* from studies of globalization
* studies of globalization
* global studies (as it actually exists)
* programmatic account of global studies
* cognitive problems of global thinking
  * multicentric
  * multilevel

### Global Knowledge as a database *independent* from studies of globalization

3 levels: (501)
* global data (wide, fragmented, impressionistic, anecdotal, journalistic, for institutions, nations)
* globalization studies ("influenced by discipline demarcations and theories")
* global studies ("integration of the above, and *potentially* an approach in its own right", emph mine)

### Studies of Globalization

* "overarching frameworks emerge but the discrepancies between how social science and humanities disciplines view globalization remain distinct and in some respects glaring. There is no consensus on the definition of globalization, its effects, and its periodization." (503)

* wide-ranging differences depending on discipline (501-02)
* and little cross-pollination *between* the disciplines, even when designed interdisplinarily (502)

### Global Studies

* studies as being able to introduce theories, methodologies that are new bc unconstrained by disciplinary canon (504)
  * present more integrated konwledge than singular disciplines - they don't tend to fracture like disciplines (504)
* actual direction and 'interdisplinarity' of global studies programmes largely dependent on faculty ancestry (504)
* "[global studies] are a pragmatic local improvisation rather than an analytically or theoretically honed project [which]", that still has to be defined programmatically (504)
* global studies (attempts) refers to studies with a wider perspective & conceptualization - moving beyond the national scope (505)
  * by moving beyond the national, previously "marginal phenomena that may help or hinder national projects" become the central objects shaping the global (506)
* thereby awareness of global phenomena is "the demand side of global knowledge, global studies represent the supply side." (507)

### Multicentrism

* there is a need to "'globalize global studies'", to decolonize the imagination of globalization itself (508)
* this thinking isn't new (see cosmopolitan thinking from Stoics, Muslim thinkers, Renaissance humanists, Kant), but scope and intensity of awareness is new (508)
* "multiplying centers [...] doesn't overcome centrism", potentially ignores actors within the centers' radii (509)

### Multilevel

2 meanings: (509)

* viewing global relations at multiple interaction scales: micro/meso/macro
  * globalization from above, from below *and* from the middle (509)
* viewing global relations "across the spectrum of class and status"
* appreciate the complexity of discourse
  * distinguish levels and spheres of discourse (and explicate them)
  * so that "global reflection and local resonance should go together", to "act global think local" (511)
* tools for that:
  * critical reflexivity
  * awareness of complexity
  * interdisciplinary synthesis (e.g. anthropology/geography)
  * thinking plurally (modernities/capitalisms)

# 3 - Economic Globalization

## Lecture

* idea of free markets as just of a 'pure' ideology as other constructed notions
* Wallerstein, Castells, Stiglitz - for each one 'globalization' is a single process

(32) Wallserstein's answers
  * no clear answer on what designates periphery/semi-
  * where does the designation & transformation happen? only structural snapshot

(33) both the education system and political rhetoric still owe a lot to core/periphery thinking starting from Wallerstein

Castells

(39) ?? existing order being challenged because US spent too much money in vietnam?
  * Engels connects this to the stagflation & OPEC crisis - how?
(42) ?? how does labor and capital connect? is not not what is being steered and who steers it? (through newly decentralized networks?)
(48) Stiglitz essentially praising (?, analyzing positively), the (statist) protectionist measures of (esp East) Asian countries in allowing self-managed market liberalization to take place
(51) !!
(55) graph still being based *solely* on economic measurements (and GDP at that); what happens with nation-internal differences and non-median distributed wages/incomes/productivity
(59) the country selection itself becomes the method of demonstration - in other words objects of analysis becoming reinforcing bias
(61) the *distribution* of income, the labor that is not wage based or stable (i.e. Informal work) - connect to (64)
(66-67) *how* is gdp collected? (additionally, do the official channels of distribution explain their collection process?)
(72) aggregation happens on the basis (since this is existing epistemology) of nations, and does not display measuring inacurracies or missing statistical information
(76) even this measurement still omits very specific information (again, swaying the lens obscures others)

## Tutorial

Castells text:

* QUESTION: where do they start with their story of capitalism / globalization? Is globalization capitalism?
  * starting from industrialized nations mainly, which creates an uneven power dynamic and terrain of globalized conditions
    * important to remember that this network society as a progress is an exclusionary process as much as it connects the world

* QUESTION: what is the main argument in connection to the text? (3-4 points)
  * main point: what delineates our society from historically similar seeming ones is not knowledge/information but that we have it in a networked way
  * a network society for him is "social structure, where networks are operated by information & communication technology; individual nodes are self-directing within the network (nodes being e.g. people)"
  * 3 network configurations:
    * network economy: notion of self-programmable and directed labor
    * networked individualism (as social pillar): hyper-connected horizontal social relationships
    * network state: because networks don't adhere to nation-states, network state is not centered around nations and instead strives towards global governance without global government
      * traditional political sovereignty is undermined and reconfigured in supra/international organizations (Stiglitz)
  * network society is completely driven by technology in constant struggle/cooperation with capitalist agendas
  * this means a more decentralized agency; but this process needs applicable education, access to technology and underlying infrastructure to participate and not be left behind and become a 'non-entity'

## Wallerstein 1974 - Rise and Future Demise of World Capitalist System

* problem of many 'grand' (encompassing) theories: reification of specific units of analysis onto the whole to paint a more general (but too abstracted picture) (388-90)
  * further problem: that of putting the reified units into several specific stages against which to measure and analyze
  * stages themselves are not the problem - but they should be stages of social systems, "that is, of totatlities": either mini-systems or world-systems (390)
* instead proposes that in 19th-20th cent only capitalist world-economy has existed as world-system (390)
  * mini systems: "an entity that has within it a complete division of labor and a single cultural framework" (390)
  * mini systems nowadays defunct due to increased interconnectedness of economies
  * world-system: "entity with a single division of labor and multiple cultural systems" (390)
    * either with common political system: world-empires
    * or without: world-economies

areas investigated:

* Latin America, with Frank v Lacau feudalism debate
* Mao Tse-Tung, Liu Shao-Chi 60s debate of Chinese People's Republic socialist state

### The Rise of Capitalism as debate

* from 16th cent onward, the predominance of market trade designated the capitalist system (391)
* the 'persistence of feudal forms' as a non-problem
  * Gunder Frank argues that it's the result of being a periphery nation in capitalist system itself leads to it (392)
  * however, when shifting unit of analysis from states to 'European world-economy', this dichotomy disappears (394)
  * Mao Tse-Tung, Liu Shao-Chi 60s debate of Chinese People's Republic socialist state, which also devolved into a discussion of 'stages' of socialism/communism (394-95)
    * raised question of nation-states as units of analysis (uoa)
  * Mao arguing for socialist society as process, not structure - again not using nation-states as uoa (397)(397)

### The three world-economy stages

* 1 establishment of world-economy at the latest 1557 after failed attempt to recuperate it into empire under Habsburg rule (407)
  * all states were subsequently quick to bureaucratize, raise standing army, homogenize culture, diversify economic activities (407)
  * by 1640 northwest Europe established itself as core-states
* 2 system-wide recession of 1650-1730 consolidated European world-economy, opened stage for world-economy (407)
* 3 first industrial capitalism, now comprises large % of world production (and surplus) (408)
* 4 after WWI further consolidation of world-economy - even under revolutions since those participating all participated in the *same* system (over short or long period, like USSR)

### Building a new Framework of Analysis

* defining a division of labor
  * substantially interdependent grid
  * economic actors operating on assumption that essential needs are met by own productive activities and exchange
  * smallest of such grids one division of labor
  * they can exchange goods they deem not valuable with other systems in a 'very limited' way (397-98)
    * since exchange of surplus-value is zero-sum game, only one system can obtain maximum profit (398)
  * in the capitalist system labor power is sold to the landlord/capitalist/owners of the means of production
    * in this reading of it, slavery is not an anomaly but one feature of it (400)
    * esp in era of 'agricultural capitalism', wage-labor was only one mode of labor recompensation with (slavery, forced production, share-cropping, tenancy other modes) (400)

### World Positions in this system

* 3 structural positions in world-economy, stabilized by ~1640
  * core
  * periphery
  * semi-periphery
* "capital has never allowed its aspirations to be determined by national boundaries in a capitalist world-economy" (402)

* if both redistributive economy of world-empire and world-economy with capitalist markets distribute their rewards unequally, why have they persisted and not been brought down? (404)
* 3 factors retain relative political stability (in terms of systematic survival) (404)
  * concentration of military strength in hand of dominant forces
  * ideological commitment to the system overall (that by the staff and cadres of the system, who feel their survival is wrapped up with that of the system)
  * the division of the majority into a larger lower stratum and a smaller middle stratum
    * in 'world-systems' this turns into the three kinds of state (core/periph/semi-p)

* in other words, semi-perihpery states exist for their *political* purpose of stabilizing the system primarily (405)
  * upper stratum is not faced with opposition by all others *unified*
  * middle stratum is both exploited and exploiter

* this should not necessarily be understood as nation-states being the unit of analysis (405)
  * uses 'ethno-states', or status-groups as utilities of analysis which do *not* necessarily move along nation-states but within world-economy on the whole (405)

* debates about socialist sttates, different world systems become moot when they all participate in the *same* capitalist world-economy system (i.e. there are no remaining feudal systems, just as there are no remaining socialist systems)


[@Wallerstein1974]

## Castells 2005 - The Network Society: From Knowledge to Policy

* main point: What delineates our society from that of other historically existing ones is *not* information/knowledge; instead it is *networks* (4)
* networks traditionally had an advantage and a problem (4)
  * adv: most adaptable, flexible organizational form
  * but: could not master&coordinate resources needed to accomplish given tasks or projects beyond certain size and complexity
* this is why historically networks were part of private life and vertical organizations dominated production, power, military ( with central authority) (4)
  * this mirrors industrial transf. being powered by power (energy) networks (4)
* 3 key configurations:
  * network economy
  * networked individualism
  * network state

### Societal Transformations

* Network Economy
  * new form of organization of production, distribution, management leading to substantial productivity increases (8)
  * while firm is legal and capital accumulation unit, operational unit is business network (9)
  * economy moving from "organization man" to "flexible woman" (9)
  * notion of 'self-programmable labor', someone who essentially acts as a self-directing and autonomous worker (or node) in the network - and due to his productivity gains gets bargaining power (10)
* Transformation of sociability (11)
  * new hypersocial society based on "networked individualism" creates individualism as dominant cultural strain (11-12)
  * "realm of communication, including the media" is transformed (12)
    * customized & fragmented multimedia system where business conglomerates work together and engage in competition at the same time (12-13)
    * communication *and* media business conglomerates operate globally and locally at the same time (12)
  * again, a move away from central organization to absorb more share of social communication (13)
  * emergence of horizontal communication networks allow "self-directed mass communication" (13)
  * ?? "In the network society, virtuality is the foundation of reality through the new forms of socialized communication." (14)
* Political competition
  * built around political leaders since television still dominates and based on images, simplest being persons (14)
  * around that person trust and character are constructed - so "character assassination becomes the political weapon of choice" (through diffusion, fabrication, manipulation of damaging information) (14)
  * media politics & image politics combined thus lead to scandal politics (14)

### Rise of a new state

* since network society does not primarily operate in national contexts it instead engages in global governance - just without global government (15)
  * in other words: "actual system of governance in our world is not centered around the nation-state, although nation-states are not disappearing by any means." (15)
* global governance becomes functional need, so "nation-states are finding ways to co-manage the global processes [by sharing] sovereignty while still proudly branding their flags." (15)
  * i.e. they form 'networks of nation-states', like the EU - ?? but does his definition of network not imply horizontal relationships without true power dynamics? or can power dynamics play a role?
* international/supranational organizations spring up (IMF, World Bank, G8, ..); also connect to regional and local governments and actors (e.g. NGOs) - this connects local and global (15)
* structural contradiction of global system and national state results in "reconfigurations in a variable geopolitical geometry" - the network state (15)

### Policy Issues

* key question from policy standpoint: "how to maximize the chances for fulfilling the collective and individual projects that express social needs and values under the new structural conditions" (16)
  * to do so it does not suffice to improve technology or technological access (see Lisbon Agenda) (16)
  * but synergy between technology, business, education, culture, spatial restructuring, infrastructure development, organizational change and institutional reform initiatives

* public sector is decisive actor in shaping network society (17)
  * to transform it an interactive, multilayered networking must become its organizational form (17)
* education system needs to be reformed with new forms of technology/pedagogy, content, organization (18)
  * education to engage learning along life cycle, and self-application of this principle (18)
* Global development can no longer thrive on charity and providing for needs out of social exclusion (19)
  * the more the network economy/society becomes efficient, the more it leaves a marginalized proportion of population behind, and the more difficult it becomes to catch up (19)
  * to counteract this exclusionary process the new model of development needs to becom: technology, infrastructure, education, diffusion and management of knowledge (19)
* (intellectual) property rights are a contradiction to the processes of creativity necessary in network society (19)
  * innovation capture by intellectually conservative business world may stall potential innovation (19)

### Emerging Dilemmas

* Rentier Capitalism v Creativity (20)
  * exemplified in (ex-)Microsoft (or Apple)'s rent extraction of means of production monopoly position
  * versus the incredible rise of open source proliferation while still working in the confines of capitalism
  * accepting this debate of redefined property rights questions the legitimacy of capitalism itself (20)
* Political Control v Communication Democracy (20)
  * emergence of self-organization bypasses mass media and challenges formal politics
  * this instigates loss of control of information and communication, the root of most power configurations
  * accepting this debate means seeing the need for direct democracy (which has so far never existed)

# Stiglitz 2007 - Globalism's Discontents

* problem of who benefits from 'globalizing' themselves becomes more of who and how that globalization is managed (295)
* that's why self-managed (East Asian) globalization has been so successful for themselves (296)
  * based on export
  * based on their own pace of managing globalization
  * interestingly, they rejected the Washington Concensus - ?? does this play a role?

* IMF as the extension of colonial (paternalist) practices (296)
  * the deregulation and (forced) liberalization of financial and capital markets lead to an economic ravaging by international actors (296-7)
  * esp capital market liberalization increases volatility which increases investor premium demands (298)
  * capital mobility provides a buffer of security for capital, but does nothing for national labor markets (297-98)
* problem being: these institutions (IMF, WorldBank, WTO,..) are an *ad-hoc* system without oversight or (democratic) accountability - "global governance without global government" (300)
  * on the absence of clear accountability (or firm established evidences), the actions are undertaken through *ideology* motivating them (300)
* the debt dichotomy: when industr. nations face slowdown, they allow themselves to adopt expansionary fiscal/monetary policies and accrue deficits; when developing nations do this becomes impermissible - highlighting the global inequality in discourse (303)
* serious disconnects between rhetoric and reality are coming to be felt everywhere - *this* is what creates a feeling of disconnect, delegitimized elite governing and ultimately discontent (304)

* *biopiracy*: when international drug companies petent traditional medicines, (with resources and knowledge that rightfully belongs to developing countries) and local producers can not muster the capital to challenge these (largely perhaps unwinnable) claims (302)
* offshore banking centers (which were suspected of funding terrorist activities) would not be shut down after the call for transparency (*by* the IMF) turned *to* the IMF (302)
  * they could be forced to comply with international standards, but serve 'core' countries' interests well enough for the moment to be allowed their existence (Stiglitz: "the interests of the financial community and the wealthy") (302)

* in new publications he then goes on to new configurations of institutions and states/regions being affected by them
  * e.g. African Union and Development organizations against/for the coming efforts at integration into world economy


[Resentment in Politics](20190507095713 Resentment in Politics.md)

[@Stiglitz2007]

# 4 - Portals of Globalization

## The Global City

* network society is not enough to explain why services *still* concentrate in cities
* cities in turn should be viewed *unbundled* from their respective national contexts (at least to a degree)
* the cities begin using the national infrastructure for their own connections with specific other cities (aside from national wants)
  * this enables different stratifications of migratory movements
* Sassen's book written in very American context of strong local government and mayors
  * does not necessarily apply to worldwide cities, e.g. Mumbai not really having an acting mayor of tax oversight at city level, instead more nationally situated

* the concept of a global city has really gone beyond Sassen and the scholarly world
  * increasingly used by think-tanks and cities themselves for PR and 'empiric' measurements
* Sassen's idea originally: to reinsert place into global processes, in a critical way (and historicize the development simultanously)

## Special Economic Zones

* small "enclaves for export manufacturing"
  * enclosed by fences/walls usually with multiple factories inside
  * not considered domestic areas, creating tax-free (or reduced) zone
  * sometimes these zones are also exempt from *laws and restrictions*, e.g. labor (safety) laws, environmental laws, etc. ()
* a variety of terms and types to signify (the attempt at) standardized 'trade' zones which remove restrictions to the movement of goods/capital/people, i.e. Resources
* expedite movement of goods especially, e.g. ports creating *massive* flows of trade (which are steadily shipping around the world)
  * places in these zones are the actual locations where global processes can become tangible
  * even 'duty free' areas in airports are essentially SEZ
* e.g. ILO Report (3500 zones, 66mil workers, 130countries - 2007 report)
* (Fröbel, Heinrichs, Kreye) - manufacturing is now being shifted to the former colonies' locations (efficiently enabled by new technological abilities)
  * this leads to a new extraction of profit/capital out of these locations
  * new manufacturing processes allow splitting into much smaller units of production, which can be shifted globally more efficiently
  * this leads to underpayment in developing countries (incredibly low wages for workers in SEZ, especially since most are women), and simultaneously underemployment in industrial countries
  * first real global survey of impact of SEZ on "fabric of global economy and labor" (Maruschke)
* carries some similarities to Global cities concept
  * part of neoliberal withdrawal of state sovereignty
  * increased emphasis on multinational corporations
* often critiqued as a neocolonial process with former colonizer "re-colonizing" independent countries with the help of these zones
  * especially with the dwindling regulatory oversight since 1970s
* idea of comparing them to Italy's free trade ports of the past

* TODO: look at Aiwha ong - Neoliberalism as exception: looking at the new inequalities under the neoliberal system *within* Asian countries (and not just in relationship to western powers, but between asian citizens etc)

## Related

[Global History - Defining Portals of Globalization](1911011333 GHS Portals of Globalization.md)
[Reaction Paper - Special Economic Zones](1911151417 IS Reaction Paper - Special Economic Zones.md)

## Frank 1980 - Third World Manufacturing Export Production

### The Changing Export landscape of third world countries

* the creation of 'free production' or 'export promotion' zones in satellite' countries (erstwhile colonial peripheries)
  * still import industrial goods (raw materials, semi-processed goods, equipment for producing exports), often foodstuffs to feed part of the labor force
  * are beginning to export industrial goods to industrial countries
  * sometimes as extension from existing import substitution efforts
  * but most often "from what is literally a tabula rasa", in that they supply a flat piece of land with infrastructure, factories, equipment, and necessary materials are 'imported' from afar, and labor is sourced locally. (83)
* exports from Third World countries consist primarily of (85)
  * textiles & clothing, leather & footwear (41%)
  * sporting goods, toys, furniture, wigs, plastic goods (all labor intensive)
  * electronics components manufacture & assembly
  * processing of domestic food, wood, mineral, metal resources
  * heavier industrial & engineering products

### Free Trade Zones for the World Market

* this model is furthered and promoted by the creation of many new free trade zones - Special economic zones in today's language - which are often created with the sole purpose of exporting to 'the world market' (i.e. industrialized nations), and financed by MNCs (86-89)
* the first adopters of this new model (Singapore, Hong-Kong, South Korea & Taiwan) experienced wage rises, which in turn outsourced labor intensive processes to 'newer' areas providing low-cost labor. (89)
  * they have, in some ways, become Wallerstein's semi-periphery states in regards to SEZ and exports

### Why build Industrial Free Zones

* especially promoted by UNIDO (United Nations Industrial Development Organization) (89)
  * part of industrial development programme of country or region
  * measure to solve employment issues (i.e. creation of new labor opportunities)
  * stimulation of export-oriented industries
  * ability to acquire modern industrial techniques from overseas (to enable catch-up)
  * encouraging industrial investments from domestic&foreign capital markets (90)
  * enables concentrated (& rational) development of infrastructure within the zones (90)
* compounded into 4 categories by Frank:
  * "achieve a healthy balance of payments in particular and external balance in general" by increasing exports
  * advance technological developments of economy and "the technical training of the labor force in particular"
  * reduce unemployment and increase employment
  * promote 'self-reliance' and 'independence' in the economy
* sees the results to be contrary to these 4 categories (90)

### Advancing the balance of payments through exports

* infrastructure investment/import costs are high, and often undertaken by foreign firms (90)
* production is highly dependent on imported equipment, raw materials, components, patents, technology, know-how, trade marks, etc (90)
* the internal firm prices to which products are often sold (between branches) do not reflect market prices and advantage "global profit maximization of the firm at the cost of particular host countries." (91)
* exchange earnings from exports are often reduced, especially through 'bonded' imports and exports, which are duty free and thus circumvent national earnings (91)

### Advancing technological progress

* 90% or more rely on unskilled (lowest-wage) labor, primarily women 'trained' in 3 weeks or less (93)
* job turnover between 50-100% within a year (93)
* even de-skilling implied, by replacing small scale manufacturing with industrial standardized Fordist/Taylorist practices (93)
* high-skilled branches of research and development for products, equipment and productive processes is largely withheld from the countries themselves, high-skill personnel is even 'brain-drained' from the country (94)

### Advancing employment

* manufacturing employment can employ people, though largely temporarily (high turnover) (95)
  * the effects are visible in specific city states (Hong Kong, Singapore), but no overall effect visible (95)
* *total* estimated employment by MNCs in Third World countries estimated at 3mil, total unemployed overall at 300mil - so a drop in the bucket (95)
* the pull factor of this manufacturing may be larger than it can support (96)
* the "deformations of the productive, income and demand structure in Third World economies [...] render their economies increasingly unable to provide employment to the population", and transform increasing amounts into a reserve for these SEZ (96)

### Advancing independence

* national economies often have to undertake manifold concessions to foreign firms and buying agencies (97)
* they are, through foreign investors' stringer bargaining power, relinquishing large parts of the possible profits and making their economy dependent on the firms operating in the SEZ at the same time (97)
* especially they are making themselves dependent on foreign (and national) cycles of demand for the final industrial product (97)

### Why then do it?

* for TNCs "the answer is relatively simple: lower costs of production." (101)
* additionally, the lower bargaining power and political concessions to the firms create beside the low wages, good political conditions, or "'investment climate'"
* (according to Fröbel etc al) inexhaustible availability of cheap labor, cheap and efficient means of transport & communication, ability to decompose production process into specific unit-focused part operations (to be done by unskilled labor) (101)

## Sassen 2005 - Global Cities, An Introduction

* what makes a global city?
  * dynamics and processes of global cities that *get territorialized* are global
  * an attempt to emphasize place, infrastructure, and non-expert jobs; they matter because they are often lost from focus in the "neutralization of geography and place made possible by the new technologies" and the discourse they created (31)
  * 'space economy' goes beyond/against the proposed duality of global/national often presumed in global economy (32)
    * dichotomy suggests 2 mutually exclusive spaces, with one beginning where the other ends
    * instead makes evident that global materializes always in specific places & institutional arrangements, many (or most) of which are located in national territories
  * distinguishes between capacity for global transmission/communication and material conditions to enable this (35)
    * even most advanced information industries still have production process at least partly place-bound
  * attempts to address 2 issues:
    * "complex articulation between capital fixity and capital mobility" (35)
    * and "the position of cities in a global economy" (35)

> Global cities around the world are the terrain where a multiplicity of globalization processes assume concrete, localized forms. These localized forms are, in good part, what globalization is about. Recovering place means recovering the multiplicity of presences in this landscape. The large city of today has emerged as a strategic site for a whole range of new types of operations—political, economic, "cultural," subjective. (40)

### Hypotheses of Global Cities (28-30)

* geographic dispersal of economic activities central factor in feeding growth of central corporate functions
* these central functions increase in complexity so that corporations increasingly outsource them to specialized firms (thus, the administrative importance of headquarters diminishes)
* mixing those firms, talents, and expertise creates an information center in some of these urban environments ("being in a city becomes [...] an extremely intense and dense information loop")
* having less centralized administrative burden, leads firms' headquarters to be freer in opting for any location
* reduced role of government in regulation intl economic activity, coupled with more emphasis on global markets and corporate headquarters points to existence of "series of transnational networks of cities" (29)
* the economic fortune of these cities becomes increasingly disconnected from broader hinterlands or national economies
* with the high-level professionals & high profit comes increasing spatial and socio-economic inequality in these cities
* many economic activities which have growing demand in these cities (but no comparable profit) become increasingly informalized

## Ong 2006 - Zoning Technologies in East Asia

* approach wants to think along economic terms, but give primacy to 'state strategies' within it (which are informed by neoliberal logic) (98)
* Chinese state practices "deploy zoning technologies for integrating distinct political entities [...] into an economic axis [and] although zoning technologies are ostensibly about increasing foreign investments and market activities, they create the political spaces and conditions of variegated sovereignty aligned on an axis of trade, industrialization, and knowledge exchange." (98)
  * this demarcates them from transstatal assemblages like the EU, since they are not forged by multilateral negotiations, but "the outcome of the administrative strategies of a single State, China." (98)
  * instead of denying civil rights, these spaces are attempts to create regulatory "spaces of political and economic experimentation." (98)
  * they are, in effect, reterritorializations of the national space - with aims of capitalist growth, but potential for political integration/absorption (99)

### A new sovereignty

* uses new post-container model of thinking about sovereignty not along Weberian idea of national power monopolies, but "as an ever shifting assemblage of planning, operations, and tactics increasingly informed by neoliberal reason to combat neoliberal forces in the world at large." (99)
  * signifies "graduated sovereignty" - by rescaling state power across "differential scales of regulation on diverse groups of citizens and foreigners." (100)
* "the power to call a state of exception to the normalized conditions of the law" (101, from Carl Schmitt)
  * not just negative exception, but positive opportunities and conditions for specific parts of the population (101)
* instead of Sassen's concept of 'giving up' parts of national sovereignty to other institutions, this instead transforms sovereignty along political/economic axes and over new spaces of exception, even cross border (102)

### China's Rezoning

* Economic Zones
  * 4 goals: (104) (which result in political action of rezoning)
    * attract & utilize foreign capital
    * forge joint ventures between mainlanders and foreigners
    * boost production of wholly exported goods
    * let market conditions dictate economic activity
  * they work as 'channels' between different economies, but also different sovereign political actors and formations (e.g. socialist & capitalist spaces) (107)
* Political Zones (SARs)
  * allow experimentation of different degrees of openness & sovereignty granted to the zones leadership (110)
  * both take place under the unifying market that is another (overlapping) space connecting the zones with mainland China - strengthening its sovereignty (110)
* these zones (esp economic ones) allow a de-facto absorption, even before a 'full' absorption based on political sovereignty took place - "an economic detour leading to broader political integration" (116)
  * for Ong, these are "spaces for experimentation with political freedom and transnational connections" (117-18)

## Bach 2011 - Modernity and the Urban Imagination in Economic Zones

* argues that ultimately the zones will transform state sovereignty and global urban imagination as a cultural phenomenon (98)
* argues for the creation of a Zone (capital Z), defined as "a spatial capital accumulation machine consisting of a designated physical area in which different rules apply to corporations, and by extension workers, than in the rest of a given state." (100)
  * the Zone is also a shift of the socio-spatial formation of late modernity when the export zones "turn from a pragmatic *space* for the production of exports into a *place*, imagined and lived." (99)
  * with the economic crisis 2008 especially, zones did not decrease but intensify their shift from manufacturing to knowledge production, "increasing their transnational urban characteristics" (99)
* "most zones are intended primarily for quick increases in exports, epmloyment, or regional development." (100)
  * they grow out of the history of "capitalism's impetus to maintain strategically ambiguous spaces to enable more fluid circulation of goods, people, and capital than might otherwise be permissible given political constraints of empires and, later, of nation-states." (100) [explicitly mentions colonial garrisons and trade spaces]
* whereas Keynesianism wants to promote overlapping national economic and political space, neoliberal de-regulation aims for a marked separation of the two (101)
* there are now many different manifestations (at least 66) of zones, so it becomes impractical to search for *the* true archetype of a zone; they "differ from one another in industry and role - from squalid sweatshops to hygienic bio-medical campuses, from providing subsistence wages to some of the best jobs in the country." (100, 101)
* 3 major functions: (investment, networks, norms)
  * "attract and channel foreign direct investment to sectors of a country's comparative advantage. The resulting benefits are often increased foreign currency earnings, employment, or regional development (though not necessarily all together)." (102)
  * "the institutionalization of custom-free zones as part of linked corporate networks that create new forms of value chains." (102)
  * "Finally, zones acquire a normative role as a vehicle for national development and a value as an experimental space in which to try out free market reforms [...] or new technologies." (102)

### A typology of Zones

* "one end of this continuum are zones primarily used for low-skill, low wage export processing that, like tent camps, can be set up and moved with minimal effort to follow low wages and tax rates." (102)
  * "nominal increase of exports and investment achieved through export zones is offset by negative economic impacts on the host country" (103)
* "the other end are zones that become literally indistinguishable from other national cities in the host state to the point of either dissolving their zone designation (e.g. Shenzen) or integrating multiple smaller zones into the cityscape (e.g. Dubai)." (102)
  * "advanced developing countries such as South Korea, Taiwan and, in a somewhat different vein, China, embedding their zones in an integrated national industrialization strategy with strong state direction, turning zones into catalysts for export-oriented industrialization." (103)

### the Zone and the Ex-City

* these zones promote neoliberal re-scaling of the economy: "variegated, sovereignty allows not only for greater integration into the world economy, but easier classifications of the population, protection against political unrest, the controlled introduction of reform or repressive measures, and the management of both the 'dangerous' class of workers and the emerging middle-class of new professionals (Ong, 1999). Zones thus become one way for states to regulate the (bio)political as well as the economic." (104)
* "zones become contested sites in the relation between government and capital." (105)
* "highly integrated zones allow developing states to integrate into the global economy while, ideally, pursuing a national development agenda premised on economic liberalization." (105)
* "graduated sovereignty in the form of various Special Economic Zones was thus employed as a state strategy to create spaces where liberalization and deregulation could take place in the face of significant resistance." (105)
* "a hybrid zone/city. The best-known hybrids are perhaps the largest: Navi Mumbai (New Bombay), Dubai, Shenzhen, but there are dozens more in development from Clark Special Economic Zone on the site of a former US airbase in the Philippines to Nigeria’s Lekki Free Trade Zone to Iran’s Kish Island. The social and cultural meaning of the rapid spread of zones discussed in the first part of this article exceeds the question of economic development." (106)
* function as an ideal/a-historical city: "To function as a node that connects the local economy to global production chains, zones must privilege transport, efficiency, and mobility (for goods). The physical space, accordingly, is meticulously and generically designed to ¢t various norms of factory, storage, dormitory, and residential space spread along grids of highways and railroads leading to ports or airports. It is in the Zone that the creation city emerges as a true infrastructure city. The intended consumer is literally the producer. The city is propelled by technological innovations that enable integration of the local, the regional and the global via all available means of modern transportation and communication." (107)
  * this is intentional, to assure "interoperability with transnational managers, global architecture firms, and global corporate cultures while emphasizing its exceptional nature. Without history, the Zone is pure possibility." (108)
* Zone becomes location of two tensions: "between the Zone as a transnational, or even ex-national, phenomenon and the Zone as a national showpiece akin to 19th-century world expositions. Second is the tension between the smooth and meticulous appearance of the Zone and the less officially visible migrants and farmers who enable the Zone through labor and land." (109-110)

### Hidden by the Zone

#### Migrants

* "Modular zones that employ mostly low-skill female labor can harbor horrific forced labor camp-like conditions - Naomi Klein's (1999) miniature military states - but city-sized zones that present themselves as paradises contain substantial environmental and social problems." (112)
* "the 'invisible' migrant [people not officially allowed to live in the zone] is a form of public secret in the story of the Zone's construction and service industry, the obverse is the displaced farmer who once tilled the land upon which the Zone was built." (113)
* "The illegal migrants in Shenzhen (illegal because they lack the authorization to live in the city) live in spatial holes created by a quirk of the system - 'urban villages' that had retained rural land use designation when the city first expanded and thus circumvented the laws of the city while located directly within it (Bach, 2010)." (113)
  * "The migrants, in turn, have little protection from the predations of hustlers and police alike. Like illegal migrants everywhere, they are largely reduced to invisible and expendable inhabitants who are nonetheless instrumental in the construction and menial service sector industries." (113)

#### Farmers

* "Former Indian Prime Minister Vishwanath Pratap Singh points out: ‘[T]he virtual confiscation of agricultural land without paying adequate compensation is a central feature of the SEZ promotion. This has created a situation where the government is acting as the muscle man of corporate powers to usurp the land of farmers. The government is virtually forcing the farmers to subsidise industry’ (Ramakrishnan, 2006). This, he warned, ‘is unjust and would act as a trigger for massive social unrest, which may even take the form of armed struggle’." (113-14)
* "The SEZs have become a major rallying point for critics who charge the Indian state with a military-backed attack on the indigenous peoples regions that connect SEZ development with the battle against Maoist rebels." (114)

?? - "a space where network and scalar logics collide (Sassen, 2008, 2009; Crang, 2009)"
?? - "The Zone is a site where, to use Michel-Rolph Trouillot's (2003: 36-37) terms, modernization as the geography of management is inextricably intertwined with modernity as the geography of the imagination." (99)

# 5 - Post-Colonialisms

## Austin 2007 - Reciprocal Comparison and African History

argument:
* "the main requirement for overcoming conceptual Eurocentrism in African history [...] is reciprocal comparison of Africa and other continents -- or, more precisely, of specific areas within Africa with counterparts elsewhere." (abstract)

supporting arguments:

* rational-choice political economy most influential paradigm of economic history - but western assumption of economic working
* best approach to creating 'general' models is to work toward genuine generalization through "properly comparative historical research" (3)
* essentially undertakes a long literature review to support his argument of reciprocal comparison leading to new insights

### Reciprocal Comparison

* definition (Kenneth Pomeranz): "viewing both sides of the comparison as 'deviations' when seen through the expectations of the other, rather than leaving one as always the norm" (3)
  * so, ask why is Europe not China, instead of the usual why is China not Europe; then do the same for Africa with bot and vice versa (10)
* one problem using this method: "familiarity with at least the basics of European history tends to be expected from specialists on non-Western countries, whereas the converse is not the case" , i.e. ignorance (10)
  * to overcome this problem synteses of African scholarship must exist (though having to strike a balancing act between too much generalization and being disabled by local differences) (10-11)

### Uses of Reciprocal Comparison

* lessons for global history of agriculture
  * no strong correlation between agricultural intensification & overall productivity (John Sutton) (14)
  * instead, import of new crops/crop varieties major source of productivity & welfare instead (over long term) (15)
  * African farmers being evidential experts in crop production questions relationship of 'experts' and e.g. development expertise (15)
* understanding relationship of land-abundance vs labor-/capital-abundance (or scarcity) (15)
  * in Europe capital-intensive approach, East Asia labor-intensive approach, Africa mostly land-abundance and scarcity of others - but still quite contrasting results
* highlighting the time-bound nature of our understanding of 'property' in highlighting property-rights importance for economic growth (esp reflecting on slavery & slave trade showing the anachronistic nature) (16)
* can question the assumed opposition of rent-seeking and economic growth (again, predominantly in the colonial slave-trade internationally and internally in stimulating foreign economies) (16)
* questions the global importance of states in economic development in the largely stateless living accommodations of many regions in Africa (peaceably), and how political centralization

### Early Approaches to African (economic) history

* W.A. Lewis' model of "economic development with unlimited supplies of labour" (6)
* together with Hlya Myint, growth of overseas demand providing market for output of hitherto idle productive capacity - standard model throughout 1960s (6)
  * but largely criticized for neglecting state intervention, labor scarcity for production
* Amartya Sen's 'entitlement theory' to understand famines not as failures of aggregate food supply but as people having inadequate rights to food - new in 70s (6)

### Examples of Reciprocal Comparison

* Jack Goody (1971, '76, '82) - proposing that less wide use of plough in sub-Saharan Africa (cmp to Eurasia) lead to smaller agricultural surpluses, traces patterns of state formation and inheritance from this (11)
  * can be questioned on neglecting material wealth, related social differentiation, reliance on extra-subsistence production, market-criticality for status/social goods (11)
* Jane Guyer's account of increased 'marginal gains' through prolonged encounters of African traders and European mercanilists continually re-formulating "a range of scales of values and types of [informal] transactions" (12)

## Arowosegbe 2014 - African studies and the universities in postcolonial Africa

* argument: (244)
  * knowledge carries a duality of understanding -- reflecting -- but also molding contemporary social reality
  * Western epistemology is still conditioning African historiography, thus the academic field remains colonized
  * Ultimately, Africa is not accepted as "living subject of history whose peoples [...] are the true markers of their own destinies and histories." (244)
* supporting arguments:
  * fundamentally asks: who produces knowledge on Africa, and who reads this knowledge; so, is there a difference btw university in Africa and African university (244-245)
  * uses Foucault's concept of knowledge in discourse being power; so as institutionalized knowledge "pathologises the criminal, the mad and the sexually abnormal or erotic; official histories filter, prioritise and select what constitutes acceptable knowledge, while also consciously excluding other interpretations and meaning." (246)
    * the domination of discourse by European categories of knowledge is epistemological violence (246)
  * the *economic* conditions equally influence the production of knowledge (exemplified in results of economic crisis of 1980 on scholarly production esp in Nigeria) (246)
    * distraction from research for knowledge producers, collapse of publishing houses, brain drain (247)
    * 1990: 1.3% funds for research & publication; 1.4% share world publication
    * 2000: 0.8% funds for research & publication; 0.5% share world publication
  * the solution? "only Africans are in an epistemic position enabling them to transcend very old traditions of denigrating and misrepresenting Africa" (247)
    * ?? while I understand what it is saying with this -- making visible the history previously dominated by other epistemologies -- it feels uneasily close to racist ideology in my mind

# 6 - Area Studies

## Lecture

### Defining Area Studies

* of course definition(s) would be more fruitful, but some pillars:
  * *intensive* language study
  * in-depth field research
  * focus on local histories
  * using grounded theory with and against detailed observation
  * conversation that is multi-disciplinary (see Sahni)

## Historicizing Area Studies

* which knowledge orders is area studies connected to / growing out of?
  * the symbol-based cosmological view (maps, topographical representations)
  * classification and hierarchical taxonomies (think Linné's animal kingdom taxonomy, museums)
  * knowledge trees & encyclopedia-'fication'
  * universalizing the world (lexica, such as 'Universallexikon')
  * the question of design v emergence (Wikipedia as crowd-sourced knowledge)
* knowledge of course travels (and with it the power dynamics surrounding it)
  * that means a spatialized knowledge order rose (e.g. building new myths of elsewhere, as escapism)

## Preproduction of these orders

* example of ens - international exchange programs only infused with students from very specific countries/ regions (US, Canada, London, etc) - then having to bring in students from other places in order to provide representation - this is an example of *cutting across* built power structures
* (31) what's missing? - Antarctica e.g.: hotly contested for oil and resources (Baumann) ??

## Seminar

* why choose text, did we like
* what does it say about area studies
* can we have a critical question on the text?

### Sidaway - Geography, globalization, and the problematic of area studies

* reflexivity was missing
* silo approach, no connection to other disciplines

### Sahni - The fallacies and flaws of area studies in India

* 4 fallacies:
  * international studies & area studies gets assumed to be the same
  * area studies should be multi-disciplined, each researcher focusing on his own discipline and then sharing results
  * area studies does not approach object of study on its own terms
  * area studies is too normative due to being used for foreign policy
* 9 problem:
  * little theory used - consciously
  * dominated by political science (no multi-disciplinarity)
  * mostly macro-level analysis
  * little field-work (due to funding mainly)
  * dichotomy of language skills - social science skills
  * no quantitative work
  * event-driven, transient, not long-term focus
  * secondary source focus
  * courses are too narrow in where they aim, and too broad in what they look at
* interesting
  * area studies still having a modern approach to the areas they are looking at; e.g. francophone studies going *beyond* the traditional understanding of France or (ger) Romanistik
* question:
  * when he asks for multi-disciplinarity; is not area studies by itself inter-disciplinary?
* criticism:
  * there's no mention of his methodology / approach
  * does not the insistence on multi-disciplinary work reinforce the schisms between the different disciplines; lead to more silo-based work?

### Hee - Chinese studies in korea: the oral history of senior scholars

* the creation of area studies as a field
* one reason would be the challenge to the dominance of the hegemonic position of existing
* many countries have area studies as a policy recommendation engine;
  * influence politics on the basis of better understanding of these parts of the world
* as an extension of an older traditional discipline (geography) to better understand the specificities of the world
* the existing expertise of older (colonial) nations to then turn it into disciplines

### Discussion

* Chinese approach to studying at 'good' universities - you *have to* study a specific language, depending on universities, which may not have anything to do with
* Similar system in Russia (Moscow International School), language to learn is given via *lottery* (or, I guess, you pay for it)
* Question of systematic integration when starting in the ambassador's career
  * still deeply embedded in the traditional ways of looking at the world
  * reflexivity to develop awareness for changing system from *inside*
  * question of how they are trained / if they are at all for the specific regions their speciality lies in

# 7 - Language and Cultural Institutions in Globalization

# 8 - Higher Education and Globalization

# 9 - Geopolitics and World Order

## Agnew 2005

* sovereignty is neither inherently territorial nor is it exclusively organized on a state-by-state basis
* there is no *de-jure* sovereignty, but rather only *de-facto* sovereignty (contrary to Murphys distinction 1996)
* puts up a model of sovereignty by identifying four "sovereignty regimes":
  * between central state authority (legitimate despotic power)
  * degree of political territoriality (administration of infrastructural power) ??
* despotic power (i.e. state autonomy)
  * "judgment aobut the extent to which a state has acquired and maintains an effective and legitimate apparatus of rule." (445)
  * direct coercion is less effective than historically, popular authority is necessary component (444)
  * but this authority can be shifted to other levels than state: city-region, locality, empire, corporations, social movements, IOs, religious groupings, .. (444)
* infrastructural power (i.e. how territorial is it in practice)
  * "degree to which provision of public goods and operation of markets is heavily state regulated and bounded territorially" (445)
  * the economic and political territories do not overlap as cleanly as before (esp under neoliberal consensus, see Bach 2011)
* give 4 manifestations:
  * strong central authority, consolidated territory: Classic (or Westphalian) e.g. China
  * strong central authority, open state territoriality: Globalist (US) - exuding soft power / market-place society
  * weak central authority, consolidated territory: Integrative (EU)
  * weak central authority, open state territoriality: Imperialist (Middle East, sub-Saharan Africa, Latin America)
* 4 dollar
  * in your territory
  * one for multiple
  * imposing it on others (even unofficial use of the dollar, just individually)
  * using someone else'
* dominant Westphalian model of state sovereignty is inadequate today
  * because ignores hierarchies of states and sources of authority other than states
  * because of mistaken emphasis on the geographical expression of authority (as "sovereignty") as invariably territorial
* reflects instead concept of sovereignty of "Enlightenment and Romantic ideals of popular rule and patriotism" (456)
  * it is a "'truth' that has always hidden more than it reveals" (456)

## Chaturvedi 2007

* world order has
  * an analytical dimension
  * and a normative dimension
* order being conceptualized as governing arrangements
* world as arrangements / understanding of spaces, spatiality, spacings
* military space

## Brenner 1999

* de/re-territorialization

## Connect to real-world

* IS
  - create a consolidated territory out of previously trans-national ideas
  * idea of Chaturvedi text being applied more to differentiate between *intention* of IS (normative state-building) / *implementation* (analyzing how the arrangement)
  * different understanding of *order* by IS (not transnational, but consolidation of religious interpretation and territorial unity)
  * previously understood as trans-*national* unity
  * people that are *not* part of the state going and fighting for the state - completely de-territorialized idea of support
* Catholic Church
  * Globalist institution - existing central authority, but exerting 'soft-power' throughout the world
  * global properties / not accountable to the state, but own rule of law / exercising power *without* a state
* Indian Citizenship - first time religion has been used for citizenship under Indian law
  * every refugee can become Indian citizen
  * *except* for Muslim minorities
  * pushing it through by coercion (i.e. force if necessary) and not persuasion
  * decreasing border porousness through consolidation of citizenry - but also based on religious exclusion
  * ordering the political authority through religious exclusions

* what does *full* state control mean? ??
  * where is its ultimate end - a police state with complete surveillance apparatus?
  * there are always spaces that are not completely controlled by state authority

* the spatial organization of the church still being bound by the central actual *thing* of a church ??

## Related

[Bach 2011 - Modernity and Urban Imagination in Economic Zones](1911151348 Bach 2011 - Modernity and the Urban Imagination in Economic Zones.md)

\pagebreak

# Bibliography
