---
title: Global History Tutorial Notes
author: Marty Oehme
toc: yes
body-flush: false
bibliography: /home/marty/documents/library/academia/academia.bib
nocite: "@Drayton2018,@Geyer1995,@Bayly2002,@Middell2010,@Saunier2009,@Ferguson2006,@Middell2012,@Bloch1953,@Dietze2017,@Goebel2016,@Djagalov2016,@McKeown2007,@Manela2007,@Geggus2010,@Cooper1994,@Hodge2018,@Burton2019,@Parthasarathi2019,@Roy2019,@Beckert2014,@Clavin2015,@Matera2017,@Manjapra2010,@Mazower2009,@Dogliani2017,@Sluga2013,@Sluga2017,@Bauerkaemper2017,@Schwenkel2014,@McKeown2004,@Lee2002,@Lucassen2010,@Labelle2014,@Prestholdt2004,@Brenner1999,@Wallerstein1974,@Bayly2004,@Bentley2012,@Maier2006,@Engel2017,@Sassen2005,@Maruschke2016,@Maruschke2017,@Sinha,@Goebel2015,@Byrne2016,@Thomas2013,@Kalinovsky2013,@Chakrabarty1992,@Comaroff,@Akami2017,@Vries2009,@Pomeranz2000,@Beckert2017,@Bryant2006,@Vries2001,@Babones2018,@Mohapatra2007,@Markovits2000,@Cassis2019,@Engel2015,@Fairhead2012,@Kerr1995,@Mueller2015,@Vleck2009,@Espagne2013,@Ogle2017,@Zwan2014,@Ferguson2002,@Sokol2015,@Schwittay2011,@Burke2011,@Burke2011,@McClellan2015,@Raj2013,@Ogle2013,@Bockman2007,@Adas2004,@Byrne2015,@Kaldor2007,@Evangelista2010,@Heymann2010,@Borowy2019"
output:
  pdf_document:
    latex_engine: xelatex
---

\pagebreak

# 0: Global History Seminar

## Some Notes on these Notes

The following notes are extracted as-is from my reading & seminar notes. They are mostly taken for the texts we read in the tutorials, and additional notes taken there. As I use my own note-taking system and I didn't spend much time on extracting them, some of it will look a bit weird --- sorry.

Most annotations for the texts, whether they are direct citations or paraphrasing of ideas, contain a page number in parentheses at the end of the line in order to quickly cite them. If they don't, either a bullet-point one step above carries it, or they are synthesized and not found in the text itself.

At the end, there is a large bibliography with all texts we used in the module, cited in APA 7th Bibliography style.

## Analytical Concepts

Defining a primary source:

> "a non-interpreted source, in relation to your research question" (Marung)

* Regimes of territoriality (De-/Re-Territorialization)
* Flows and their control (i.e. dialectic of flow)
* Scales of Globalization (micro/meso/macro and their blurring)
* [Portals of Globalization (on horizontal & vertical axis)](1911011333 GHS Portals of Globalization.md)
* Global Cities
* Agent Linkages

## Organizational

- office hours at GESI 3.08: send e-mail beforehand; or at other times (usual office at SFB) ~Tue 15:15-16:15
  for mail, questions etc, just send to both
- Final essay - 4 hours; final paper - written as if for a journal (i.e. GHS audience), try to reign yourself in for a monograph; does not have to be newly published; language can be manyfold (Ger/Eng/Spa/Fr); *can* be written in German etc
  * at the very least hint at understanding the debate around the concepts you use (e.g. capitalism - see session 7 notes)
- good journal sources:
  -  [Connections](https://www.connections.clio-online.net/) (book reviews, conference announcements, field news)
  -  [H-net](https://networks.h-net.org/) (blog posts, articles, news)
  -  [H/soz/kult](https://www.hsozkult.de/) (forum, interactions, blog)
- Text profiles: send in night before; format "Oehme_Bayly_2019-10-15" (my name,author name, date ISO); PDF or doc; send via mail (to both, or instructor per lesson)

## Introductory Lecture - Notes

* introduced the development of the discipline situated between national and global transformations
  * Fukuyama's *End of History* vs Huntington's *Clash of Civilizations*
  * emergence out of individual 'border crossing' moments (e.g. fall of Iron Curtain)
  * *but* still always follows from individual perspectives (see globalization projects below)
    * e.g. rift between global studies and International Relations / post-colonial studies in Africa as imprinting western culture vs acting from local contexts)
  * idea of the global village increasingly in use after unipolar moment
  * these academic ideas then spilled over to businesses, state & civil administration

* central point: *interaction* between societies produces change, not the individual societies themselves (through e.g. stereotypical 'properties' or similar)

* the paradigm (roughly) shifted along:
  * New Political Geography ('Space' as research object)
  * Transregional Studies (moving beyond container-centric approaches)
  * Global History of Sociology ('society' beyond nation-state as research object)
  * Global History itself

"It is normal when finding 'New Realities' that you are an object of dispute."

* (from ~2017) moving beyond globalization as naturalized phenomenon, denaturalize the process itself
  * e.g. bank bailouts proving the continuing importance of nation-states for global processes
  * discussion forming around democracy within/without the state

* Nation-states came into being at the same time that globalization as a phenomenon came to be
  * Middell questions the idea of 'newness' regarding gobalization here (or similar processes, *new* media, etc)

* the context-specific application of globally applied 'projects' makes up large part of Global Studies
  * how do people *manage* globalization, how do they *steer* it, force it, undermine it, ...
  * how are flows influenced by political/economic/cultural action and mechanisms globally
  * which flows *are* encouraged/discouraged, which flows should be? (this obviously invokes the normative debate, how normative/analytical are we in assessment?)

* idea of the schizophrenia of normativity - it *always* accompanies research (since the aim is usually to increase the good of the world)
  * however this is also why GS often eschews grand theories - they always try to predict the future to some degree - in favor of case studies

* the *spatial turn*:
  * space is *constructed* as much as it exists
  * space as territorialization / categorization is seen as made up
  * leads into the search for a 'relevant' spatial framework of our time
  * and growing debate about how to see the world (and an *anxiety* of missing a fitting single conception)

* a demarcating fact between empire & nation-state is the equal rights for everybody* (well, except women, children,...) in states
  * in empires a 'fringe' population always exists (with fewer rights etc)
  * this also gives rise to a 'belonging' to singular nations which directly lead into colonialism
  * an interesting example (as it's based on inversion) is the French Free State which was upheld mainly by French colonies which, after not really belonging to the French nation beforehand, now stated their belonging to the French and thereby 'saved' the French nation - and now had to be thought of as full members of France in the aftermath

# 1: What is Global History

## Lecture

- global history, in its break with "world history" roots tried to distance itself completely from 'old' history
  - but a past *needs* to exist (or rather, be invented) in order to distance itself from it (see newness discussion), stays stuck as historical narrative

### Multiple Histories

- traditionally, in Roman/Greek 'Global' History, it was seen as contemporary and looking at their own times, trying to find a 'balance' (to supernatural narratives) in the ordinary people
- singular theories (i.e. theistic ones) only work as long as no counter evidence (archaeology) exists, and with very limited mobility (travel abroad until one meets a counter-narrative)
  you can not question the source of your narrative to accept it as the only narrative (e.g. bible)
- the recognition of the multiples of history starting after spells the difference between memory (singular correct narrative) and history (multiple possible narratives)
  - discovery of the processes of memory-building now also come into historic discoursive frame
- history had to deal with the same problem of how to rectify the 'other', different from what was accepted narrative - answering these question finally changed historiographical thinking
- Enlightenment now starts debate about existence of 'progress' (and furthermore makes an evaluation of it) (this recognition of progress itself enables political action, revolutions, etc)
  - it also starts comparisons of development - e.g. 'under'-development based on progress
  - finally invites finding superiority in imperial/colonial narratives, or reciprocally in the romanticized notion of the primitive

### Lineage of Global History

- world history: often done by experts on more focused topic, toward the end of their career as a capstone
- universal history: based on grand theories with an aspiration for monistic universalisms (see [Acharya](1910161730 Acharya - Global International Relations and Regional Worlds.md)), history as focused by a single human & *their* experiences
- global history: attemptint to distance itself from others while building on them - focused on inclusive pragmatism, collective knowledge (projects focused around diverse teams)
  "unity of the world created by singular humans & their brains; today it is created by collectives & their mobility" (Michael Geyer)

### The Global Condition

- What is the current global condition?
  - a condition of totality, where even the act of rebellion against status quo of global connection only happens in relation to it
  - an increasing reach/frequency/level of interconnectedness (in regards to mobility, knowledge, economy, culture, social)
- When did we enter it? (different ideas)
  - 1918: beginning of decolonization (beg. of end of empire)
  - 1945: the global demonstration of nuclear capacity
  - 1960s: the beginning global ecological awareness
  - 1990s: unipolar moment & end of cold war fractures (although, of course *not* everywhere)
- interestingly, periods of increased connection (i.e. globalization) were also always periods of increased conflict
- question the 'newness' of global condition:
  - FDI was higher in 1890s
  - a continental connection existed during slavery
  - italian remittances played major role in state construction

### The Boom and Bust cycle of History

Discipline of history follows a boom/bust cycle itself, which often closely aligns with current thinking/ political trends

- Marx shifts thinking from cultural encounters to economic entanglements (introduces 'class')
- 1900s shifting world leads to many historians analysis of esp N-American rise
- inter-war scepticism and subsequent attempt to use GHS to 'pacify' the world
- postwar discussion of integrating decolonization project into GHS (e.g. Toynbee being very normative in trying to 'educate' to prevent war)
- 70s Lyotard's argument of historians only producing perspective from which they start an ideology comes up (wants to attack marxist narrative; kills modernization theory in the process)
  - upcoming post-modern condition stops grand narratives, begins ideas of 'dig where you stand', 'history from below'
  - the cultural turn recognizes structural standing within society is major factor (i.e. identity moves to the center of history, but social origins of inequality still largely obscured)
- 1990s the 'globalization turn'
  - esp in US, the opening of borders necessitates a new 'multi-cultural' classroom - *this* is now addressed in Global History

### The Problem of Scale

- defining Global Studies as the study of large-scale problems invites a follow-up problem: what *are* large scale processes?
  - only those who affect many people?
  - those where the agency lies with many people? many countries?
  - does it automatically disregard the periphery/the unprivileged?
  - defining it the other way round also problematic, since it can disregard single (e.g. national) leaders
- if GS is a look at processes of mobility (in whatever form), then does it automatically exclude the immobile? (esp if we think of glob. as time compression & slowness at the same time)
- conclusion: "a sense of dialectics is lost by many Global Historians" & "Global History is, in a sense, about both global mobility and its control" (Middell)

- examples:
  - William & John McNeill - looking at historical processes since Mesopotamian city states
  - David Christian - beginning his history with Big Bang
  - Wallenstein's world system theory - center/periphery opposition (often lead into teleology); expanded to many-worlds theories to increase explanatory reach

## Tutorial

core: interplay btw flow and control: where is mobility and where is *immobility* ( of people, goods, technologies, ... )
by looking at narratives, always centered around specific *actors* driving *processes* of change and transformation

comparison as a method:
  - problems of creating entities
  - necessary to create a framework of comparison which overlays your own narrative
  - problems of finding scale
reactions:
  - reciprocal comparison - more or less (advanced/democratic/...)
  - look at *both* sides as deviations (Great Divergence debate, Pomerance et al)
  - not necessarily Euro-centrism vs an 'Other' but compare between multiple (traditional) 'Others'

- turning away from comparison, instead look at *entanglements*, look at the *mediators* - 'those who find a problem in their own society who are looking for solutions from outside' (not look at the authenticity, but how solutions are created through the eyes of the actors) (i.e. 'follow the actors')
  - this also allows finding different scales that actor work within (those actors themselves become explanatory figures, not just national scale)
  - finding this again in 'jeue de shelle' Jaque Revel (i.e. entanglement theory of political science); both jumping levels but also being transformed or hindered

next week:
 - 2 texts on concept of space (i.e. methodological framework, esp Middell)
 - Fergusson more applied at a case-study


## Manning - Navigating World History

[@Manning2003]

### Guiding Questions

1. What is "World History" (for Manning?)
2. Why is the sub-title 'historians create a global past'? Is this a normative directive, analytical help, or something different?
3. What are the main themes of the book?

1. "World History" defined as "the story of past connections in the [global] human community" (15). Connections looked at: "internal" route to world history - "expanding the scale of analysis" to help explain patterns of global linkages; and "external" route, subsuming new fields/quantities of information within the discipline. A clear definition is still left open to debate.

2. It traces the formation of world history itself as a mode of analyzing the global developments within the world. It also looks at the external/internal path divide - seeing it as the ultimate/proximate causes of the development of global history. So, historians are actors as much as analysts within the framework.

3. Defining World History currently, and tracing its path along (historiographical) developments.
  - Looking at "historical philosophy" to situate the idea that "historical writing requires documentation of the past *and* an interpretive framework to organize it" (17-18, emphasis mine), where more writing relied on speculative reasoning than empirical observation.
  - The generation of the big ideas within world history - the themes of (change as) "progress", "dominance" (35), as well as "human origins", "evolution", "class conflict", and more (36)
  - Goes over Spengler's, Wells', Toynbee's grand syntheses of history on a world scale, with their approaches and biases (as well as the following Lenin, Polanyi,..) who focused variously on civilization or capitalism as the explanatory axis.
  - McNeill's *The Rise of the West* signals both the last big synthetic overview of world hist. and a turning away from grand syntheses. It both signifies a turn to modern methodologies and signifies how these grand visions both stood on their own but also "created the connective tissue among issues in world history" (54).
  - 1980s was really a period of thematic insights (as opposed to earlier grand syntheses); "thematic monographs explored politics and economics, trading systems, empire, and colonization, with growing attention to areas of the world beyond the North Atlantic." (77), but still mostly expansion in scope not advancement in method (esp teaching)
    - Wallerstein developed political economy approach of 'world-system paradigm', splitting world into interactions between core, semi-periphery, and periphery (62). (later developed into 'Modern World System')
    - Ecological history developed by Crosby, esp with *Ecological Imperialism*
  - since then the study of history has increasingly attempted to make sense of the world by invoking "context in space and time" (117), adding geographical space considerations, then the social sphere, "focusing especially on politics, warfare, commerce, and the rise and fall of states." Finally, the natural world has increasingly entered the world history focus (117) [think e.g. Scott - Against the Grain]

- the difference between theme, topic, subject matter (scholarly discipline):
  "a theme may be distinguished from a topic, which refers more to historical subject matter rather than relationships within it. [...] Thus forced migration is a theme, the Atlantic slave trade is a topic, and demography is a discipline for studying both." (56)

## Hughes-Warrington - Fifty Key Thinkers on History

[@HughesWarrington2000]

### Guiding Questions

1. What is the purpose of the book?
2. What are the selection criteria for her thinkers?
3. Is she trying to establish a 'canon' a-la literature?
4. What the underlying style of book?

1. To present a unified overview of ideas, thinkers, streams of thought and responses. It seeks this 'unified' view (that is probably the wrong word, rather a view with a keen eye on the dichotomies and inherent otherness in such unified views) to promote "exploration of ourselves and others through historiography", and at the same time try to be provocative as much as it is suggestive. (xii-xiii)
2. Those who majorly *influenced* thinking around a specific topic. They don't have to be the genesis for a new narrative, they don't have to be its apex, but they do have to be situated somewhere along the curve between its non-existence and zenith. Above all, they have to be situated at a crossroads of discussion and thought which might happen through them or around them.
3. Not necessarily a canon of 'great' people or ideas whose intent is to be exclusive in its selection, but a canon of impetuses which started, ended or segwayed discussion/narratives in very specific directions. This seems to be what she is getting at in her (preface) analysis of 'situated' historiographic discussion versus more encompassing views, the tension between highlighting difference or ignoring it, and '[talking] for and over the top of people'. (xi)
4. Every 'thinker' gets a short essay where their ideas and the context for their inception is examined, as well as their biography if pertinent. A list of major works and often discussion around them follows.

## Geyer, Bright - World History in a Global Age

[@Geyer1995]

arguments:

- World History was abandoned asa subject due to professionalization (and its promised objectivity)
- "The very act of mapping and thinking the world implicated historians from around the world in a nexus of histories of imperial power" (1036)
- reimagining of the world as history is under way [...]" but "it has little to do with the normative universalism of Enlightenment intellectuals" (1037)

- World history in the global age can *not* simply be an extension of the enyclopaedic nature of previous world historical narratives (akin to a compendium) (1042)
- "World history in a global age [...] is compensatory." (1042) "the multiplicity of the world's pasts matters now more than ever, [...] producing a collage of present histories that is surely not the history of a homogenous global civilization." (1042)

- "There is no universalizing spirit, [...] instead, many very specific, very material and pragmatic practices that await critical reflection and historical study." (1058)
- "there is no particular knowledge to be generalized [, instead] there is general and global knowledge, actually in operation that requires particularization to the local and human scale." (1058)

- with the 20th century there has been a break in how world history is formulated. that does not mean a stop to world history; nor the need for new grand unified strategies - but the entanglement of local and global histories which come together (or oppose each other) to form the current global condition

??:

- one aim of global history is "to facilitate public cultures as the free and equal marketplace of communication among the many voices of different histories and memories" (1059) (mentioning Georg Simmel: in an integrated world we meet more strangers)

examples of world history implementations/attempts: (1038-39)

- William H. McNeill's "creation history"
- Harold Innis "queries [...] into the nature of empire" (1950-51)
- Friedrich Ratzel, Karl Haushofer's "geopolitics" (~1923-42)
- Wallenstein's "world system theory"

## Bayly - Archaic and Modern Globalization in Eurasia and Africa

[@Bayly2002]

guiding question:

- Try to define archaic forms of globalization, "and show how they were subordinated by, but sometimes empowered, the modern forms of globalization which were emerging", while "[avoiding] the rigid teleology of [Wallerstein's 'world empires']" (48)

thesis:

- 'the first age of global imperialism' [...] saw the subordination of older forms of globalization to new [...] ones emerging from Euro-American capitalism and the nation state", which 'cannibalized' archaic forms of globalization. (50)
- "Already [1750-1850], features of proto-globalization based on the supremacy of market-driven, profit-maximizing forces emanating from Euro-American capitalism and the nation states were apparent [which] slowly subordinated [archaic globalization] to these forces, rather than being wiped out by them." (68)

arguments:

- there is a strong delineation between 'archaic' and 'modern' globalization, with James Cook's travels being a strong exemplification of the former
  - 'first contact' was marked by highlighting ethnic and cultural differences, "variety of dress, language and deportment." (47)
- 'globalization' is more useful as a heuristic than strict implementation: it highlights "disruptions in patterns of polities, cultures and economies" which are harder to explain without it as a concept (49)
- world systems theory has problems in early globalization - where there was no clearly defined 'centre' or 'periphery' (50)

- modern globalization underpinned by:
  - nationalism
  - capitalism
  - democracy
  - consumerism
    "Modern 'positional' goods are self-referntial to themselves and to the markets that create demand for them; the charismatic goods of archaic globalization were embedded in ideologies which transcended them. In one sense archaic lords [...] were collectors, rather than consumers." (52)
- archaic globalization:
  - cosmic kingship (god given right by king "to aggregate and order, not to assimilate this God-given variety into one." (51) )
  - universal religion (religious travelers in "periodic pilgrimages had brought into being a large infrastructure of transport" (52))
  - humoural understanding of the body and land (traded goods, "though harbingers of modern capitalism and colonialism [...] also fitted into the biomoral logic", by trading specific goods targeted at the humoural body-religion connection)

- archaic globalization could be understood more as regional linkages than globalization *but* "some truly global linkages however, were established" (54)
  - silver exports across Atlantic & Pacific
  - Portuguese empire overseas (and its proto-capitalism)
  - Indian blue cloth as a status symbol in west Africa

- the consumption value (or cultural imprint) of goods changed over time, with "agents of early capitalism [still] happy to maintain archaic ideologies of consumption [...] at least in the medium term" (57)

the agents of the archaic:

- tribal dynastic migrations and pioneer horsemen
  stopped mainly by "new European-style armies, [even of non-european origin], which could stop tribal cavalry charges dead", new capitalist styled polities could extract more resources and exploit more 'efficiently' which the migrations could not 'keep up with' (59)
- merchant diasporas and sea-borne trading powers
  proto-nationalist control over trade-routes and punitive action against non-European trading powers by the nation states (60-61)
- the land caravan trade
  esp economies of scale and the protected trades over seas (see last point) allowed more capitalist oriented Europ nation-states to out-fortify and -trade the more small scale caravans (though some survived quite late) (62)

- proto-globalization then "subsumed [archaic forms] and put to use as the link between the global and the local." (65)

## Drayton - Discussion: The Futures of Global History

[@Drayton2018]

main point:

- to overcome the stagnation (and identity crisis) of being stuck as a "federation of national and area studies history", global history needs to be reframed along axes which question the prevailing conceptual categorizations (some as fundamental as "'revolution', 'class', 'progress'", even 'empire')
  * in a sense, it must highlight the *dialectic* of flow and control, and not become stuck in one of the two modes

theses:

- "the claim that 'resurgent ethno-nationalism' in some way challenges the premises of global history is odd"
  - "ethno-national resistance [...] were themselves responses to new kinds of global connections"
  - e.g. jihads & fundamentalist uprisings - only exist in connection to new connections
- 'global' history is not a "rejection of the smaller scales of historical experience"
- 'rediscovering' the entanglement of local, national, regional, global, [...] is useful even to the very origins of human society
- languages *other* than english need to be paid attention to - in texts, sources, and active engagement
- david bell's arguments that global history ignores the local are wrong

content:

- global studies is an old genre (ex "Universal History") comprised mainly of comparative/connective approaches
- nationalism and anti-globalism are not exempt from global history - indeed they themselves reconstruct it and the answer to their problems should not be to turn away from global history


### Reply from Bell

- argument is not that ghs ignores local/regional issues - but often frames them as the *results* of global interactions instead of events in their own right

# 2: The Role of Space in Global History

## Tutorial

* what are spatial concepts the authors are working with?
* did the authors historicize the concepts/are they seen as new? did they contextualize?
* how are the concepts useful for writing GHS? What is the meaning of the concepts for GHS?

* historicization
  * always look for the 'political' agenda that a text (may) follow consciously/un-
  * contrast the different sources for their gaps/ implicit biases - which sources do *they* use?

* what makes a space / spatial reference for our purposes?
  * (somewhere) social/economic/.. *interactions* take place between specific *actors* who are acting *in relation* to their spatial surroundings
  * spaces in that sense are made *by* the actors, but can also be impressed upon them from outside processes/actors
    * e.g. university as subjugating itself to a national level's laws etc *but* also being potentially a transnational space (depending on where students, its main actors are from)

## Middell, Naumann - Global History and the Spatial Turn

[@Middell2010]

main argument:

* The spatial dialectic should not be reduced to the national, or even separate national, regional, & international levels. Instead, it should be viewed through portals of globalization, regimes of territorialization, and critical junctures of globalization.

* supporting argumentation:
  * "institutional means and structures have failed as frameworks for managing contemporary problems [...] because they are based on the assumption that political sovereignty is to be organized within a hierarchical order of space [...] along one of these [national, regional, international] spatial levels." (151)     [territorial regimes](#Territorial Regimes)
  * globalization is decentralized and not originating from specific 'centers of globalization' anymore [Decentralization](#Decentralization of Globalization)
  * NGOs, MNCs, and an array of actors other than political elites are contributing to global interactions [Actors](#New Actors)
  * all of these actors and locations underlie "a dialectical process of de- and re-territorialization" (152)

side-effect:

  > "Louis Michel's proposal that politics must intervene on all possible spatial levels at once seems confirmed." (151)

* 2 definitions of globalization: "as an objective situation; and second, as a multitude of political projects to redetermine what is meant by interdependence and sovereignty." (152)

### The Spatial Turn

* from (roughly, depending on discipline) 1970s developed
* recognition of changes in spatial order:
  * world was "becoming more tightly connected through new information technology, conflicts over natural resources, and the consolidation of international regulation" (154-55)
  * "on the other hand, a spatial organization of social exclusion was emerging as a consequence of these new factors." (155)
* traditional notion of space ("a container in which historical change unfolds") could not show dialectic of flows *and* attempts to control them w/ territorialization (155)
  * e.g. flows: migration, capital, goods, ideas
  * e.g. territorialization: nation-states, regions, cities, supranational structures, identity politics, transnational networks
* spatial turn's 4 pillars
  * recognize constructed nature of space
  * acknowledge simultaneity of multiple spatial frameworks
  * acknowledge centrality of historical actors *and* historians when defining spatial orders
  * refuses methodological nationalism, or centrism

### Territorial Regimes

history:

* ordering the world into constituent parts "became codified by the emergence of the 'comparative method' (Kulturvergleich)." (157)
* "the conceptual containment of the world's past in civilizational segments remained an essential framework", and similar essentialism survived well into the late 20th century. (157)
* in these models, and their descendants, "the western European pattern of socio-historical transformation [was seen] as the norm, against which the rest of the world appeared as deformed and imperfect variants." (157)
  * e.g. seeing Asia as singular area - "an essentialized West and an equally essentialized other." (157)

* "By the middle of the nineteenth century, [slowly], the national subjugated all other spatial units." (150)
* "The national is still prevalent today, but the hierarchical relationship between the different spatial references seems to have been dissolved" (150)
* After the Cold-War, "international organizations and transnational regimes gained greater importance [...] but the idea of global governance was soon recognized to be more wishful thinking", than new reality - no comparable vision of new spatial order emerged in its stead. (150)
* key idea: "regime of territoriality and regimes of territorialization"
  * *Effective territories*: "units 'where decision space [...] shared the same boundaries with identity space'" (163, cf. Maier 'Transformations')
  * for analysis - *adequate space*: finding the correct space on which actors attempt to create flows or attempt to restrict and regulate them (rather, entanglements and their control) (165)
* these regimes of territorialization do not imply just and unhindered reorganization - rather they help analyze the origins of conflict and violence during the transition, when "certain types of spatial order [move into the centre of attention]" (166)
  * such a change of regime is e.g. the change from 'archaic' to 'modern' globalization, [see Bayly](#Related)
  * such transformations, when understood as *critical junctures of globalization*, are points where "new spatial relationships are established as a reaction to the effects of globalization [and] alternatives are marginalized thereafter." (169)
  * critical junctures are moments where, within short periods "the processes of change evolved [globally] at the same time and became synchronous." (169)

### New Actors

* "non-governmental organizations -- from the globally to the locally active -- have developed into a factor without which the Millennium Goals could not be achieved, nor international negotiations be successful." (151)
* "multinational corporations have reached sizes that rival those of some smaller national-political economies." (151-52)
  * "Commodity chains create an interdependence that is less visible but often far more effective than political negotiation at the international level." (152)
* "Global interactions are created and remade daily by a multitude of actors -- not just by political elites -- in a diversity of spatial frameworks and organizations." (152)
  * "Immigration, mass tourism, the exploitation chains of organized crime, and general worldwide mobility are likewise creating a new social reality." (152)

### Decentralization of Globalization

* "Globalization is not confined to a few prominent places and no longer develops along clear-cut borders and within stable structures of centre and periphery. [...] More and more, around the world, societies and socially diverse groups are directly exposed to the global assemblage and drawn into a human web of exchange and communication." (152)
* Instead, *Portals of Globalization* are useful foci for study
  * they are places where economic and military dispersion took place, global networks have been created (162)
  * but also places "where a whole range of social forms and symbolic cultural constructions [...] challenge national affiliation in communities of migrants, merchants, and travellers from distant places." (162)
* Portals allow
  * "analysis of how global connectedness challenges a seemingly stable territorial order by extending it to other spheres" (162)
  * "invites us to look at the various means by which elites try to channel and therefore control the effects of global connectivity", e.g. by creating political structures and social control.

### The Concepts we invent

* sometimes you have to go beyond the existing categories in the way of legitimizing newly 'imagined' disciplines?

### References

Middell, Matthias & Naumann, Katja (2010) 'Global history and the spatial turn: from the impact of area studies to the study of critical junctures of globalization', *Journal of Global History* 5, 149-170

‘Transformations of territoriality, 1600–2000’, in Gunilla Budde, Sebastian Conrad, and Oliver
Janz, eds., Transnationale Geschichte: Themen, Tendenzen und Theorien, Göttingen: Vandenhoeck &
Ruprecht, 2006, pp. 32–56.

## Saunier - Globalization

[@Saunier2009]

* emphasizes the 'longue durée' aspect of globalization, and reminds that "not everything transnational or even long-distance is global." (457)
* refers to three questions that needed to be asked in regards to globalization: (cf. Cooper, 2001)
  * "newness (about the historical depth of interconnection)" (457)
  * "comprehensiveness (the evolving linking and delinking operated by the circulations in play)" (457)
  * "operation (the specificity of the structures that make connections and circulations work)" (457)

### models of globalization

* Williamson (1996), using "economic performance and convergence as key criteria" (458):
  * phase 1, "between 1850 and 1914, was when globalization began" (458)
  * phase 2, "a deglobalization phase from 1914 to 1950, as national economies turned outside-in" (458)
  * phase 3, "from 1950 until the peak of the late 20th century" (458)
* World bank uses slightly adjusted Williamson model (1870-1914, 14-45, 45-80) (458)
* Charles Albert Michalet, claiming "to build from a wider range of economic data" (458)
  * "'international' from the 19th century until the 1960s, 'multinational' from then on to the 1980s, and 'global' ever since" (458)

But:

* these models primarily built "from North Atlantic data and focused on a model of development bounded in time and space" (458)
* disregard "two of the biggest world frontiers [...] Northern and Southeast Asia" (458)
* also 'camouflages' many economic convergences and migrations during the 1920s (458)

these chronologies change considering non-economic flows:

* "refugees' and seasonal workers' movements [...] even increased" (458)
* "the sea transportation of Asian pilgrims to Mecca peaked in the late 1920s." (458)
* migration of artists, scientists, thinkers in 1930s, as well as political, social activists increased as answer to nationalization (458)

### circulatory regimes

"The proposal is to focus on the structural but dynamic, specific orders that organized, directed and empowered flows and networks of goods, people, ideas, projects or capital" (460)
"the study of circulatory regimes or configurations, and of their concatenation over time, is a promising way to capture historical developments of circulations and connections in their multiscalar instantiations." (460)

characteristics of circulatory regimes:

* actors (individual/collective), "'regime makers' - who invest time, energy and resources [...] in the establishment, maintenance and use of connections made to circulate specific items beyond the limits of their polities and societies" (461)
* formation of "intertextual" and "interactional" communities, "which can be used as resources for action by every member of these communities" (461)
* stable, long-term patterns of interactions by those that participate in the circulations (461)
* a common language for "agreements, disagreements and misunderstandings [...] that are discussed and disputed among themselves" (461)
* explicit aims to establish connections, maintain and orient their directions (461)
* a landscape "where the status of a region [...] is tied in with its role and place in the circulatory regime" (461)

?? would this landscape fit the 'regimes of territorialization' of [Middell and Naumann](#Related)?

* the landscape is simply not *affixed* to specific locations/places/something somewhere - e.g. Middell's portals of globalization
* fix it somewhere - to some specific actor's output at locations/spaces along whatever region you look at
* so *who* are the actors for him

- this brings a `transnational perspective`

### Related

[Middell, Naumann - Global History and the Spatial Turn](1910181336 Middell, Naumann - Global History and the Spatial Turn.md)

## References

Saunier, Pierre-Yves (2009) 'Globalization' in Iriye, Akira & Saunier, Perre-Yves (Eds.) *The Palgrave Dictionary of Transnational History*, Houndmills/Basingstoke: Palgrave Macmillan, pp. 456-462

Cooper, Frederick (2001), 'What is the concept of globalization good for? An African historian's perspective', *African Affairs*, 100, 399, pp. 189-213

McKeown, A. (2007), 'Periodizing globalization', *History Workshop Journal*, 63, 1, pp. 218-30

Williamson, John G (1996), 'Globalization, convergence, and history', *Journal of Economic History*, 56, 2, pp. 277-306

## Ferguson - Governing Extraction

[@Ferguson2006]

key data:

* written (2006)
* about FDI in Western Africa (esp Angola), and esp by the US

arguments:

* "new forms of capital investment are intersecting with new techniques for establishing selective political order on the continent" (195)
* attempting to establish *why* Africa's economy is seemingly poor in relation to other mineral-rich economies (194-195)
* it might be seen as a (highly efficient) model of "extractive neoliberalism" instead of inefficient corruption

### The targets of corruption

* world bank sees African economies as "stagnant because they fail to attract private investment [because] of bad government, corruption, and civil strife." (195)
* question becomes: for whom is it inefficient? (looking at Angola)
  * "the oil companies, for their part, seem to be quite satisfied with the existing arrangements [compared] with other contexts where they are dragged into costly and politically damaging disputes over environmental damage, demands for social services" (200-01)
  * indeed, it seems to be much *more* efficient for foreign oil companies than other more politically ensnared locations (201)
  * foreign banks also profit from high interest-rate short-term loans (201)
  * the government itself grows rich, by extracting rent through corruption (201)
* by being a socially "thin" model of extraction, it carries low overhead (for foreign investors & extractors), and this carries high FDI & foreign production (201)
* of course, the profits do not carry over to the population:
  * "fifty-seven percent of the population, for instance, has no access to clean water, and educational expenditures as a percentage of GNP have remained [low]." (202)

### Spread of the 'Angolan' model

* other countries seem to be engaging in a similar model of extraction with little social back-payments, and high (foreign) efficiency; however many not quite as efficient (202)
* to remain socially 'thin', governments (and foreign investors) attempt to re-create the Angolan model by having the production be as "off-shore-like" as possible:
  * "spatial enclaving of production sites with the use of foreign crews of skilled workers and private security forces" (203)
* however, territoriality is *not* completely circumvented - the government(al elites) still plays formative role in moving capital/work/economic output in&out of specific location
  * this makes it an example of de- *and* re-territorialization

other examples:

* Nigeria much more affected by social pressures & activism (202), essentially relies on "governable or ungovernable spaces" (202, cf. Michael Watts, 2004)
* Sudan, "perhaps the most violent form of the project of separating oil production from the local population" (203), rendered oil-rich land uninhabitable by driving population off it with paramilitary forces
* in opposition, (land-locked) Chad sees development efforts by the world bank in "an unprecedented attempt [...] to create institutional and financial structures", which are meant to protect the oil revenues from such private appropriation (204)

### The Efficiency of socially thin models

* there is a tension between socially 'thick' (national development states) and socially 'thin' (enclave extraction) locations (203)
* seemingly, in nations which can not create such enclaves, be it due to geographic or social 'hindrances', a socially thick approach will be preferred (204)
* but everywhere else, extraction enclaves "efficiently exploited by flexible private firms, with security provided on an 'as needed' basis", by private corporations with profit extraction in exchange for international legitimization by local elites seems the more dominant model (204)
  * there are attempts to mimick this off-shore oil extraction model in other mineral mining ventures (204-05)
  * they will attempt to employ small groups of highly skilled workers, often foreign short-term contract work (205)
  * again, the extraction enclaves will attempt to isolate themselves through the use of private security corporations (205)
  * even the term 'security' seems to be appropriated by "a privatized and spatially patchworked project [with] chaotic and undoubtedly violent surroundings" (206)
* Ferguson sees this as "perhaps the principle of deregulation taken to its logical extreme", in the sense of economic extraction and foreign exploitation making use of local resources through exploiting political instabilities. (206)
  * but highlights that there are many parallels to colonial-era extraction, where it ultimately appears "the national economy model in Africa appears less a threshold of modernity than a brief, and largely abortive, post-independence project." (207)

### Scale-making as a concept:
* worker (actor) connections create more collectivized movements - by increasing scale
* but this community-creation was undercut by the creation of enclaves - regional isolation

### Issues?

* be careful with the dissection of secondary material
* be careful with the trans-border porousness intraregional connections between African countries
* note the 'newness' he still puts in his view on the creation of these categories (even if he puts a small note at the end toward colonialism)

## Middell - De-territorialization and Re-Territorialization

[@Middell2012]

Territoriality:

* attempts at making congruent different forms of space (political, economic, cultural, identity) and convincing a community "with historical narratives and ethno-national arguments" to make it "the most applicable denominator for belonging." (Middell, no page n)
* "a historically specific formation, slowly becoming a model for the social organisation of the world from the mid-17th century onwards and dominant since the mid-19th century up until the 1970s" (no page n)
* deterritorialising factors were (for a long time) answered by reterritorialising strategies - increased border-transcending mobility in its wake provoked better measures of controlling the movements
  * e.g. passport, and concept of citizenship in answer increased migration
  * national banking, statistical data and border control allowed answer to FDI
  * increased "circulation of new cultural patterns" were confronted by "national styles and characters [which] ended often in a strong national, if not nationalistic, appropriation of cultural elements whose origin is carefully hidden" (Middell, cf. Espagne)

adequate space:

* what is the correct 'scale' of these terri/de-terri processes?
* question becomes visible in economic geography - where expansion into new markets, or defense of saturated markets highlights the vying for answering question of adequate space
* similar example can happen in political sphere: "collaboration with international regimes for the regulation of transnational problems", by finding forms of government to preserve agency and guarantee sovereignty.

### Territoriality, Imperial Structures and Nation-States

*Territoriality* as a concept is necessary for modern state-building. It is the subordination of  political, economic, cultural, identity spaces under the generalizing feature of territory. "What makes a territory effective is the coexistence between structures of dominance -- political-administrative institutions, regulation of economic activities, and social processes -- and its acceptance as a framework for loyalties."

*Imperial structures* are based on large transitional zones between neighboring societies. They are organized through an interwoven network of rulership, property, and loyalty without clear-cut borderlines. Importantly, there is no universalized notion of sovereignty by all members of the structure.

*Nation-states* became successful enough to colonially dominate parts of the globe through their high capacity to organize power and maximize profits in world markets. They are, in difference to imperial structures such as empires, based on complete equality before the law, which ultimately generates a common understanding of sovereignty by all members.

The clash between these two understandings of national sovereignty and imperial leftovers was especially highlighted during the Wilsonian moment. While the processes of de- and re-territorialization are ultimately always at work their dialectic creates moments of synchronized battle for a dominant spatial configuration -- these can be understood as critical junctures of globalization.

### Related

[Middell - De-/Re-Territorialization notes](1910210809 Middell - De-territorialization and Re-Territorialization.md)
[The History of Nation-Building (Wimmer&Schimmel)](1910281018 Wimmer, Schiller - Methodological Nationalism and beyond.md#The history of Nation-Building)

## References

Matthias Middell (2012) 'De-territorialization and re-territorialization' in Helmut K. Anheier & Mark Juergensmeyer (Eds.) *Encyclopedia of Global Studies*, : Thousand Oaks, pp.

[Text Profile](1910221118 GHS Text Profile 1.md)

# 3: Working with Historical Sources

# 4: Portals of Globalization

## Portals - a Definition

* analytical category
* places (cities, ports, trading, ..)
* high level of interactions (humans, goods, ideas,..)
* where globalization happens
* transformation; influencing *inter*-national outcomes by actors exercising their agendas
* flows / control

* a way to shift the analytical lens (btw scales)
* horizontal/vertical axes
* shed respatialization
* actors and agendas define places (??)

## Goebel - Tracing Portals within his text

actors:

* governmental institutions attempting to exercise
  * the different ministries being split into (3 regional and depending on nationality / designation) (1447)
* the various coffee house/soup kitchen owners which brought together several as negotiators
* the organizations to provide economic help fusing with ideas of political activism (e.g. UIC)
  * French communist party as being a national institution and becoming a more international place of operation
  * ComIntern being an important pillar of confluence for idea culture
  * Haitians

flows:

* ideas
  * colonial structure / anticolonial ideas
  * liberté, egalité, fraternité becoming a contradiction under the portals shrinking scale: esp ex. of veterans
* people themselves constituting a flow
* capital/goods becoming both a

paris control:

* discouraging colonial relationships - domestic scale
* control social movement (hinder them from moving to specific countries) - district scale
  * controlling the places of living (spatial control)
* surveillance of most leaders creating more 'underground' connections (local actors exercising control from farther away)
  * where people live, where people work
* hindering from gaining citizenship - which in turn fostered anticolonial ideas (citizen/foreigner/colonist [who are not counted]) - national scale

paris channelling:

* pressure to put specific immigrants into specific lines of work creating higher interconnection
  * attempts at exercizing control of living situations create inadvertent new connections between migrants and citizens
* locations of UIC/living quarters/ComIntern organizers
  * providing structures for coming together - *creating* their own structures of connection
* the social infrastructures channeled cross-ethnic contact and political activism
* idea (liberte egalité fraternité) of this idea spawning on a small scale and then jumping into an international context to influence international anticolonial ideas

different scales

* the more contrasting interconnection of different ethnicities and nationalities all being condensed into small space -- high interaction is almost unavoidable
* idea (liberte egalité fraternité) of this idea spawning on a small scale and then jumping into an international context to influence international anticolonial ideas

portal:

* is it paris itself?
* is it certain parts of paris - coffee houses / conferences / ports / living quarters
* what makes it a portal besides being a platform for these flows?
  * does the urban planning contribute? i.e. are the governmental institutions attempting to exercise control and inadvertently creating flows of anticolonial ideas the reason its a portal?
  * is the platform the critical part? the interconnection? the multitude of actors? is the existing ideology (liberte/egalite/fraternite/ vs imperial ideas)

## Related

[Text Profile - Goebel](1911031425 Goebel - Text Profile.md)

## Dietze, Maruschke, Baumann - Portals of Globalization an Introduction

[@Dietze2017]

definition portals of globalization:

"an analytical category [of global studies] to investigate how global interactions are anchored and managed in particular places" (8)

* processes of glob become tangible at particular sites (metropolises, border checkpoints, trading places, international conference venues) (8)
  * portals as structures of emanating power (Maruschke)
  * places of high intensity of interconnection (of ideas, people, goods, capital, .. -> flows)
  * places where specific *actors* can *influence* and channel/restrict flows by exercizing their *agendas*
    * i.e. they can *control* flows
* places become both locations of processes and symbolic reference points in debates of interconnected world (8)
* as such they are good starting places to analyze globalization by taking specific location and their role in global networks
* esp Sassen: cities are not merely nodes but pillars of global economy by providing its foundations: (8)
  * social connectivity
  * central management functions
  * cross-border mergers and acquisitions
  * denationalized elite and agendas
  * not *all* global cities are portals (and vice-versa)
* they allow analyzing "how global connectedness challenges a seemingly stable territorial order by extending it to other spheres" (12) by
  * looking at practices institutions and materialities of particular places
  * looking at actors which enhance, steer, regulate flows w/in specific political, economic, social projects
* portals can also be seen as places of re-spatialization, by taking active role in producing new spatial orders
* horizontal / vertical understanding (i.e. through different points of time / different scales or regions)

### References

Antje Dietze & Claudia Baumann (2017) 'Portals of Globalization - An Introduction', *Comparativ: Zeitschrift f"ur Globalgeschichte und Vergleichende Gesellschaftsforschung* 27, 7-20

## Goebel - The Capital of Men without Country

[@Goebel2016]

* "Paris and other nerve centers of African and Asian diasporas were fruitful terrain for incubating anticolonial nationalism because they condensed global inequalities as in a pressure cooker, rendering them more volatile by spotlighting the outgrowths of a wider imperial system." (1465)
  * i.e. they become *portals of globalization*

* African and Asian workers received discriminatory wages (1452), and suffered disproportionately from unemployment (1453)
* "low pensions of colonial war veterans proved even more upsetting, because they unmasked the French republican rhetoric of colonial soldiers fighting in the name of *liberté*, *egalité*, *fraternité* as no more than a cheap sham." (1453)
* "Tellingly [of the harsh colonial marriage repressions], almost all colonial community leaders in interwar Paris lived with French women before they became prominent anticolonialists. Franco-colonial intimate contact, it appears, brought the pitfalls of the imperial order into sharper relief." (1454)
* "mutual aid, local community work, and anticolonialism were inseparably interwoven." (1455)
* "Politics in the empire thus fed diasporic activities in Paris, while the politics of the city's African and Asian communities reverberated overseas through newspapers and returnees." (1456)
* "By repeatedly expelling protesting Chinese worker-students from France during the early 1920s, the French state helped to create precisely the intimate feedback loop between Paris and Shanghai that it wanted to prevent in the case of its colonial subjects." (1457)
* "the unusual concentration of disparate migrant groups within one urban space also helped transform Paris into an incubator, an amplifier, and a transmitter for the global spread of anti-imperialism." (1458)
* "Since activists from these latter countries [Haiti, Turkey, China, Syria, Cameroon,] frequently had the advantage of connections to the diplomatic circles of Paris and Geneva, there developed a shadow diplomacy in which anticolonialism and international relations overlapped" (1458)

* ?? "On the one hand, the political climate was more permissive in the French capital than in the activists' places of origin. As the civil rights lawyer Baldwin judiciously noted, anticolonial movements were "outlawed in the colonies, but comparatively free in France." (1456)
  * I wonder if this is due to the relative 'visibility' of inequalities and misery that was percetible by the French citizens - in other words, if it was an issue of location, whereas in the colonies this space was so far from the citizens that repression could be conducted much more freely

## References

Goebel, Michael (2016) ''The Capital of Men without Country': Migrants and Anticolonialism in Interwar Paris', *The American Historical Review* 121, 1444-1467

## Rossen, Salazkina - Tashkent 68

[@Djagalov2016]

structure:

* looks at festival as "heterogenous and productive site for understanding the complex relationship between Second and Third-World culture" (297)
* relates the Tashkent cinematic space to the present moment

main points:

* the structure of the film festival reflected the inherent conflict of both
  * political and commercial interests
  * and the two meanings of Third-World cinema
    * to represent the whole of the 'global' south countries
    * and to enable political action and activism on the part of filmmakers etc
* a reflection of the political landscape and cleavages between specific national/filmmaker/director actors
  * reflects the competing agendas of both participants and representative nations
  * interest cleavage between local and center (Tashkent-Moscow) (in "a brief equilibrium, a fragile co-existence", 298)
  * remained key player in intern. cinematic networks through 70s-80s, due to increased room for dialogue and room to maneuver between individual actors (298)

further information:

* Elena Razlogova (researcher and translator for some of the festival)

### References

Rossen Djagalov & Masha Salazkina (2016) 'Tashkent '68: A Cinematic Contact Zone', *Slavic Review* 75, 279-298

# 5: Global Moments, Revolutions and Critical Junctures of Globalization

## Global Moments as a Conceptual Framework

* Global moments are *imbued* with importance from the discourse before (by different actors etc.)
  * Global moments are *produced* as moments of change (enough people are talking about it - this might be an indicator)
* revolutionary change as indicator
  * quality of caesura needs to be large enough to make it qualify (and *rapidly* different enough)
  * a simultaneity of multiple transformations becomes important
* it can become important to 'listen to silence' - find moments of little discourse and you *could* still find those moments of changing global meaning
  * moments can be given global meaning by contemporaries - but just as much in the aftermath / other periods; it can also disappear
  * another indicator can be the synchronicity of crises
    * synchronicity due to the same *structural* reasons - the reasons of their unrest
    * this is *different* from entanglements as connections (Che Guevara, Wilson as spreaders of their models entangle things)

## Global Studies and the 'State of the Art'

* sometimes it may be more fruitful (when reading a text) to not ask 'what is the main message', but instead 'what is the author's question? what is the problem the author wants to solve?'
  * often hidden in discussion of the state of the art (previous lit review)
* the art of picking a discourse / analytical category / research problem *itself* is an exercise in reflexivity
  * you have to find the right disciplines which relate to your research question
  * try to find the 'state of the art' that would be the usual disciplines' approach, but then also try to find the *holes* of the existing canon
    * this allows you to give a voice to those hitherto ignored

## Periodizing as political agendas

* Periodizing *always* becomes a political action:
  * it can shed notions of responsibility by invoking a newness debate (see German history)
  * it can invoke a collective myth of origin (see Bloch - The Historian's Craft)
* example of contextualizing as importance:
  * talking about democracy in Haiti 18th cent. - modern ideas of demos and their agency

## Related

[Text Profile (McKeown)](1911101611 GHS Text Profile 5 - McKeown.md)

## McKeown 2007 - Periodizing Globalization

[@McKeown2007]

* highlights globalization's obsession with newness (219)
* definition of globalization: "The era of globalization is precisely that period in which a sense of living in the midst of unprecedented change has dominated social and personal sensibilities." (219)
  * "the belief in a new global era, pervasive since at least the early nineteenth century, is an ideal candidate to mark the beginning of modern globalization." (219)
  * 'timeless time' (Castells) also beocomes key feature of globalization by repeatedly asserting the newness of connections and communication
* 3 reasons for long-term periodization:
  * makes visible how global interactions have grown alongside growth of borders, nations and other containers which are often supposed to be defined in opposition to global/international (220)
  * allows 'productive dialogue' between qualitative analyses of transformation and empirical tracing of flows of people,goods,capital (220)
  * allows comparing with other estimated starting points of globalization to compare their strengths and weaknesses (220)
* by adding/viewing 1770-1820 as transitional period new transformations of institutional conditions of globalization can be highlighted (220):
  * free labor, rights of man
  * spread of the nation-state
  * economic development as moral and political imperative
  * free trade v protectionism debates
  * development of diplomatic,commercial,legal institutions shaping modern interactions

### Approaches to Periodization: 1571-

arguments:

Flynn & Giráldez (221-22)

* 1571: founding of Manila, establishing steady trans-Pacific connection (221)
* global silver price convergence 2x - globally linked economies (221)
* institutions & routes for long-distance interactions Atlantic-Pacific established
* new global awareness through mapping
* exchange of New&Old World crops and germs - globally transformative effects

C.A. Bayly (222)

* archaic globalization - signified by flows of exotic objects, experts, religion, ideas
* items of trade began to embody power, prestige and status
* trading diasporas developed sophisticated, far-reaching networks of credit and trade

advantages:

* helps qualify post-1820s as 'modern' globalization (222)
* helps think critically about the *emergence* of contemporary assumed paradigms of globalization (price convergence, per capita international trade, investment, networked information flows, neoliberal reform, cultural fragmentation/homogenization) (222)
* shows the ways of homogenization of border and power: the more state institutions conformed to the international 'standards', the more they could participate in the global interaction - if they did not do it "voluntarily", violence made their participation mandatory (223)
* leave room to show the mutual interaction in building a globalized world and show that our 'current' kind of globalization is just as much a historical formation and not 'the' globalization without alternative (224)

### Cycles of Globalization: 1820-

* non-periodized (or 'wrongly' periodized, like World Bank) assumptions of globalization often devolve into tautological arguments since their 'ideal' form of globalization, against which to measure, was a very specific historical constellation of 1900s North Atlantic region (226)
* migration trans-atlantic equally depicted as 'the' migration to look for, while disregarding northern and southeast Asian migrations (226)
  * "To ignore these migrations is to depict the world beyond the Atlantic as outside globalization, waiting to be incorporated" when instead they "were part and parcel of [...] a segmented and unequal globalization." (226)

### Periodization against the Newness debate

* use of these markers also explains why the newness idea is so firmly entrenched, even though 1970s onwards, as opposed to "a transformative moment of accelerated interaction or time space compression", looks strangely static&irrelevant in terms of trade, investment, tourism transformations (227)
  * "Newness render past interactions invisible." (228)
  * making any such categorical claims about a revolutionary change only reproduces "a blindness about the past and the rest of the world that has long been a constituent part of globalization." (228)
* final argumentation
  * it is not fruitful to restrict ourselves to finding a singular *true* globalization, instead it should be (reflexively) regionalized versions of globalizations (plural)
  * discourse shapes agendas; and selective blindness in discourse affects the reach/efficiency and ultimately *narrative* of resulting agendas (e.g. those outside globalized economy only awaiting to be incorporated)
  * always attempt to sensitize to flows and control - highlight the dialectic in its complex form and not just as (e.g.) national vs global

### Related

[C.A. Bayly - Archaic and Modern Globalization](1910150850 Bayly - Archaic and Modern Globalization in Eurasia and Africa.md)
adding to the concept of discourse being shaped: Melancholy Order (Adam M. McKeown)

### References

Adam McKeown (2007) 'Periodizing globalization', *History Workshop Journal* , 218--230

## Manela - Dawn of a New Era

[@Manela2007]

* after Wilsonian promise of self-determination for all peoples turned out to be primarily targeted at European/NA nations (122-23)
  * disillusionment "fueled a series of nationalist upheavals across the colonized world" (123)
  * rise of anti-colonial nationalism as international phenomenon (123)
* text is also an attempt to change the existing Euro-centrism into a more US-based one
  * making the argument that the decolonization project stems (after WWI) from the *US* instead of European idea culture
  * in other words it replicates the object of origin, just in his own national way

research question/hypothesis:

* analyzing the reaction, especially of nationalists in the colonial world, to Woodrow Wilsons promise allows understanding the rise of anticolonial nationalism as global trend, and emergence of nation-state as "sole legitimate unit of international society." (123)

* supporting arguments: (125-26)
  * combination of rhetorical iconoclasm and political opportunity played major role in motivating movements of anti-colonial nationalist resistance in Asia & Middle East 1919
  * Paris Peace Conference, marked beginning of end of European imperial project by undermining its legitimacy (in Europe *and* the colonies)
  * evolution of elites' anti-colonial nationalist sentiments was inseparable from emerging new world order they perceived, which required re-imagining themselves as self-determined nation-states
  * looking at Egypt, India, Korea, China to give colonized a voice in the proceedings

### Notion of Self-Determination

* first notion of self-determination appeared in Bolshevik-controlled Russian Provisional Government calling for peace settlement based on right of "self-determination of peoples." (126)
  * though aimed primarily at European anti-imperialist left (127)
* This was first brought into conflation with Wilson's more gradual reformism ideas (consent of the governed) by Lloyd-George (127)
  * and then adopted by Wilson (Feb 1918), when outlining US peace plan before congress (128)
* neither targeted them at peoples of Asia, but it still "echoed widely far beyond the European audiences for which it was primarily intended." (129)
* it became a "central tenet" of postwar settlement discourse in colonies (129)

### Dissemination of the Promise

* technologies of telegraph and mass print media circulated the message massively (sometimes intentionally, through US propaganda efforts) (129-30)
* many formulated political plans and postulations (through activist groups etc) for the moment that the peace conferences would be lead on the basis of the promise itself (to make a point for their individual countries) (132-33)
* but colonial intellectuals had dissenting voices which critized especially Wilson's "vague notions of political - not social - revolution." (133)

### Mobilization

* in India, national Congress called for the application of the principle of self-determination, and wanted the recognition by the major power with many local 'self-rule leagues' asking for Indian self-determination (also *in* London)(133-34)
* China (already being, formally, a sovereign state) secured seat at peace conference table, with the aim to further the establishment of a League of Nations, with them in it (135)
* Egypt wanted to send a delegation but were denied by British authorities, which lead to protests and daily petitions to be included in the proceedings (136)
* Korea prepared petitions for being freed from Japanese rule but was mostly denied by US State department, "wary of inciting Japanese ire" (136-37)

### Upheaval

* protests in Korea spread all over, with more than a million participating, and fueled by the unreliability of information due to Japanese censorship of official sources of information (137)
  * "colonial authorities launched a brutal campaign of suppression that left thousands of casualties in its wake" (137)
* in Egypt, British authorities arrested activist leaders, which sparked "massive wave of strikes and demonstrations [leading to] violent clashes" and sabotage of railway&telegraph lines (138)
  * "Over the next several months some 800 Egyptians were killed in clashes and many more wounded [which] escalated Anglo-Egyptian tensions, [...] casting a long shadow over all subsequent attempts at negotiation." (138)
* India's hopes were slowly dashed and "replaced by bitter disillusionment as the government tried to stem the tide of 'sedition' by [extending] wartime powers of internment without trial." (138-39)
  * British responded with violent actions, culminating in "the massacre at Amritsar [which] dealt the final blow to Indians' faith in British intentions" (139)
* In China, Wilson handing over German concessions to Japan resulted in violent protest and complete disillusionment in foreign powers, which united disparate "strands of political, social, and cultural discontent" (139)

### Disillusionment

* "expectations in which Woodrow Wilson was given a central role both as a symbol and a facilitator [...] in the birth of a new international order in which they would have a place as equals, began to crumble." (140)
* but the promise left its mark: the staked claims for full and immediate self-determination received public mobilization and the subsequent violent clashes with authorities "galvanized even further the commitment to an uncompromising nationalist agenda." (140)
* the imagination of "an international society in which the claim to nationhood would be the ticket to membership [...], the currency of identity, of personhood" meant that people in China, India, Egypt, Korea began "to adopt, adapt, and deploy effectively these discursive strategies to make use of the international political space created" by the pronouncements (141)
* this irrevocably transformed the fundamental colonizer/colonized relationships (141)

> The Wilsonian moment firmly planted the newly articulated right to self-determination in the international discourse of legitimacy, gravely undermining what A.P. Thornton has called the "imperial idea", not only among the colonizers, but also, and more significantly among the colonized themselves [resulting in] the swan song, rather than the apex, of the European imperial order, as the new discourse of self-determination, relentlessly wielded by colonized groups, proceeded to undermine its legitimacy. (142)

### Critiquing the text

* he is just as much branding / periodizing the process of decolonization
* an attempt to give some agency to both sides of the colonized and colonizer (but still his focus very much on diffusion *from* the important US source)
* nowadays EU/US often thought of as a unified west - but at that time still vying for primacy in being the exerter of globalization(s)
  * becoming an argument of hierarchical entanglements spread from Wilson - a PR campaign

### References

## Geggus 2010 - The Caribbean in the Age of Revolution

[@Geggus2010]

* Caribbean colonies were affected unevenly by revolutionary conceptions 1790-1820 (99)
* imperial powers generally kept their colonial rule, but "most of the regional population had by 1840 escaped from slavery, as a result of revolution or reform." (99)
* economically, the Caribbean lost dominance of international market and began its decline during this time (99)
  * due to local factors (soil exhaustion, revolution and slave emancipation disrupting economy) (100)
  * strengthened global competition (Brazil's coffee production, India's indigo, US rising cotton trade) (86)
  * "As faster-growing populations in Britain and the United States urbanized and industrialized, the Caribbean simply mattered less as a centre of wealth creation." (86)
  * only little influence from strictly European developments (100)
* slave emancipation in British colonies followed (primarily) metropolitan imperatives and timetables
  * Haiti's revolution removed economic dominance of France, which allowed "British politicians to vote their consciences without fear that it would significantly advantage France" (91)
* legislation of racial equality more influenced by local activism & demographic change (100)
* Haitian & French Revolution shaped each other, but with Haitian revolution only influencing colonial policy in F Rev (100)
  * Haiti's revolution involved *all* sectors of society and transformed economy, society, and politics (100)
  * however, central pursuit of freedom was constructed in a narrow manner, as "freedom from slavery rather than as political rights" (100)
* implicitly arguing against the global momentousness of the Haiti Revolution
* the connection of these revolutions happened not necessarily in a planned/conscious way, but due to the overlapping nature of their structural preconditions (economic, political)

  * ?? what is definition 'Republican racism' of the mid-1790s - Dubois

# 6: Postcolonialism

[@Cooper1994, @Hodge2018, @Burton2019]

* [Cooper 1994 - Conflict and Connection_Rethinking African Colonial African History] - reviewing histories of decolonization (through the lens of the subaltern); arguing decolonization was *not* always abt the nation-state
* [Hodge 2018 - Beyond dependency: north–south relationships in the age of development] - zooms into legacy of postcolonialism; history of development as political/cultural/social framework
* [Burton 2019 - Hubs of decolonization] - zoom into african agency in specific hotspots of decolonization (connect to portals/hubs); telling various narratives of different decolonizations & transnational connections

# 7: Economic Globalization

## The difference between economists and economic historians

* staticness v cyclical processes
* historians - periodizing economic phenomenon, the genesis of concepts and processes
  * the changing patterns of economic logic (through time, place, and space)
  * looking at differences (possibly) as different *modes* of economic operation
  * engaging social, political and (ofc) economic spheres of agency - attempting to *establish a holistic history*
* economists: finding a generalizing applicability
  * looking at differences as *deviations* from a normality
  * actors as participating in a model within structural givens / institutions / frameworks
  * engaging (mainly) economic models while trying to *factor out* as many variables as possible - attempting to establish *applicability through exclusion*

## The driving question of economic historians

* how did we arrive at the contemporary global economic condition
  * how did inequalities emerge
  * to understand the specific connectedness of the economy
* understanding the potential multiplicities of capitalism
  * the difference between land/capital/labor -intensive capitalisms
  * a temporal trajectory through colonial -> wage-labor capitalism
  * different institutional configurations: liberal capitalism v authoritarian capitalism (see China),
  * capitalism *as a term* was mainly coined by critics of the system (this can be seen reflected by the disappearance of the term in the 90s, when it was - for a time - approached more uncritically)
* understanding capitalism as a configuration of institution in an empirical sense to circumvent the vagueness of theoretical discussion
  * (economic history as the 'reality check' :-) )
  * configuration of private ownership of the means of production
  * but also the primacy (or at least continued support) of free markets
  * and the institutions enabling this market primacy

## Global economy without capitalism

* Roy & Riello reading two different versions of globalization (price convergence [hard globalization] / sustained exchange of goods [soft globalization])
* ?? the question of where the state comes into this picture (Smith's invisible hand vs empiric evidence of war capitalism)
* for many understandings of global economy, intermediaries (traders, mercantilists,..) *lose importance* and the different trade partners engage more directly
  * though this can pretty much only be rectified by engaging multiple understandings of capitalisms
* the construction of measurements is equally influenced by the historical *trajectory* of their structural configuration
* how to do this - *applying* the general debate around capitalisms to reading further texts
  * be aware of what critique of capitalism as a (historic) phenomenon the text creates
  * and be aware of what strand of capitalism the text then goes on to try to create (or reinforce)
  * i.e. where are the contradictions in supporting textual arguments and their previous critique
  * find the actual *usage* of the term within the text

## The influence of this on 'The Great Divergence'

* what is the great divergence
  * the attempt to explain the rise of Europe (in mainly economic) conditions - and primacy
  * answering when and how this schism began
  * which pillars of modern society actually influenced the rift (esp differences in culture)
* Pomeranz & Parthasarathi's agruments
  * method: looking at the *similarities* of economic/cultural trajectories to find remaining differences
  * solution for the original problem of land-problems was land-expansion through 'ghost-acreage' (by importing foodstuffs & cotton from Americas; substituting coal for wood) and using their status as empire to further expand their own empire
  * Parthasarathi focusing very much on *limiting* the possibility of their competitors through expansion
  * Pomeranz more focused on the exploitation of coal and colonies
  * still, the debate is *always* focused around amixture of the arguments: colonialism, conjuncture, capitalism, problems creating solutions (which worked beyond expectations), regionalization beyond 'Europe' and non'Europe' - (see Yangzi, London, etc) (as a complication of the argument)

## Parthasarathi, Pomeranz -- The Great Divergence Debate

[@Parthasarathi2019]

3 main points:

* the debate on the great divergence, while still infused with striking disagreements, can simultaneously be seen as slowly converging on some fundamental matters while centering debate around more detailed divergences
* this is mostly attributable to the interpretive nature of economic history - and its reliance on fragmented evidence to support it
* this naturally raises the question of there even being *one* overall, widely accepted, explanation for this large issue of economic divergence

personal observations:

* while repeatedly pointing out the loss of explanatory value in generalizing the progress of these nations beyond specific regions, the authors simultaneously seem to fall into generalizing some of the *scholarly* output into just such nation-based boxes
* the authors are very careful in hedging their bets when supporting arguments on purely quantitative data interpretation - especially when reviewing some mixed method approaches along the way - which (to me) paints a more complete picture of the debate as opposed to, for example, Roy and Riello in a later chapter

## References

Tirthankar Roy & Giorgio Riello (2019) 'Trade and the emergence of a world economy' in Tirthankar Roy & Giorgio Riello (Eds.) *Global Economic History*, London: Bloomsbury, pp. 137--156

Prasannan Parthasarathi & Kenneth Pomeranz (2019) 'The great divergence debate' in Tirthankar Roy & Giorgio Riello (Eds.) *Global Economic History*, London: Bloomsbury, pp. 19--37

# 8: International Organizations

## Manjapra 2010 - Communist Internationalism and Transcolonial Recognition

[@Manjapra2010]

### Transcolonial Connections

* study of transcolonial connections helps "break down the false dichotomy of the global versus the local by showing how these two static heuristic categories do not adequately portray the overlapping scales of political activity among colonial intellectuals." (159)
  * the 'foreign' terrains were laboratories for catalyzing anti-colonial struggles (161)
* 3 movements were in place before WWI: Khilafat, Swadeshi, Ghadar Movements (161)
  * Khilafat Movement: muslim community, generated in unifying wars against Ottoman Empire, and British-Turkish wars; both expatriated and fueled global anti-colonial sentiment (162)
  * Swadeshi Movement: fueled by revolutionaries travelling to Japan, activism in NY, London, Paris (coordinated); as well as Ger efforts to help revolutions in GB colonies (162)
  * Ghadar Movement: esp operating from West Coast US; close ties to other 2 movements, similar leaderships
* all used imperial urban infrastructures, amenities, shipping routes, financial capital to generate/transfer their idea clusters (161)
  * esp German/Japanese imperial structures provided transport systems (163); sometimes consciously in efforts against other nations
* counter-movements by (esp GB) government: (163)
  * Defense of India Act (1916), detaining alleged 'terrorists'
  * institution of passports (1917) for Indians travelling abroad
  * Rowlatt Sedition Bill (1919) larger scope for imprisonment w/o trial and appeal

### Communist Internationalism

* Indian communism in 1920s can best be understood by "dialectic between filiation and affiliation" (159)
  * it was not so much an *adoption* of foreign doctrine, but a circulation of ideas and field of symbols with varying interpretations (160)
* after Russian revolution (~1917) most of leadership figures converge on Moscow (164); lively interpretation of communism along movements ideals begins
  * (esp Ghadar) combining Pan-Asianism & Communism, attempts to fuse Ghandism & Comm.; (164)
  * (Khilafat) invoking peasant imaginary narratives and agrarianism w/ communist symbols (165)
  * (Swadeshi) equivalates European & Indian social dynamics (165)
  * shows "'local' expression of communism involved the modulation of globally circulating symbols using site-specific interpretive styles" (165)
* this dissemination was made primarily possible by communication systems and legitimizing authority of Soviet state (165)
  * *however* with rise of Stalin, consolidation of Soviet state late 1920s, attempts to institute more rigid political order by Soviets (167)
  * "But the Soviet state could not effectively suppress the plurality that its own global infrastructures had helped bring about" (167)
* this plurality born out of anti-colonial struggles: "labor towards common cause, [creating a] sense among the participants that solidarity was a feat of the imagination" (169)
  * "the only adequate response had to be of global, not just national, proportions [thus] the League [Against Imperialism] was a transcolonial performance of collective autonomy from imperial domination" (169)

### Related

### References

Kris Manjapra (2010) 'Communist internationalism and transcolonial recognition' in Sugata Bose & Kris Manjapra (Eds.) *Cosmopolitan thought zones: South Asia and the global circulation of ideas*, Houndmills, Basingstoke, Hampshire and New York, NY: Palgrave Macmillan, pp. 159--177

# 9: Mobility of the People

* traces mobilities of Vietnamese 'contract workers', how they form a variety of diasporic communities and play into stratification of modern Vietnam
* allows comparisons to contemporary Vietnamese governance models, arguing for panacea-like effect of export labor in reduction of poverty

supporting arguments:

* (labor) mobility was key fact to realization of "socialist international ideology" and export the idea of global socialism being the preferred path of development (236)
  * current scholarship still emphasizes migration waves as Western capitalist phenomenon, disregarding concurrent project of socialist globalization (238, 241)
  * in (national) Vietnamese media, juxtaposition of successes&failures of labor export programs often takes place, but fall prey to 'newness' emphasis (240)
  * by disregarding socialist mobility - trajectory of progress implied (from isolation to integration; stasis to development) (240)
* by disregarding global socialist movements academia highlights capitalism's mobility and obscures socialism's mobility (juxtaposing instead its immobility in created oppositions) (241)
  * after fall of Soviet state in fact many were expelled from countries abroad and sent back into "prospect of capitalist *immobility*" (241)
* global socialism - the GDR-Vietnam work exchange program:
  * "beyond the 1970s, postwar socialist aid was increasingly multidirectional and arguably less hierarchical" (242)
  * aid exchanges were often reciprocal; e.g. North Korea being assisted country but itself building housing estate in Hanoi; Vietnam being beneficiary of multilateral aid in turn sending experts to "allied countries in Africa to train technicians and scientists" (242)
  * mutual benefits for the state: GDR mitigates labor shortage, Vietnam alleviates unemployment & poverty (243-44)
  * discrimination was often *seen* as being not an issue; but legal loopholes still allowed "insidiously" working forms (e.g. job assignment toward lower-paying) (244)
  * sentiments transformed into accusations and envy over 80s especially, as shortages highlighted imagined special benefits for Vietnamese workers (245-46)


* in 2009, government stipulation created which stipulates 10,000 workers sent abroad have to come from poor districts & areas of large minorities (237)
  * main actor: Ministry of Labor, invalids and Social Affairs (Molisa) (236)

## Schwenkel - Rethinking Asian Mobilities

[@Schwenkel2014]

Schwenkel, Christina. “Rethinking Asian Mobilities: Socialist Migration and
Post-Socialist Repatriation of Vietnamese Contract Workers in East Germany.”
*Critical Asian Studies* 46, no. 2 (2014): 235–58. https://doi.org/10.1080/1467

Christina Schwenkel:

- currently teaching at University of California, Riverside
* teaching in Department of Anthropology as Associate Professor
* more specifically Head of Program for Southeast Asian Studies

### With which parts of the world (places, regions...) does this text deal?

* concerned with Post-War socialist era, especially transformations after 1970 and 1990s respectively.
* Vietnam and GDR predominantly; but also mention of other (mutual) aid programs in North Korea, several African countries

### What is the main point the author makes in this text?

* traces mobilities of Vietnamese 'contract workers', how they form a variety of diasporic communities and play into stratification of modern Vietnam
* intends to allow comparisons to contemporary Vietnamese governance models, arguing for panacea-like effect of contemporary export labor in reduction of poverty

3 supporting arguments:

* (labor) mobility was key fact to realization of "socialist international ideology" and export the idea of global socialism being the preferred path of development (236)
* by disregarding global socialist movements academia highlights capitalism's mobility and obscures socialism's mobility (juxtaposing instead its immobility in constructed oppositions) (241)
  * "beyond the 1970s, postwar socialist aid was increasingly multidirectional and arguably less hierarchical"; however, while discrimination was presumed to be a non-issue, legal loopholes were still exploited to enable such hierarchical transformations

### What kind of text is it (research article, review article, introduction, book chapter,...)?

* Research article
* Based on archive material, interviews as well as secondary academic literature

# 10: Mobility of Goods

## Prestholdt - Global Repercussions of East African Consumerism

[@Prestholdt2004]

* investigates repercussions of production/consumption *reciprocally*, by taking into account local contingencies (of Bombay & Salem, MA) (755, 759)
* uses concept of 'links' as connectors in global product chain: consumers, retailers in East African society; caravan porters carrying goods; caravan leaders buying and arranging goods' transport; foreign agents & local buyers in Zanzibar (& elsewhere); ships delivering cargo; firms, brokers, consignors in ports; manufacturers of consumer goods (760)
* show *how* consumer demand drove system of production; how it was relayed (and *translated*), how this organized and brought to bear certain production chains over others (760)
  * example of consumer driven production in sending of samples, gauging and translating of needs *through the local agents*, reflection of local expertise vs general assumptions in production location (762)
  * local expertise is built up over time, e.g. complaining "that the consignor in Salem had spent enough time in Zanzibar 'to know the kind wanted for the Zanzibar market.'" (762)
* caravan leaders as "lynchpins in the system of global exchange.", knowing about demands of commodities through experience, reports, expertise in trade equivalencies of goods (764)
  * also (political) gifts to make spatial navigation through different territories possible (765)
  * they even *re-fashioned* a lot of the goods along the way to better comply with local demands, complicating the simple view of selling finished products of a global value chain (i.e. finished good import; raw material export in Africa) (765-66)
* traces domestic US occurences (mostly the economic export results of the Civil War) through their influence on the African import markets; as well as power relations to other exporting actors (e.g. Bombay textile firms) (772)

* especially the 're-configuration' of imported goods seems an important point to make: even today, market analysts struggle with the recognition of qualitatively different steps in along the capitalist value chain - *especially* in mostly quantitative data
  * example of disputed nature of mainland China's
* the *links* themselves, in this text, become the main actors with producers especially almost becoming an externality
  * this is useful to highlight the reciprocal nature, but I still wonder if an analysis of the *active* role of the same phenomenon at the production locations instead of solely *re-* active actions would fully complement the painted picture

### Related

### Who is the author?

Jeremy Prestholdt:

* Department of History Northeastern University
* focus on contemporary globalization; its migration and economy as well as study of globally circulating symbols

### What is the period of time this text deals with?

1820s-1880s; special emphasis on rising US relationship during 1830s and its decline (during and) after Civil War

### With which parts of the world (places, regions...) does this text deal?

* Zanzibar
* Salem, MA
* specific portals such as ports; Bombay's textile production facilities and ports
* the coastal trading region of East Africa; its inner trading routes

### What is the main point the author makes in this text?

main points:

* investigates repercussions of production/consumption *reciprocally*, by taking into account local contingencies (of Bombay & Salem, MA) (755, 759)
* uses concept of 'links' as connectors in global product chain: consumers, retailers in East African society; caravan porters carrying goods; caravan leaders buying and arranging goods' transport; foreign agents & local buyers in Zanzibar (& elsewhere); ships delivering cargo; firms, brokers, consignors in ports; manufacturers of consumer goods (760)
* show *how* consumer demand drove system of production; how it was relayed (and *translated*), how this organized and brought to bear certain production chains over others (760)
* caravan leaders as "lynchpins in the system of global exchange.", knowing about demands of commodities through experience, reports, expertise in trade equivalencies of goods (764)
* traces domestic US occurences (mostly the economic export results of the Civil War) through their influence on the African import markets; as well as power relations to other exporting actors (e.g. Bombay textile firms) (772)

personal observations:

* especially the 're-configuration' of imported goods seems an important point to make: even today, market analysts struggle with the recognition of qualitatively different steps in along the capitalist value chain - *especially* in mostly quantitative data analysis
  * e.g. disputed nature of mainland China's rapid rise to high-tech export nation versus considerations of being an 'assembly' step not unlike a more globalized version of these caravan traders
* the *links* themselves, in this text, become the main actors with producers especially almost becoming an externality
  * this is useful to highlight the reciprocal nature, but I still wonder if an analysis of the *active* role of the same phenomenon at the production locations instead of solely *re-* actions would better complement the painted picture


### References

# 11: Financialization

## Cassis - The Rise of Financialization (2019)

[@Cassis2019]

argument:

* Traces changing actors and processes in global finance:
  * How new leading cities emerge, why, and when
  * How banks as actors develop and internalize more and more resources on increasingly unregulated market
  * How factors of control are reintroduced as responses to (global) crises in finance or sectors adjacent enough to directly influence it

### 1850-1914

* starkly rising net exports from 1855, especially relative to GDP (in small European countries those became substantial: NL, Belgium, Switzerland) (230)
* France, GB primary exporter; US primary importer (230)
* rising transport (&comm) possibilities:
  * 1830s+ beginning of railways (1870: 205,000km world mileage; 1906: 925,000km) (230)
  * 1890+ maritime transport
* rising communication:
  * telegraph (GB-EU 1851; Transatlantic 1866)
  * telephone (1891; 1920)
  * wireless telegraphy ("messages were transmitted across the Atlantic from 1901") ?? (230)
* economy ordered around system of 'dominant' economy (GB) which functioned as multilateral link to others (231)
* economic importance of London: (232)
  * drawing bills of exchange
  * issuing foreign loans
  * stock market exchange
* Loans went to unequal ends and achieved unequal profits (and advancements) (234)
  * Many gov loans went to military / previous debts
  * Some went into infrastructure (rail/roads/docks/ports/power stations,..), thus stimulated economic growth through more natural Resource exploitation
  * *But* indebtedness bore costs (which went up for many countries not in the group of countries seen as financially stable), and even political costs (loss of pol. Independence etc) - until occupations e.g. Egypt

### 1914-1973

* WWI years often not seen as 'global' market, due to loans primarily between national governments with few private parties involved (235)
* shift of financial actors towards US, "Europe was no longer the world's banker; Germany lost nearly all its foreign assets; France most of it" (235)
  * with esp New York being marked by interplay of national & international business (investments) much more than London (236)
* Great Depression "was a watershed in the history of global finance [with] four interrelated shock waves: the Wall Street Crash of October 1929, by a series of banking crises occurring over a period of five years, the collapse of the world monetary order, and an economic slump of dramatic proportions." (237)
  * capital investments dwindled, NY's position shriveled (237)
* WWII similar to WWI, only with global financial activities already in decline beforehand; more national financial regulation already in place than before (237)
* after WWII NY emerged again as world financial capital; unchallenged until 1960s (though with overall smaller trade volumes) (238)
* emergence of 'Euromarkets', where transactions in dollars took place outside US (and outside US regulations!) (238)
  * esp expedited in London after sterling crisis (1957) banned using sterling instruments for 3rd party trade (238)
  * *rapid* expansion of this market afterwards, starting "multinational expansion of American banks" (238-39)
* London re-grew in importance through "positive attitude of the British monetary authorities" in comparison to Europeans (239)

### 1973-2000

* demise of Bretton Woods fixed exchange rates Mundell-Fleming's trilemma was solved ('only 2 of the three could be pursued:') (240)
  * fixed exchange rates
  * free movements of capital
  * independent monetary policy
* new climate now marked "by financial deregulations and innovations." (240)
  * culminating in repeal of Glass-Steagall Act of 1933 (1999), allowing intertwined branches of commercial & investment banking (240)
* "New York, London, Hong Kong, and Singapore stood at the apex of a hierarchy of financial centres [...] with some countries, especially in Eastern Europe, having more than 50 per cent of assets controlled by foreign banks." (243)
* Global Finance now dominated by world's leading universal banks who, "even more significantly, the bank had internalized its international activities, and was able to draw resources from one place and exploit them in another." (243)

* "A retreat from global finance has never been a natural swing of the pendulum. It has always been the result of a devastating financial crisis. And the upheavals of the world's financial order have tended to take place in the midst of military cataclysms." (244)

## Engel - The Bang after the Boom: Understanding Financialization (2015)

[@Engel2015]

* Presenting overview of state of the art of Financialization research; and explaining concepts of marketization (disintermediation, securitization, derivatives)
* poses additional questions regarding financialization
  * it reconfigured "the basic mechanics of the capitalist economy [...] at the financial core.", but did it affect the economy outside this core, i.e. the logic of everyday economic procedures and actions? (510)
  * questions the pervasiveness of rent-seeking as feature of financial market capitalism, and how it will compare to entrepeneurship (510)

### Defining Financialization

* often seen as transition from Fordism as regime of production to financial market capitalism as finance-oriented regime (502)
  * Doering-Manteuffel and Raphael state, it's the most important and powerful force behind the complex societal transformations of the last four decades (502)
  * but "they seem to regard financialization as the one historical process that can be taken as given, and which can be used as a back-drop against which to problematize all others." (502)
  * see it as comprised of 3 factors: rise of information technologies & digitalization; change in macroeconomic principles to monetarism; 'progressive' reconceptualization of man for 'entrepeneurial self' tending to become hegemonic form of subjectification -- but where is *the financial* in this? (502-03)
* might be misleading to pinpoint monetarism as central pillar by suggesting that privatization & deregulation are aspects of monetarism and not marketization (or that marketization is result of monetarism) (503)

### Perspectives on Financialization

* economic sociology
  * Paul Windolf (Sociologist) defining financial market capitalism as a regime of production, seeing it as governing force of 'real' economy; by industry financing now driven through stocks; firms depending on possession of shares and thus looking to maximize (short-term) profit per share (504)
  * Krippner instead stresses *shift* of profits from 'real' economy to finance; analyzing growth of financial markets and to new indebtedness of states, corporations and households (504-05)
* political economy
  * Gerald A. Epstein encompassing definition: "the increasing role of financial motives, financial markets, financial actors and financial institutions in the operations of the domestic and international economies" (505; from Epstein 2005, 3)
  * Arrighi and Silver using world system theory to define it as "final stage of hegemony by a politico-economic power"; referring to contemporary US, 19th cent. British empire; early modern Dutch financialization; "the hegemon loses its real economy to the periphery (due to lower wages) and turns to finance as the final and most lucrative economic field in which hegemony can be sustained." (505)
* economy history
  * Davis, financialization as powered by transformation from manufacturing to service economy, helped by rise of information and communication technologies; i.e. as consequence of failure of Fordism and faltering post-war economic boom (506)
  * Deutschmann, as success of entrepeneurship automatically leading to increased rent-seeking, and transition to capitalism in which rent-seeking is more successful than entrepeneurship, thus hegemonc; i.e. as result of success of Fordism (506)

### Concepts of Financialization

* increasingly borrowers and lenders circument traditional services of banks in practices ('disintermediation') (507)
* 'Securitization' means turning (difficult to transfer) financial assets into tradable financial assets ('securities'), pooling individual debts, capital demands and turning them into tradable versions; e.g. the stock market (507)
* Derivatives being securities that derive their "value from the development of an underlying asset or parameter (such as a commodity or share price, an interest rate, or a stock index)." (508) - they can work as buffers to offset risks (hedging) or to allow more specific risk-taking (speculation)

## Fairhead - Green Grabbing (2013)

[@Fairhead2012]

* looks at new legitimizations, implementations, and valuations of (environmentally justified) land appropriation and valuation

### 'Green' Appropriation

* appropriation understanding as central to accumulation and dispossession (238)
  * e.g. simple capital accumulation and reinvestment for concentrated ownership
  * or primitive accumulation through private enclosure of publicly owned good/nature and expulsion/forceful separation
* article focuses on "instances where environmental agendas are the core drivers." (239)
* phenomenon builds on colonial/neo-colonial resource alienation *in the name of the environment*, e.g. for parks, halting of (assumed) destructive local practices, forest reserves (239)
* invoking Harvey's "accumulation by dispossession":
  * privatization
    * public assets -> private companies; e.g. nature, once held in trust of people by government, now sold as stock to foreign wildlife, ecotourism, mining companies (243)
    * can happen as primitive dispossession, or by delegitimizing claims through legislation, or 'through the market' by having those who possess valuable assets have too low incomes for social reproduction and thus forcing the sale of said assets (243)
  * financialization
    * critical precondition for emergence and operation of e.g. offset trading market (243)
    * leads to new valuation of nature, from resource conservation to "demonstrating extensive economic value", creating a approach that seeks payment for services (244)
    * nature now valued by potential for economic viability (along environmental basis, i.e. developing the 'green gaze'): "Environments thus become business assets, cash cows producing dependable incomes from the services they provide." (244)
    * rests on equivalence to enable dislocation: carbon offset market, species offset market depends on idea of carbon existence and species (and even individual animal) existence being the same 'here' or 'there' (the same meaning, effects, sustainability, etc) (245)
    * ultimately, "nature must pay its way [by producing] the value that keeps it afloat. It must create water, air and other taxes for 'ecosystem services' competitively or go under to be replaced by nature somewhere else that produces such products and services more efficiently." (245)
  * crisis management and manipulation
    * perpetuation of a sense of crisis to work towards accumulation ('disaster capitalism'); nature "becomes a valuable asset which not only can be, but must be, sold" by those in position of non-negotiability, or through discourse of inevitable crisis management (245)
  * state redistribution, favoring capitalist business interests
    * fiscal policies favoring investment, thus those with capital to invest, over income stability and security (245)
    * availability of assets, by turning land into marketable resources allows accumulation, and is incentivized especially "for states with limited fiscal resources" (245)
    * value of the commodity is at the same time "co-produced within the architecture of its financialization -- in interaction with the international institutions apparently governing them and the policies of the state." (246-247)

### Materiality of Nature

* 'green economy' as firm part of ecological modernization, with "economic growth and environmental conservation" working in tandem; ultimately "things green have become big business and an integral part of the mainstream growth economy." (240)
* nature increasingly appreciated as valuable source of profit, new contemporary materiality and political economic relations are created (241)
  * can be seen as 'discursive commodity', which is legitimized through nexus of technological advances and (often academic) discourse (241, 246)
* dual appreciation of nature (conservation and sustainability) becomes infused by third valuation: 'economy of repair' (242)
  * repair of damaged nature, pricing the downside of growth *creates new value* in "commodities such as carbon, biofuels and offsets of all kinds" (242)
  * often driven under banner of sustainability though logics are incompatible: in repair economy "unsustainable use 'here' can be repaired by sustainable practices 'there', with one nature subordinated to the other" (242)
  * this allows doubly valuing nature -- using it and then repairing it; economic growth driven damage provides grounds for new economic growth of repair (242)
* standing against this (now naturalized) process of market logic and valuation is ('peasant resistance') increasingly dismissed "as individual, isolated opposition: not as valid social mobilization, but anachronistic holding-out against a common-sense green tide." -- which is again the naturalizing effect of the prevailing discursive agenda (253)

### New Perspectives

* article servers as introduction to ways of analyzing these processes, but also toward new appreciations "for distributive justice and equitable development [of inserting firm requirements] into market arrangements." (255)
* calls for emergence of new mobilizations to "recapture nature from the clutches of market mechanisms, reinforcing local cultural understandings and the embedded 'ontologies of nature'" (255)
* most importantly, "this will require a reframing of intellectual as well as policy focus. While understanding and critiquing the processes that result in the 'neoliberalization of nature' are important, they are clearly insufficient." (255)

### Related

## References

Youssef Cassis (2019) 'The rise of global finance, 1850-2000' in Tirthankar Roy & Giorgio Riello (Eds.) *Global Economic History*, London: Bloomsbury, pp. 229--247

Engel, Alexander (2015) 'The bang after the boom: understanding financialization', *ZZF – Centre for Contemporary History: Zeithistorische Forschungen* 12, 500--510

James Fairhead & Ian Scoones (2012) 'Green grabbing: a new appropriation of nature?', *Journal of Peasant Studies* 39, 237--261

# 12: Technology

## Tutorial

### Definition Technology

The discursive framing, organization, application of knowledge, ideas and assumptions through exercise of individual/collective agencies; epistemes; power relations.

### Technology Transfer

* conscious / unconscious
* interrelated transfer of technology
* exponential?
* it follows specific agendas
  * either by those creating the diffusion of technology, by those implementing it
  * in other words, influenced on all levels and scales (both by those shaping discourse and agenda and those implementing the regime)
* since both the creation & transfer of new technologies is contingent on specific (solutions / political ambitions / economic) -> agendas, it equally depends on the ability to exercise these agendas

### History of technology

* first originally mainly European centered
  * idea of linear expansion
  * exponential spread and diffusion of technologies
  * idea of levels and backwardness vs advanced implementation
* decolonization
  * rise of new actors participating in international institutions and discourse
  * physical / epistemological violence able to disrupt and counter technology, also exercised by (Western) European countries
  * thus disrupting the existing technological resources in order to hegemonically institute a new paradigm
* de-dichotomizing
  * attempts to dis-entagle the apparent dichotomy of those implementing and those receiving technologies

## Kerr - Railways in India

[@Kerr1995]

* the hybridization of technology
  * e.g. sleeping disease medicine being developed in West Africa (tropical hygene) profiting from test-driven research in colonial laboratories
  * colonies as experimentation laboratories (other examples e.g. Angola mining technologies then also being exported South-South within Africa)
* impacts of technology on social- / labor-systems
  * whole families working with 'their' implementation of technology (basket on head); attempts at disruption through institution of wheelbarrow (which now would only allow the men person to transport it) (page??)
  * counter-example of successful transfer of technology in brick moulding from British engineers
  * training of Indian laborers into new 'skills' creates a new class of skilled laborers, who then through increased mobility in turn train new local experts and participate in other projects (98)
  * partial transfer of technologies, e.g. repair of British locomotives which were imported at the behest of Britain
* e.g. anti-industrial movement by forces following Gandhi's refusal to bow down to the 'destruction' of indigenous technologies and existing culture by 'alien' hegemonic influence
* these are examples of industrial techniques being transferred, but count-examples exists where there was e.g. 'systematic' rejection or hegemonic
  * these sometimes (had to be) accepted by the colonizing nation

## Müller, Tworke - Telegraph Lines

[@Mueller2015]

* Global capitalism and the spread of communications (especially the telegraph) were intensely intertwined in their influencing each other
* however: this influence did *not* only happen unilaterally; nor was it a process of continual, mutual support by the participating actors
* the analysis of this basic assumption (the spread of both processes as unilateral and mutually supporting) has, so far, mostly been glossed over, both in business analysis and historical scholarship, pointing to fundamental epistemological assumptions about their relationship
* the article seeks to --- if not undermine --- then at least complicate the basis of these assumptions by including specific actors that followed their own agendas in the creation of these networks; and specific instances highlighting the dialectic of extending, directing, and controlling these new markets and their connections
* switch from political control / and imperial logic -> technology as control and diffusion of power (control of empire)
* over time imperial logics drift from active pursuit into a more deregulated, market-driven profit maximization regimes
* resistance along laid lines by the powers over-land -- often being targets of attacks and physical violence
  * afterwards appropriation of Indian nationalist now using the communication technology  (example of the contextual importance of time: same group, 20 years apart rejecting or making use of the imported technology)

## Van Fleck - Airline at the Crossroads

[@Vleck2009]

* corporate buying of shares of Afghan airline by Pan Am
* modernization along Cold War ambitions *but* also exploitation for economic profit maximization
* different narratives of modernization being instilled
  * nationalist narrative - modernization (from Pashtun ethnicity)
  * US - 'modernization theory' narrative, but also PanAm specific modernization narrative (the modern Western, gender inclusive [stewardesses] narrative)
  * Socialist imaginary (Soviet union's ambition toward airport, dam, etc modernization projects)
* commercial aviation projects being not just capital interests but also the symbolic importance of *modernization* on a global level, on a regime level (capitalist, socialist), on a national level, on a cultural, and local/regional level of different applications of ability

### Comparison

* different narratives of hierarchies of control, labour systems being decided by technology, technology itself underlying regimes of transfer and diffusion, categorization of knowledge production and transfer
* analysis of technology and capital as different interests
* technology and communication as different ways of approach

# 13: Knowledge

## Tutorial

* why are the origins important?
  * creation of knowledge already creates (asymmetrical) relationships of power dynamics
  * who sees what as relevant -- and why?
* differentiating between the different steps within knowledge production
  * collection / production
  * processing
  * distribution / diffusion (and reconfiguration)
* emergence of universities now being the institutionalized space of knowledge production
* institutions of knowledge
  * universities
  * conferences
  * world exhibitions
  * museums / libraries / archives / collections
  * schools as dissemination spaces of knowledge
  * think tanks
  * centers of big data
  * church
* the preserved knowledge in these locations already *ordered* after a certain (naturalized) organization scheme
  * even the underlying infrastructure of these hubs of knowledge dependent on underlying access to resources (power/internet access)
* infrastructure often being provided by the nation-state, thus (apparently) often focused around this scale when thinking
  * especially national fairs becoming prevalent during the nation-building project
  * world exhibitions as the paradox of being very national in character, but also spaces of international/transnational diffusion and reconfiguration
* when thinking about transformation to *information*/*network* society, it is equally important to think about *disconnects* that pertain to e.g. the underlying infrastructure
* how does the church play into this
  * printing press as innovation already providing reconfigurations *within* religion
  * churches then (before the nation-state) being the primary providers of literacy and
  * not necessarily a dichotomy between
  * church producing knowledge itself: church-schools, missionaries, skill production within church communities
* *how* does science move?
  * publications to be analyzed (who are the readers/who are the authors/editors of the journal -- where do they come from [discipline/location]/who gets access/who do they cite)
  * physical movement (scientists/explorers)
  * publishing houses objectifying knowledge and *allowing* setting it on its move in the first place
* Raj looking at the exchange and encounter in e.g. a journal as the *site* of knowledge creation -- footnotes as scholarly discussion already including a circulation of knowledge

## Raj 2013 - Beyond postcolonialism and postpositivism

[@Raj2013]

* fundamentally: taking 'Great Divergence' debate and focusing it onto science (and scientific 'progress') itself; attempting to escape the resulting dilemma of West and Rest (340)

### Trajectory of historiography of science: (337-38)

* from idealist version (science just 'spreads', based on acceptance of rational truths)
* to post-idealist split: post-colonial [only chronologically, *not* yet post-colonial in approach!], postidealist
* Joseph Needham's approach: why did (modern) science only originate in Europe, not China? (338)
  * answered in "resilience of China's agrarian bureaucratic culture", not letting mercantilism, ind. capitalism spread (338)
  * science not created ex nihilo but subsumed to medieval learning of West/East; *but* culturally universal (e.g. maths)
  * thus, inventions did not bridge cultural gap from China -> West, but underlying theoretical approaches did
  * many attempts to complement it over time, e.g. from South Asian sources - see p. 339 fn. 8
* George Basalla: three-staged model of 'spread' of 'Western science' (338-38)
  * period 1: non-European "societies serve as passive reservoirs of data"; preliminary scientific experimentation (338)
  * period 2: colonial dependency, European scientific institutions begin encouraging 'Western' scientific activity outside Europe
  * period 3: "colonized societies gain maturity"; struggle to create sc. traditions *based on* Western professional standards (339)
  * critical engagement:
    * judging non-West as "scientific *tabula rasa*" (339, emph author)
    * diffusionism - dichotomy of active 'spreader' and passive 'receiver'
    * colonial scientific policy - the active shaping vs hegemonic power imbalance
* fundamentally still share the belief of universalism; embedded rationality, truth; markers of morality, social, material, and civilization's progress (340)
* so, question becomes: does science emanate from Western Europe, creating own Great Divide; or based on nationalist/civilizationalist narratives vying for precedence? (340)
  * following Foucault, Said, (& Headrick) explanations rather around hegemonic 'master narrative' of science as formalized subjugating discourse
  * this still supposes fundamental unity of 'Western' science, and opposition to 'Other' narratives (i.e. follows diffusionism) (341)
* post-positivist understanding:
  * science itself understood as "construction, maintenance, extension, and reconfiguration of knowledge" with material/instrumental/corporeal/practical/social/political/cognitive aspects (341)
  * science thus not based on truthful reasoning, formal propositions and universal rationality; "but on pragmatic judment, much like that involved in practical crafts" - making it contextually situated in place and time (341)
  * however: still sharing dogma of Western origins (only case studies from Europe & America); implicit acceptance of 'center/periphery' model (342)
    * "postpositivist historians have acquiesced willy-nilly [...] that modern science is 'distinctly Western in its inception' --- although no longer in its pursuit or execution." (342)

### New Framework - Transformative Circulation

* not restricting to viewing laboratories, libraries, 'cabinets of curiosity' as places where science is 'made', but during its *movement* and *encounter* with skills, practices, material, ideas of other specialized communities (ethnology, linguistics, medicine, cartography, ..) (342)
* *interactions* as "a locus of knowledge construction and reconfiguration", by focusing on "centrality of circulation to analyze its consequences for the sciences and their history on a global scale" (343)
* circulation: not just dissemination/transmission/communication of ideas, but: "processes of encounter, power and resistance, negotiation, and reconfiguration that occur in cross-cultural interaction." (343)
* example: Botany example of 14 vol. herbal containing 720 Indian plants, issued by French surgeon end of 17th century (343-344)
  * local agents (invariably indigenous people) described as "informants" (passive role of answering)
  * *but* contain varied roles: "fakirs as depositories of herbal and medicinal knowledge to male and female collectors, illustrators, translators, bookbinders, and mediators", going so far that "the French surgeon designates himself a mere translator of the text" (through Raj disagrees) (344)
  * template for very structure of the work is existing Dutch compilation from southwestern India; finally being deliberately ignored in France over ontological dispute (344)
* circulation as refusal of uni-directionality of diffusion/transmission, including mutations and reconfigurations en route (344)
  * should not be understood as ignorance of asymmetric power relations at play; but imbue local actors with agency within this hegemonic framework (344)
* *not everything circulates*: dependent on conditions of e.g. exchange of favors, patronage, friendship, obligation, economic exchange, .. (344-45), sometimes this happens 'by chance' (unconsciously), sometimes in very directed process (see Markovits quote, 345)
* ultimately idea is to move circulation to be the *site* of knowledge formation to remove the separation of three moments in making knowledge: (345)
  * collection of information or objects
  * accumulation and processing within local, segregated space of laboratory
  * spread and (eventual universal) acceptance

## Burke 2011 - A Social History of Knowledge II

[@Burke2011]

* monograph generally carries idea of dialectic of expansion & control:
  * "The nationalization of knowledge coexists with its internationalization, secularization with counter-secularization, professionalization with amateurization, standardization with custom-made products, specialization with interdisciplinary projects and democratization with moves to counter or restrict it." Excepting however technologization, which "seems to march onwards without encountering serious opposition." (2)
* going beyond a pure history of ideas, and looking at the centers/actors/processes which shape and contextualize them, mostly through comparison
  * looking for advancements and agents of exploration in groups, not individuals (though still naming plenty individuals) (4)
  * especially looking at spatialization of knowledge; but undercuts its ivory-tower like remoteness: "[scholars] take the world, including politics, into the lab with them, while their results are often used [...] for worldly purposes." (4)
* idea of multiplicity of 'knowledges' (5)
  * differentiating between information and knowledge (see Bronislaw Malinowski, Drucker 93)
  * typology: explicit/implicit (tacit); pure/applied; local/universal; knowing 'how'/'that'
  * dominated/subjugated knowledges *alongside* rather than underneath dominant ones
  * political understanding of knowledge: who decides authoritatively what constitutes knowledge?
* geography of knowledge: (6)
  * spread - not necessarily appropriate; "it is more realistic to think in terms of an active reception in which individuals and groups beyond the West appropriated and adapted western knowledge for their own purposes" (active shaping of knowledge through local agency)
  * traffic in opposite direction (indigenous guides, maps being key for explorers, botanists, linguists, etc) (cf. Raj2013)

### Geographies of knowledge

* spatial turn generated questions of situadedness, also within sciences; problematizing transition from local to general knowledge (187)
* micro-space geography scales:
  * clinic/library/observatory/laboratory/museums as dependent on their inner geography (188)
  * units, e.g. university campus; bigger laboratories creating isolation at periphery of campus; reinforcing specialization (188)
  * cities with large concentration of people sharing certain interest to enable exchange of ideas, information at sites (bookshop/club/coffeehouse/tavern) (189)
  * region: centrality allowing certain perks; but also periphery enabling more freedom, less surveillance (e.g. Academy in remote Siberia opening removing Soviet surveillance to large part) (191)
  * nation: increasing importance of national origin of science/scientist (192ff.)

### Nationalization of knowledge:

  * scientists acting as 'representatives of their respective countries [as] an organized army labouring on behalf of the whole nation' (Helmholtz 1893, 24) (192)
  * nat. of knowledge sometimes basically continuation of politics by other means (192)
  * institutionalization of study of vernacular literatures by universities replacing ancient Greek/Roman writers (193)
  * compilation of dictionaries becoming 'patriotic enterprise', different national interpretations of the truths (193)
  * foundation of libraries&archives underlying nation-building project (e.g. French royal library, Royal Library at The Hague, etc) (195)
  * political nature of encyclopaedias; field of competition for nations (196)
  * competition/rivalry of scientific discoveries and advances themselves increasingly 'belonging' to nations and national consciousness (197)
  * thus turn away from 'commonwealth of learning' as imagined community towards specialization and nationalization (198)
    * shift especially from cosmopolitanism to nationalism ('Weltbürgertum zu Nationalstaat', Friedrich Meinecke)
    * Napoleonic wars then disrupting intl communication; undermining 'sciences are never at war' even while spoken (1803) (198)

### Centres, peripheries, and migration

* centres creating hegemonies of learning and esp *styles* of learning (political constitution of authority over knowledge) (198-201)
* disadvantaging peripheral (subaltern) countries, esp 'Matthew Effect' (Robert Merton) (202)
  * "well-known scientists often receive credit for discoveries made by less famous figures" (202)
* extrapolating from this effect: (203)
  * macro-level: contributions from non-West often insufficiently acknowledged
  * micro-level: members of prestigious departments cite other members of prestigious departments more than non-prestigious
* in circulations/transferring knowledge - the 'indigenous informants' can be much more understood as cultural brokers, actively shaping knowledge acquisition/processing (205)
* migration/exile of scholars itself constitutes circulation of knowledge (regimes) (208)
  * pull factors of attractive working conditions
  * push factors, fear of persecution, exile - famous example Marx; 'Great Exodus' in 1930s
* understanding knowledge dissemination as supply oriented (212-13)
  * encouraging spread by westerners as imperial exercise (or through belief of superiority)
  * before westernization project, e.g. British administrators were more accomodating ('sympathetic') to local culture and knowledge regime
  * often, opening up to western knowledge as defensive reaction of incorporating a 'minimum dose of Westernization'
* demand oriented (214-15)
  * especially military knowledge, e.g. 'Self-Strengthening Movement' establishing westernized military arsenals with help of expats
  * through this demand-led 'invasion', traditional (e.g. Chinese) science suffers displacement, rivalry, segregation
* example of difficulty of acclimation:
  * 'liberty' in Japanese translation carrying connotations of selfishness, willfulness; concept *itself* did not translate well (216)
  * connotations like these are presumably an example of larger phenomenon of hybridization
  * "Sometimes we find a synthesis between different knowledges, at other times a kind of segregation. Today, when practices such as yoga and acupuncture are common [...], the clients generally share western attitudes in other respects."

# Related

[Text Profile - Raj 2003](2001201450 GHS Text Profile Raj Beyond Postcolonialism.md)
[Knowledge Typologies](Knowledge Typologies.md)

# References

Helmholtz, H. von 1893, *Popular Lectures on Scientific Subjects*, London

# 14: War, Peace and Intervention

## Byrne - Beyond Continents, Colours, and the Cold War

[@Byrne2015]

* traces Non-Aligned Movement's inception, and actions within the first decade after 1955s Bandung Conference; in 2 phases --'61, its formalizing, a period of 'insurgent neutralism'; and --'65 aborted 'Bandung 2' Conference, a period of vying to be the prime Third World affair organizing principle (913)

* first phase 1955--61 (Bandung--formal founding of NAM in Belgrade): "saw a propagation of of a new understanding of non-alignment that I will call 'insurgent neutralism'" (913)
  * smaller, 'insurgent' countries (e.g. Yugoslavia, Egypt) began exercising "more provocative and intentionally destabilising concept of non-alignment" (913)
  * this increased their agency compared to previous big Third World players China, India (who mostly still practiced stabilizing non-engagement)
  * interestingly, *rhetoric* of NAM continued advocacy of cold war disengagement, but NAM itself was often used to engage/exploit tensions by smaller nations
* second phase ~1962--65 (founding--'non-event' of Afro-Asian summit 'Bandung2'): "NAM vied with Afro-Asianism as the primary organising principle in Third World affairs" (913)
  * question centered around inclusive/exclusive understanding of Third World: along "explicitly southern, non-white, poor, and post-colonial identity" (914)
  * alternative proposed was to see it as *political* project which "welcomed any country or organisation that shared the same goals" (914)
  * esp. Chinese leadership arguing for racial/geographical ThW definition (see Chinese colonial dichotomy, cf. Goebel and Monson) (914)
  * particularly racial identity question laden with politically unifying potential but finally 'failed' due to failed overlap of ethno-racial and political spaces of ambition (926)
* late 60s NAM experienced more of lull -- most 1st wave leaders either dead or politically less relevant (Nehru, Bella, Nkrumah, Sukarno, Nasser,..) (927)
* however, resurgence of NAM (~mid 70s) mostly referring back to *identity* question of second phase which invoked South-South existential debates over belonging (927)
* but still, NAM was (at least its resurgence itself) a political reaction to détente: 'anti-systemic rebellion' (in wake of industrialized trade barriers, incoming structural adjustment methods) (928)
  * however, rhetorically still non-aligned -- inherent tension to existence (928)
  * tensions e.g. visible in final declaration of Algerian meeting: "The global economic order, the declaration continued, was essentially the old imperial system adapted in order to ‘perpetuate in another form their stranglehold on the resources of the developing countries and to ensure for themselves all kinds of privileges and guaranteed markets for their manufactured products and services’" (928)
  * however, possibility that still economic policies were decided on based on re-intensifying great power rivalry (*not* conclusively decided by text!) (928)

Ultimately NAM take-away by Byrne:

> The success of a provocative yet inclusive concept of Third World neu- tralism enabled the great expansion of NAM’s membership to include nearly every country in Asia, Africa, and Latin America, in addition to some European countries, while still retaining the capacity to mobilise effectively this massive coalition behind a single cohesive agenda (927)

## Ada - Contested Hegemony: The Great War and the Afro-Asian Assault on the Civilizing Mission Ideology

[@Adas2004]

* Argument: the discourse generated by WWI was important in re-shaping / undermining the rhetoric of the Civilizing Mission, thus providing avenues for reinvention of the colonized cultures, by marring the espoused virtues and attributes valorized by its pre-war champions
* Civilizing mission:
  * premier means by which European politicians/colonial officials/popularizers/propagandists identified areas of human endeavor of European superiority and could *incontestably* establish degrees that others lagged behind in comparison (31)
  * late Victorian standards of gauging/justifying superiority and global hegemony were envisioned "both empirically verifiable and increasingly obvious" (32) -- tight epistemic community (or 'historic bloc') contestable only through dissident individuals
  * pits imagined European noble male values (esp scientific, energetic, disciplined, progressive, punctual, reserved -- but also taking control) versus imagined Othered (often feminized) values of superstition, indolence, reactionary, out of control, oblivious to time -- obviously "blatantly essentialist [dichotomous comparisons]" (32-33)
  * often argument followed; duty of inventive, inquisitive Europeans to conquer&develop primitive peoples' lands; once achieving political control being incumbent to replace their "corrupt and wasteful indigenous regimes with honest and efficient bureaucracies", thus also reorganize society; and restructure *physical* environment to "bring them into line with European conceptions of time and space" (36)
* male focus of civilizing mission:
  * people deemed martial (Sikhs of India, bedouins of African Sahel) were ranked higher in hierarchy of human type, underscoring masculine bias of desirable attributes within civilizing mission ideology (34)
  * "within colonizers' enclaves, the logic of the separate spheres for men and women prevailed", underscored by apparently paradoxical behavior like Egypt's proconsul (Evelyn Baring) both supporting efforts to 'liberate' Muslim women from veil/purdah in colonies and supporting antisuffragist movements in Great Britain (34)
  * such compartmentalization fixed image and position of European 'memsahib' (passive, domestic, apolitical, vulnerable) and made it "all but impossible for indigenous women in colonized societies to obtain serious education in the sciences or technical training" (35)
* elite-focus
  * efforts at conversion to civilizing mission's virtues "were normally reserved for the Western-educated classes" using state-supported and missionary education epistemologies, values, modes of behavior were propagated "that had originally served to justify their dominance and continued to be valorized in their rhetoric of governance." (37)
  * in turn, Spokesmen for these classes, even when already agitating for ending colonial rules, still clamored for Western education and more integration of Western science and technology (38)
  * thus, e.g. "depite the Hindu renaissance that was centered in these decades in Bengal, [...] Calcutta, had been reduced to an intellectual outpost of Europe" (38)
  * transmission of civilizing mission ideology elite-to-elite means that it worked in different hegemonic sense than Gramsci's original formulation (37)
* in Africa, due to different valuation of 'original encounter' (after slave-trades ragaves), fewer opportunities for pursuit of 'Western' training in sciences, medicine, engineering; and less technological diffusion, training remaining mostly with most rudimentary machines (39)
  * even defenders of African culture "conded that Africa's recovery from the ravages of the slave trade depended upon assistance from nations 'now foremost in civilization and science'", though pinning blame squarely on previous slave trade (40)
* Post WW1:
  * "postwar efforts to restore credibility to the civilizing mission ideology were exercise in futility", having destroyed its pretenses at moral superiority or Western innate rationality (42)
  * mechanized slaughter of trench warfware "transformed machines from objects of pride and symbols of advancement to barbarous instruments of shame and horror.", equaling reversal of Europeans transforming from masters to slaves of their machines (43)
  * values of "take-charge European male ideal" were transformed into passive, dependent, immanent medical objects (45)
  * reversal of savage/noble explorer relationship (46-47)
* Adas argues for postwar discourse being "first genuinely global intellectual exchange" (61)
  * but still largely confined to Western-educated elites (62)
  * also "the cage of language set the limits and had much to do with fixing the agenda of that interchange", shaping sources and their arguments (62)


# 15: Climate Change

## Heymann - The Evolution of Climate Ideas and Knowledge

[@Heymann2010]

* idea-history overview of climate mapped along 3 axes:
  * reversed epistemes of climate itself (changes in space v changes in time)
    * intrinsic linking of epistemes to their imbued meaning on climate (e.g. comprehensive 'classical' episteme of linking 'weather phenomena and cultural phenomena')
    * contept of climate itself grew out of heterogeneous understanding into more synthetic vision
  * anchoring to specific actors & epistemic communities (e.g. different understandings of climatologists, geologists, paleoclimatologists)
    * the formation of these dominant concepts on the basis of social processes and superior social credibility (or cultural authority)
  * understanding of climate needed interpretation; thus dependent on broader cultural context (e.g. technological advances, political demands)
    * e.g. increasing accuracy of measuring instruments necessary (but not sufficient) requirement for systematic ('instrument-based') observation
    * but thus also: scientific findings needed to be framed; imbued with meaning and credibility
* the concepts did not exist / become created in a vacuum
  * rather, scientists & scientific communities needed to give them 'visibility and weight'
  * accomplished through:
    * linking to broader cultural interests
    * reaching popular perceptions
    * transferring/embedding in more general public discourse
* importantly, processes such as these are not *linear* advancements, but odds and ends left over from multitude of diverging areas (often those becoming dominant through their social legitimization by imposing social agency [see climate as interpreted, thus contested, concept])
* the idea that a new 'dimension' of climate has been discovered (and made legitimate) after epistemic shift from classical climatology can be seen as twist on spatial re-formation (and equally, the necessary legitimization of the same;)

## Borowy - Before UNEP: Who was in Charge of the Global Environment?

[@Borowy2019]

* traces genesis of UN Environment Programme, tracing it through the actors considered as driving forces of United Nations Conference on the Human Environment (Stockholm, '72) (89)
  * sees these actors never acting in isolation, but in specific constellations reflecting the national/institutional interests they represent, and "different ideas regarding what should constitute international environmental work" (88-89)
  * e.g. Janez Stanovnik (Yugoslavian Executive Secretary of ECE), Richard Nixon as 'principal movers', Emile van Lennep (Dutch OECD Secretary-General), Manlio Brosio (Italian NATO Secretary-General) -- looking at pathways considered / taken (89)
* issue crystallizing first in international organizations -- representing industrial nations (who were biggest polluters); thus also naturally becoming entangled in Cold War configurations (since they were major participants) (91)
  * organization hinged (as many conferences) on 'upgrading' coherence of io cooperation on the issue (92)
  * '69 onwards, preparations for 2 environmental conferences: one global, one regional (92)
  * global differences in valuation of growth v conservation (esp industrial vs developing nations) lead to global nature perhaps *hindering* appeal of conference rather than helping (93)
* different conference aims also belief different IO aims: (105)
  * NATO favored by US Nixon administration for issue
  * OECD favored by GB administration for expertise and issue closeness (??)
  * ECE only agency to include eastern European members (105)
  * Global South first being (unofficially, simply due to 'nature' of orgs) barred from participation in decisions (105)
* protection aims from reduction of air pollution and information-sharing to more holistic approach over 60s (93)
  * esp ECE and OECD quite critical of growth economy model to be long-term sustainable
* different aims in participation of elevating the issue:
  * field itself was promising to many to further own organization's international prestige (~)
  * efforts "to shape economic-environmental debates according to their understandings of what was required for a sustainable future -- years before the expression had entered the public discourse." (105)
  * rivalry itself created (perhaps) more in-depth studies than what would have been possible without (106)

## Related

[Text Profile - Heymann](2003021851 GHS Textprofile - Heymann.md)


\pagebreak

# Bibliography

\hangingindentworkscited
