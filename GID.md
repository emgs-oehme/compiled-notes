---
title: Global History of International Development Notes
author: Marty Oehme
toc: yes
bibliography: /home/marty/documents/library/academia/academia.bib
nocite: "@Cooper2010,@McVety2018,@Cooper1997,@Engermann2009,@Ekbladh2018,@Kalinovsky2018,@Rostov2008/1969,@Scott2008/1998,@Ferguson2008/1994,@Rodney2012/1972,@Chasse2016,@Finnemore1997,@Kassymbekova2013,@Webster2011,@Hodge2007,@Ekbladh2011,@Pribilsky2009,@Bamba2010,@Maul2009,@Burton2017,@Iandolo2012,@Spaskovska2018,@Schayegh2018,@Castelo2014,@Dimier2008,@Hatzky2011,@Monson2011,@Bockman2015,@Pernet2018,@Zimmer2014,@Kalinovsky2018a,@Gupta1998,@Sackley2011,@Gorsky2017,@Borowy2017,@Boserup2008/1970,@Williamson2008/1993,@Dyson2008/2005,@Ekbladh2011a"
output:
  pdf_document:
    latex_engine: xelatex
---

# Marung - Global History of International Development

## Organizational

[link](https://www.google.com/url?q=https://docs.google.com/document/d/1OW-lQwGU6W-j6HzVRo6avQG8vpQP9SkpjV9Ei12V-jw/edit?usp%3Dsharing&sa=D&usd=2&usg=AOvVaw2F4I_a-suOnhTn-W0gzrjz) to documents to be read

## Handout

* History of organization: when founding, which historical moment, reaction to which challenge, transformations and crises in history of organization
* Mission: what is understanding of development, how relates to mission; have their been transformations
* Projects: what does it do? which type of projects/activites
* Membership: who which countries are members; how can relations between organization and (specific) member countries be characterized? What is staff of organization?
* Structure: how are decisions made?
* Resources: budget, where from, who decides it? Which other than material financial potentials does the organization have to influecne national or internatinoal policy?
* Position: in network of international organizations dealing with development?

# Session 1 - Where does the idea of International Development come from

syllabus:

- session 2
  - texts address the creation of development models (US/USSR) *not* just domestically, but in relation to international power relations / and international voices of dissent (or ciriticism)
- session 3
  - Rostow: often criticized base point from which western perspective of growth stands
  - Rodney: counter-point to Rostow / global view in critical perspective
  - Scott: demonstrates that all *grand* schemes of development will fail
  - Ferguson: looks at institutionalizations of development from a critical perspective
- session 4
  - Cooper: how knowledge itself becomes a powerful device (which is diffused through institutions)
  - Finnemore: showing how World Bank brings together institutions globally to further economic growth model
  - Speich: Where do the instruments of how to measure development come from (i.e. GDP)
- session 5
  - Kassymbekova: Soviet perspective onto soviet western Asia (reproduction of european colonial empire patron-client relationship)
  - Webster: how are development advisers (which previously worked under colonial measures) now employed in development
  - Hodge: transformation of colonial into post-colonial development through experts
- session 6
  - when the different agendas encounter themselves (in competition / cooperation)
  - who are the actors that actually *set* the agenda
- session 7
  - similar agenda-finding in soviet areas
  - socialism beyond just eastern-bloc state developments > how does it lead into new visions of development
- session 8
  - Shayegh: readings already contain transregional perspective, in the Ottoman-Empire
- session 9
  - Monson: the position of China along the global south, building of railway in Tanzania (as refuting newness of development)
  - Hatzky: Cuba as a 'development power' during the Cold War in Angola
  - third text: Yugoslavia
- session 10
- session 11
  - less 'geographically' connected question but more contextually connected: how to 'improve' the countryside
  - how do rural communities become actors on their own in state-building and development
  - Kalinovksy-Gupta: comparable questions on rural developments between soviet and indian 'village' development (find connections)
- session 12
  - how gender/health become important topics toward end of cold war
- session 13
  - new economic world order / OPEC crisis as reshaping the fundamental nature of development projects
  - debt crisis as binding measures (patron-client) and Washington Consensus

end conference:

- shuffling into teams on one specific organization
- during semester come together as group to research the fundamental position
- include 'chairs', with position papers, rapporteurs, present it in a 'general assembly'; delegations and working committees (where one team member will be *sent*)
- we *are* the organization for the final conference (i.e. policy creation)
- find a topic/conflict to be solved (around Christmas)
- look at model UN simulations
- ILO/WHO/IMF/worldbank/AIIB/OECD/FAO/

end paper:

- can be on the organization developed during the conference
- for the topic come to office hours / send e-mail if we need (recommended)
- 10-15 pages (officially)

next week: come up with a comprehensive list of possible orgs
recommended reading: Unger/Engelmann - State of Development

## Unger - International Development A Postwar History

### Guiding Questions

1. What is development? Is there a (universal) definition?
2. What is the origin of development? (reason, spatial, chronological)
   3. Is development a positive or negative connotation today?
4. How does modern development differ from its history / origins?
5. Is there a lot of development currently ongoing?

---

1. a definition of development appears to "encompass all facets of the good society, everyman's road to 'utopia'" (15, cf. Heinz Arndt)
   - prominently economic growth == economic assistance as instrument (16)
   - individual freedom, social equality, democracy

2. 'moral' mission of esp. religious activity attending the 'humanitarian & civilizing' mission (24)
    - first seen as problems 'within' people which should be repressed/worked on/helped (1880-1910s)
    - increasingly moving through increasing structural nature of problems (1930s depression)
    - to ideas of welfare policies (40s onward)

    this was 'not' just a linear progression (48) but confluence of WWII problems:
    - newly divided progress and standards of living
    - divisions created through war, where it took place
    - war-based innovations
    - but also post WWII Cold-War thinking and worldviews (esp bipolar powerstructure & red scares)

4. mostly the idea that social problems: inequality, structural differences - forces 'outside' an individual could equally control their fate as their inner character. The main thing changing over time were the circumstances looked at: esp. ones of racism and colonial superiority slowly faded out of minds

   identifies 2 main strains:
    - imperial powers clinging to thinking about colonies (77)
    - bi-polar, then uni-polar moment shaping US-lead development efforts: (Ch. 6)
      - community
      - agriculture
      - education
      - public health (& birth control)

5. less focused 'development'
    - often more directed at enabling *individual* 'capabilities'; then top-down strucutres (140), esp from 70s onwards

  after neoliberal turn & USSR deissolvement the idea of development itself became increasingly vague - was more structured to align to ideas of market and making these sustainable (143)
  attempts to move away from nation-states as central organizer & facilitator of development
  - in a way putting emphasis *back* on economic growth, just along new axes of mechanisms, actors and instruments (esp market) (145)
  - sustainability efforts intended to make recipients self-sufficient (147)

  discussions of 'dead aid' & 'degrowth' move the question from development: how? to development: why?
  - questions if it helps *at all* (esp with its focus on growth) (151)

## Cooper - Writing the History of Development

arguments:

- tries to establish a (historiographic) definition of Development:
  - from the (announced) death of development ~1980s (5)
  - to recognition of market limits and state intervention as [panacea] (5)
  - to more market oriented/ individual (bottom-up) approaches (5)
    - micro-credits for small enterprise (5-6)
    - but also still large-scale regional projects (6)
  - critiqued and criticized by deconstructivist discurse (6)
    - as reproduction of existing power hierarchies
    - as naturalizing Western modernity
- but (realist argument): "critiques do not bring piped water to people who lack it" (6)
  - advocates for a pragmatic balance between the two:
    "each also has merit - the former in keeping open the possibility of finding solutions to problems, and the latter in tearing down the arrogance that often attends attempts at wide-scale reform." (7)
- orientation "toward local initiative and improvisation did not necessarily produce happy results", when it "took the form of warlords organizing local armies, of underpaid civil servants organizing rackets [...], or of structures of [...] dependence becoming entrenched." (8)

development history:

- began in the colonial 'civilizing mission', often "subsumed under a rhetoric of European-directed progress" (9)
- many colonies (after 1920s more conservative govts ceased funding development efforts) were still connected to their European countries, but "hardly fit a tribal model of colonial society" (10)
- conception remains rather stable: "state projects, channelling resources in ways that the market does not, with the goal of improving the conditions that foster economic growth" (10)
- in coming from colonial thinking to present, the development idea came "to grips with the fact that markets in themselves do not necessarily provide the conditions for economic growth and human welfare" (20)
- in the decolonizing mission, countries granted various 'citizenships' to their previous colonial subjects, with France going the furthest into a wholly equal relationship. (12) (mostly due to Free France being helped massively by the Colonies, see Middell GHS)
- the dilemma - integrate or forcibly 'modernize' - is exemplified
  - by Feichtinger and Malinowksi in the "coerced relocation of thousands of Algerians [...] into controlled villages", where they undertook 'forced modernizations' (12)
  - by Kenyas 'stabilization' programme to urbanize their workforce, and turn them into an example of nuclear families (akin to European workers) (13)
- still ultimately "ethnocentric: the end point of modernization was widely perceived to be European or North American society." (14)
- the increasingly perceived 'entitlement' of ex-colonies "turned into supplication", and independence negotiations largely existed on this background of "cost-benefit analysis" (15)
- but it also allowed former colonies to shop around for new patrons, of which the US tried to establish itself (at least ostensibly) in a leading role. (15-16)
- afterwards development efforts varied (according to the contemporary 'western' economics), "balanced growth, unbalanced growth, growth with redistribution, basic needs, structural adjustment, sustainable development, microcredit [wandered] along tendencies to emphasize 'one size fits all'" and looking towards the newest interesting theory. (16)

- the 'client-patron' relationships are a remnant of colonial politics, and the "interface between national and international economies" became points of contention between newly arriving 'gatekeeper states' and their patrons. (18)
- in this climate, "capital, expertise and long-distance connections [were turned] into a resource by governing elites; concentrating resources" instead of making them available (18)

going forward:

- "Sharing ideas and expertise across regions - 'South to South' - may play a bigger part in present and future development efforts than criticism of an all-effacing 'North' might allow" (20)

central conclusion:

- "point to the importance of questions of who intervenes, for what reasons, through what relationships, and to what effect. [...] Generalized praise or denunciation" does not help circumvent pragmatic case study, because "'community' no more represents primal innocence than does the IMF." (20)

## McVety - Wealth and Nations: The Origins of International Development Assistance

main argument:

- the emergence of 'the economy' as an idea, and its national income accounting; war devastation and international relief action; and the outbreak of the Cold War helped entrench international development as "a standard part of post-World War II international relations", with a specific narrative centered around the respective economy, capital investment, and technological innovation. (23)

- Sun Yat-sen sent call for International development plan around the world (esp to US) but was rejected, after the end of WWII the US would create a committee solely centered around that theme. (21-23)
- From 'development' (i.e. social progress based on material change) Smith et al created an idea that culminated in the 'civilizing mission'.
  - Japan's rise questioned some of the racist aspects of the idea, but solidified the idea that "the triumphs of science and industry" needed to be made use of to 'develop' a nation. (24-25)
- esp after Great depression economic growth became the primary indicator of nation's development efforts (26-27)
- national income estimates were now routinely compiled in the aid of finding comparative data on standards of living - 'a lens through which to see the world as an object to be improved by liberal capitalism, western science, and international organization.' (27, cf. Clavin 172-179)
- this data changed the way many nations/empires would (had to) approach the previous 'civilizing mission', into one of capital, but also one of 'positive security' (esp. living standards) (28)
- the Atlantic charter shifted toward economic growth as one primary way of attaining and securing lasting peace (30-31).
- with UNRRA (United Nations Foreign Relief and Rehabilitation Operations), created out of a US office to help provide immediate postwar aid through food, clothing, shelter, later return of prisoners homewards, and finally "'restoration of essential services'", as well as resumption of economic processes - international relief shied away from changing into development (31)
- The Marshall plan then changed this immediate relief into long term emphasis on economic development in any underdeveloped areas - with a coordinated and expanded approach. (35)
- even postwar ideas of development remained firmly centered around "national economic growth [and] increased standards of living" (38)

### References

Amanda Key McVety (2018) 'The origins of international development assistance' in Erez Manela & Stephen J. Macekura (Eds.) *The Development Century: A Global History*, Cambridge, UK: Cambridge University Press, pp. 21--39

Patricia Clavin, *Securing the World Economy: The Reinvention of the League of Nations, 1920-1946* (Oxford: Oxford University Press, 2013)

# 4 - Development as Knowledge-Power-Regime

## Cooper, Packard - Introduction

### Directions of knowledge

* orthodoxy: "foreign aid and investment on favorable terms, the transfer of knowledge of production techniques, measures to promote health and education, and economic planning" to make impoverished countries 'normal' market economies (2)
* underdevelopment theorists out of Latin America arguing global exchange itself widens rich/poor rift (2)
* Marxist theories analyzing global capital accumulation in development (though ending up at different directed change being the answer) (2)
* rejection of the entire development framework (2-3)
  * ultramodernist theorists - completely trusting the market to produce an optimal allocation of resources
  * postmodernist - development as extension of (imperial) apparatus of control and surveillance

* Ferguson (1990) proposing controlling discourse btw state & intl agencies: (1)
  * both accept each other
  * nat gov allocates resources and becomes beacon of modernity
  * intl agencies intervene in sovereign states but legitimize it as aid

### Semantic Ambiguity of 'Development'

* the world development (4)
  * implies notions of increased output
  * notions of improved welfare
* appears like discourse of control *at one level* or entitlement at another

### Colonial to Less Developed

* colonial idea of "other" people needing to adopt 'modern' ways of living was adopted into development discourse
* GDP and national statistics to compare firmly established nation-state as base economic units
* World Bank (WB) and IMF expanded from financing European recovery & financial stability 40s to intl development 50s
* however during this time also multiple knowledge-power-regimes came into being
  * structuralist center-periphery approach of Prebisch et al (10)
  * dependency theory seeing the *cause* of underdevelopment in development linkages
  * critics of 'modern' nation-states could still
* also a time of uncertainty around a lot of issues (11)
  * which also opened avenues for discussion for newly independent states
  * esp some late-comers could use the existing knowledge frameworks and either
    * support their implementation
    * or lean against the orthodoxy (as intellectuals in Africa did ~1950s) (12)

### References

Frederick Cooper & Randall Packard (1997) 'Introduction' in Frederick Cooper & Randall Packard (Eds.) *International Development and the Social Sciences: Essays on the History and Politics of Knowledge*, Berkeley: University of California Press, pp. 1--41

## Chassé - Roots of the Millennium Development Goals

* research question: can a similar co-construction as that of national institutions in GDP and others be found in the global analysis of statistics?
* result:
  * the (massive) reduction in complexity through statistics necessarily renders biased images of the world
  * internationally no single center of calculation started to emerge (as opposed to national)
* areas investigated: (221)
  * 1960s East Africa, with intl actors such as UN
  * rise of modern statistical imagination produced chaos where the local could not cope with its impression onto it - creating in a sense the 'non-Western world', esp highlighted by colonial bureaucrats
  * the intrinsic connection of statistical tools of governance to political form of modern nation-state

* making statistics the object of analysis themselves
  * "statistical facts are the result of conventions and still work as the real" (220)
  * being linked to centers of power, governmental authority has increasingly been expressed through statistics (by being both conventional and real)
* vital in nation-state building project itself
* e.g. African nationalists - appropriated promise of modern technology to reproduce mode

### References

Daniel Speich Chassé (2016) 'The roots of the millennium development goals: a framework for studying the history of global statistics', *Historical Social Research / Historische Sozialforschung* 41, 218--237

# 5 - The Triumph of the Expert

## population growth and development discourse

* 2 thinkers of (late colonial) development
  * Albert Sarraut - key thinker/scholar on French colonial development efforts (20-30s)
    * "la mise en valeur" - idea of reorganizing the empire to increase its profitability (*not* to make independence easier)
    * new idea of *integrating* the colonies into the empire (build infrastructure, *increase population*) to siphon its profits (seeing them as future economic powerhouses)
  * August Seidel - key German pro-colonizer working in colonial administration (not scholarly work)
    * writing about Cameroon (then colonized by Ger; later split between FR/GB)
    * arguing for the propagation and *need* for colonies (against Bismarck's unconvinced administration)
    * key idea - prove the European/German superiority by bringing the civilizing mission (and be *recognized* in League of Nations)
    * shows the production and institutionalization of specific (administrative) knowledge regime
* seen sometimes as precondition in late colonialism
  * population growth as precondition of the development (August Seidel, 1906), only densely developed areas are able to develop industrially in his view
  * he *laments* population sparsity which means development possibilities are not available
  * 'missing the material necessities', population as a resource (Sarraut)
* this stands in stark contrast to the British (perceived) problem of overpopulation already
  * the *perception* of pop growth in GB vs FR/GER stark difference, even though facts are similar
  * migration from rural -> urban already
* lamenting the missing *indigenous* population ('race', in Seidel's words)
  * esp Albert Sarraut arguing for *export* & integration by immigrant (i.e. European) population
  * export of workforce in GER colonies stands in contrast to GB 'settler' colonies, where expats are more 'integrated' (though that is probably the wrong word choice)
* both Seidel & Sarraut compare the development to the colonial project of Great Britain

## The Expert

* an ascribed function (through institutionalization/fellow voices/ some process of legitimization)
* someone *recognized* as expert requires an audience or some position of authority
* there's the additional question of generalization v specialization - who recognizes your expertise earlier (esp the more niche you become)
* since experts are legitimized through institutions, they at the same time need to promote their own agenda *within* an institution, be on the market for a more influential position in institutions
  * institutions and experts can be cooperating or in a struggle when conflicts of interest (or agenda) arise
  * the de-politicized nature of status-quo knowledge delineates it from 'outside' political knowledge

## Webster - Development advisors in a time of cold war and decolonization: the United Nations Technical Assistance Administration, 1950-59

* technical assistance posing a global framework (both for capitalist and socialist nations) in 1950s to exude their soft-power
* describes conflicts in Indonesia by national technical frameworks being (attempted) supplanted by foreign Technical assistance
  * ultimately they are pushed out of Indonesia in favor of national
* question centering around the imagined versus the actual expertise of what technical assistance can provide
  * constant tension of expectations of where expertise and development should lead to
  * further clashes in how goals are perceived by nationals (those 'to be developed') and those who are undertaking the project

## Kassymbekova & Teichmann - The Red Man's Burden

main argument:

* "Since the power of the Moscow state leadership relied on "extraordinary measures" instead of bureaucratic institutions and procedures, a despotic exercise of power and mobilization campaigns became the main tools of governance in the peripheries of the Soviet state."
* Those affected included "European officials [who] were also vulnerable representatives and potential victims of the new Soviet state [and] neither European party bosses nor government officials were immune from the Bolshevik state's hefty and violent reprisals." (165)

* state apparatus in Central Asia resembled standard Soviet model: parallel structure of government institutions and party apparatus, with 2 additional layers: (167)
  * Central Asian Bureau of the Central Committee of the Russian Communist Party (*Sredazbiuro*) - influential, of in conflict with national rule (governments and party apparatuses)
  * secret police, state and party control commissions - which oversaw these conflicts and kept them in check (through administrative and violent means)
* this hierarchical structure bears *strong* resemblance to colonial administrative institutions (167-168)
  * reflected in prominence of European decision-makers in government positions, backstage positions of Asian Bolsheviks
  * reflected in 'dual secretary system', with indigenous first secretary, nominal power, and European 2nd secretary
* these state institutions, staffed with foreigners to the land, understaffed and under-financed with vague instructions were expected to improvise a revolution - turning to despotic means (169)
  * "almost insurmountable distances" to the Soviet center, no reliable infrastructure, communication, bureaucracy, or rule of law meant the repeated use of ("arbitrary") violence and despotic means to enforce Soviet rule
  * also major source of failure of Soviet colonial project, instability and insecurity (170)
* the idea of it being simulateneously being a project of colonialism and post-colonialism (Marung) - by pretending to be the experts and civilizing the periphery but *also* being a constant own reflection and perhaps civilizing themselves - simultaneity of appearing backward and highly advanced

### Official's Situation (in Dushanbe, Tajikistan)

* housing, office space was incredibly sparse for European officials (171)
* poverty, since salaries were insufficient for survival (171-72)
* malaria struck many of the periphery regions ("main enemy of Soviet rule") (172)
* to leave the dire situation, many got issued medical certificates (since otherwise it would be a desertion of word). Doctor's were increasingly instructed by the government to only do when absolutely necessary - or not anymore at all. Finally, in the late 1920s, officials' passports were confiscated as soon as they arrived. (173-74)
* officials also had few social networks to fall back on, with high turnover rates in government offices (175)

### Cotton Revolution

* beginning in 1930s, nationalization of farmers' lands to plant cotton began (176)
* goals were impossible to fulfill and resisting (by planting e.g. rice) lead to being shot (176)
* thousands fled to Afghanistan, Persia (176)
* farmers demonstrated by local offices, officials were beaten or killed in what "turned into a messy civil war that claimed thousands of victims on both sides." (177)
* others joined local rebel bands and began killing Soviet officials when government requisition brigades arrived (179)
* The Soviet state threatened to implode with militias not receiving salaries, moving over to rebel groups; Soviet officials being killed by the populace,.. (180)
* responsibility "was always a personal affair" in the Soviet state - conferring broad powers, but coming with personal accountability (182)
* many of those persecuted were European officials - signifying their changing position since being the dominating force in a 'colonial' system (183-84)
* in creating a narrative of continued power, Moscow used violence in peripheries filled "with discontent, failure, and widespread suffering." (186)
  * (Marung) - even though it is so horrible and cost so many lives - can it not be argued that the project of industrialization and development effort actually worked out pretty well?

### References

Botakoz Kassymbekova & Christian Teichmann (2013) 'The red man’s burden: soviet european officials in central asia in the 1920s and 1930s' in Maurus Reinkowski (Ed.) *Helpless Imperialists: Imperial Failure, Fear and Radicalization*, Göttingen: Vandenhoeck & Ruprecht, pp. 163--186

## Hodge - The Triumph of the Expert

* the 'development project' would continue colonial biases and development structures along state-centered ideologies long after the colonial project's admitted failure (255)
* international programs and institutions of development are deeply infused by colonial historical constructions, reproduced by experts who transferred from one advisory body to another (255-56)

* example in FAO survival after "world food plan" was instigated by Lord Boyd Orr and essentially 'imperial' measurement of needs; plan was abolished but FAO became a leading agency (256-57)
* UNESCO being originally headed by Julian Huxley, previously on Education Advisory of the Colonies (~1930s), to reconstruct large-scale planning through population planning, birth control, wildlife, nature conservation (257)
* similar patterns seen throughout WHO, ODA, ODM, CDC, HTS, UN, World Bank, FAO, ...

* most enduring legacy "of the late colonial development initiative was the intellectual one" (261)
  * developmental, environmental narratives, policy strategies became embedded and institutionalized in the international aid agencies and organizations which were clearly driven by late-colonial "concern about the emergence of a surplus population" (261-62)
* much of the development effort was infused by the paradox of colonial rule; so goal was not to replicate image of West, but to manage transformation in front of contradictions of "private capitalist enterprise and colonial rule": (263)
  * emergence of surplus population of rural & urban unemployed
  * loss of productive resources
* thus, development efforts carried deep agrarian bias, and tried to depoliticize the question of labor and wage (i.e. poverty) (264-65)
  * most of economic devel efforts were undertaken as answers to this dichotomy (which threatened the political stability of many erstwhile colonies) (266)
  * this was undertaken in front of a stage-based discourse of breaking through the 'agrarian revolution' to enable industrial conditions for the countries (267)
    * agrarian bias: idea of import substitution vs export driven development (producer of crops etc for Europe), and control the population (stabilize them, keep them from urbanizing)
    * see e.g. Lewis model of structural change based on industry, v traditional agrarian narratives (*both*) of which are seen critically from today (268)
    * also e.g. in green revolutions spread of HYVs, but too costly of fertilizer & irrigation, which thus did not spread to smaller, poorer farmers and ultimately increased income inequality & rural unemployment (269)
* even after many failed attempts, experts opinion still formulated around rural stabilization due to (depoliticized) poverty, which stipulated development agendas (into 1980s) (272)
  * in many ways the market-based, FDI heavy development efforts starting from the 90s (and ostensibly intending to bring stability to region) mirrored earlier colonial efforts at labor exploitation and resource pilfering in the name of development (275-76)

### References

Joseph Morgan Hodge (2007) 'Conclusion: postcolonial consultants, agrarian doctrines of development, and the legacies of late colonialism' in *Triumph of the expert. agrarian doctrines of development and the legacies of british colonialism*, Athens, OH: Ohio University Press, pp. 254--276

# 6 - Competing Agendas - The US Project

## Harry Truman's US development agenda - Inaugural Address January 20, 1949

### what are the specific points he mentions

* live together in peace and harmony, lasting peace by treating all as equals
* other nations look for strength and wise leadership
* right to freedom of thought & expression, created equal (in the image of god)
* freedom to govern themselves for all nations
  * reason and justice to govern democratically
  * for the benefit of the individual to exercise the abilities of each person
* democratic principles for international relations
* worked for limitation and control on armament
* world trade on 'fair' basis; greatest economic project in history
  * purpose is strengthening democracy in Europe
* 'structure of international order and justice' - institutions of liberalism
  * now begin new programs to strengthen this world

### 4 Points, and Point 4

  * support for UN and related agencies; strengthening their authority
    * which is strengthened by the new nations
  * world economic recovery programs
    * European recovery program; as major venture in world recovery
    * reduce barriers to world trade - peace depends on world trade*
  * defend 'freedom-loving' nations against encroach of non-democratic forms
    * support for NATO establishment (treaty of rio de janeiro)
    * common defense, idea of overwhelming force (defensively)
    * military advice and equipment, in return for cooperation
  * new program for scientific advances and industrial advances for underdeveloped nations
    * we 'lend' our expertise
    * to primitive and stagnant countries to lead them toward modernization
    * decrease suffering
    * inexhaustible technical knowledge and expertise; in cooperation with other nations
    * help nations in their *own* efforts
    * support food, housing, electricity
    * using UN and specialized agencies
    * goal: achievement of peace, plenty and freedom
    * cooperation of business, capital and labor to increase prosperity (reinforcement of capitalism)
    * balancing guarantees for the investor and for the goals of the people in respective nations
    * more efficient exploitation of natural and human resources
      * abundance
    * greater production is key to stability and peace - from domestic to international
      * support international peace from a domestic national position of strength
  * freedom of speech, of religion, of (pursuit of happiness)
  * buzzwords of justice, harmony and peace

### Embedding in larger historical narrative

* 'special challenges of this period' - a decisive period for the world
* a turn after the 'brutal attacks' of WWI-II
  * a new political thinking after the 'trauma' of the 2 world wars
* ideals which inspired this nation from the beginning
* we have imposed our will on none, not done anything which we asked from others
* saved countries from colonial structures
* heaving countries out of historic unmet needs, suffering, deprivation
* this will bring new duties for US
* distancing himself from 'old' imperialism
  * but very cautious to concretely position himself against colonialism
  * still part of a world of empires when making this argument (Marung)

### Development and Communism

* US and other 'like-minded' nations, opposed by another regime adhering to a false philosophy (i.e. communism)
  * which purports 'false' freedom through deceit and lies
  * punishment without trial, justice only through violence
  * hold division into classes (as opposed to democracy)
  * collectivism vs individualism in comparing the two
* democracy
  * freedom
  * right to govern themselves
  * peace and stability
* idea of 'free-nations'

## Ekbladh 2011 - The American Mission

* traces the development of aid efforts throughout Eisenhower administration from free market primacy toward consensus approach and state planning in modernization
  * especially after an 'aid offensive' by the USSR in the mid-1950s

### TVA

* TVA - (Tennessee valley authority), text looks at how this model is diffused by the US development efforts worldwide
* developed in the US south during the depression
* model of transformation created for people who are already 'Othered' in these places - already *defining* the targets as backward, so that they can be lifted out of their problems
* the modernization effort being a holistic effort of progress
* including education programs (expertise, alphabetization, family planning), local communities
* working with local governments to strengthen institutions (and enable them to participate in 'modern' industrial economy)
* its success might be dependent on its foundational focus of 'overcoming backwardness' (similar to the Soviet project starting in East Asia)

### Trade not Aid

* Randall Commission 1953 report suggested turning priority of foreign aid around a policy of free trade (155)
  * continued support but less focus on multilateral strategies
  * increased emphasis on military aid and military pacts
  * reformulation of many previous (Truman) points of modernization in aid efforts (156)
  * a still changing administration was unsure of how to proceed with existing aid organizations, forming and reforming them (156)
  * Generally, international frustration prevailed over focus on Trade (157)

### The UN

* recommending mainly state actions: increased capital formation; domestic institutions to develop infrastructure, industrial, agricultural, medical, human capacities
* controlling population growth
* all done through (decentralized) planning
* development efforts increasingly studied through the eye of their *social* impacts, beyond the economic boons (158)
  * recognition of the relationship of social stability and development efforts ("not solely an economic question but also a critical mental health issue")
* replacement of many of the New Deal emphases on public good by private actors with a corporate outlook (i.e. profit generation, or profit extraction)
  * but, the main premise was still marked by continuity: to "release the 'creative energies of men'", giving locals chances to become part of the process of change (161)
  * thereby they would be empowered towards technical talents, new technological skills; leave traditional "backwards, ignorant" outlooks behind to participate in modern society (161)

### Development Efforts

* example of Indus modernization project to 'defuse' India-Pakistan situation (with Nehru as the 'modern' instigator for his society's integration) (162)
* the state/individual dichotomy (swapping over from socialism/capital discourse) also put tension on consensus development which favored state support instead of individual empowerment (163)
* state planning being the prime method of accomplishment seems interesting in regards to general opposition to Soviet economies - "statist efforts [seemed] acceptable, even attractive" in racing to develop states into modernity (164)
  * Aswan dam development as reflection of Cold War efforts in juggling development goals (167)
  * there is an inherent tension to the TVA development model based on state planning
* US saw the Soviet development program, under the Chinese-USSR economic 'offensive' on developing countries, transplanted institutions together with social and economic changes to further *their* project in the Cold War - communists selling a 'Forced Way' of social organization (169-170)

### Actors

* American philanthropies
* American government
* American religious groups

* > the mutitude of ideas and influences of the NGO actors were not focused enough to 'keep up' with the Soviet project of development
  * the Eisenhower administration returned from focusing on trade and letting private entities steer the ship
  * into general state-based development efforts

## Bamba 2010 - Triangulating a Modernization Experiment

* nationalist enthusiasm from post-colonial nation-building allowed the Kossou dam to be built (66)
* ruling elite in Ivory Coast stated that electricity from dam would help secure welfare and provide basis for national industrialization *which was considered precondition for creation of modern states* (67)
* this dam was now built with the effort of multilateral relationships, not just its (erstwhile) colonizers (67)
  * this can be a case study on how US ideas were transferred through politics of triangulation

### The American Modernization Teleology

* American developmentalist strategies operated as subtle critiques of European colonial powers' civilizing mission (69)
  * they propsed new teleology: equation of mechanization with modernity, reliance on local expertise of engineers
  * thereby also supplanting civilizing mission of paternalistic domination with one of tutelage
  * (of course while still operating on assumptions of exceptionalism)

### Triangulating

* French still sought bilateral agreement
  * became mediators in ostensibly US-Ivorian modernization project
* these competing national interests allowed colonial subjects / postcolonial leaders more room for opportunities to "rearticulate *their* visions of modernization and nation-building." (82-83, emph mine)
* the initial refusal by French agencies then coming to be accepted and making the blueprint successful in general
* problems and challenges also arose out of these ridges and could, in the case of Kossou, not be overcome (83)

# 7 - Competing Agendas - The Socialist Project

## Seminar

### Iandolo

* how is *development* filled with *content* - what is the goal of development; how are the fields of action defined; narratives; theories
* which actors are figuring most prominently - who are the promoters
* what are the strategies and practices of development
* what are the tensions between other models (esp US) / but also within model

### Actors

* phase 1, engagement
  * Nkrumah/ Tour
  * Khrushchev - *globalizing* the Soviet mission and socialism; turning against the ideological narrowness of Stalin
    * reorganizing Soviet agriculture itself (relates its modernization to ultimate goal of industrialization)
  * Ghana: British cocoa industry, imperial gov structures
    * imperial power wants to control the former colony and its relations
    * old cocoa beans company still keeping
  * Guinea: French gov blocking international relations, Guinea turning starkly away
    * imperial power actively blocks and destroys existing infrastructure and expertise
  * Benediktov (state farms minister) - expertise; selling 'model' of soviet collective farms
    * similar function to Lilienthal
* phase 2, reassessment
  * Belgian mining company (state owned), Belgian government - European government
  * Lumumba & Tshombe (as succeeding leader)
  * UN - as intervening in the crisis itself
* phase 3, disengagement
  * Nkrumah
  * Touré

### Strategies & Tensions

* phase 1
  * Soviet - strategy
    * of using Ghana as role-model of Soviet development (farming techniques/collectivization)
    * way of returning interest - with long-term interest; for commodities (flexible loan system)
    * limiting foreign capital input; state-provided means not private actors (nationalization)
* phase 2
  * tension - between imperial structures / colonial government leftover/blockades
  * tension - 'true' Marxists
  * tension - reliance on local actors for expertise / but then turned against them for inefficiency
  * tension - vision vs material means
  * tension - material needs (esp hard power) and Soviet logistic possibility
* scales
  * local - Nkrumah/Touré's use of the funds; imperial leftover structures
  * regional - the new local power configurations through Congo crisis; Belgian support for its mining company
  * global - remaining influence by colonial powers;

* development goals agricultural development to fund and further industrialization (through infrastructure)
  * forced march modernization (through mechanization)

## Spaskovska - Building a Better World?

* more focused on the *inter*-national relations themselves
  * the movement of national governments to form alliances and (regional) blocks
  * not necessarily focused on redefining economies globally
  * rather to change the *global hierarchy* itself
  * dual position of e.g. Yugoslavia/Romania who put themselves into both developing and socialist country, i.e. develop while developing; blurring the clear lines
* attempts to outcompete capitalism/socialism by finding entwined version of both
  * began well by Yugoslav workers first still being cheaper than in other countries
  * as they became more expensive, they had to shift means
* workers' owned cooperatives then invest in development projects
  * means of development are technical assistance given by the companies
  * this is already another way of *doing* development than US/Soviet project
* solidarity of movement began to erode in 80s
  * after OPEC the debts of the developing countries could not be repayed to Yugoslavia
  * OPEC itself shaping the way this development *can* be done; i.e. they are losing all material means and have to bow to Washington consensus (reflects in e.g. Venezuela, and debate around socialism - non-socialism)
  * attempts to repay in oil, but this fails as well
  * questioning ideological goal / extent of the mission
* Yugoslavia's increasing nationalism put the final spanner in the works (??)
  * question of development through/with *trade*
  * making profit of development, through necessity of debt-crisis
  * and how much it is still a socialist project

## Burton - Socialisms in Development, Revolution, Divergence, Crisis

* uses story of Ali Sultan Issa, politician from Zanzibar, to tell 3-staged story of socialisms (4)
  * entanglement shows transnational pathways of socialism & plurality of models and ends to be used for (esp between revolution and state-led development) (5)
  * as seen in last session, even US/Western efforts at development were highly entangled in socialist efforts, going beyond divide along Rostow's Anti-Communist Manifesto (5-6)
* divides socialisms into 3 periods: 1917-45; 45-73; 73-91 (6)
* *Seeds of Revolution* (1917-1945)
  * Stalin practiced theory of "primitive socialist accumulation", collectivized and (re-)subjugated peasantry to enable Soviet industrialization (in extractive relationship) (7)
  * international communist intellectuals had active exchanges internationally (esp in Western urban portals), see  e.g. Portals of Globalization session GHS; Goebel - Capital of Men without Country; Manjapra - Communist Internationalism and Transcolonial Recognition
  * only after colonial rule ended, could socialism move from strategies of revolution to strategies of development (8)
* *Divergence and Competition* (1945-1973)
  * argues that Soviet project, born from Soviet-dominated project, now moved to pluralized and fragmentation (8) - this goes against e.g. Manjapra in his varying interpretations ??
  * Sino-Soviet split (1960) signifies these divergent views, bilateral relations collapse and all Soviet personnel is recalled; Soviet Union now viewed by Chinese as bad example of socialism
    * divergent ends: "Soviets believed that anti-imperialism was a means to combat capitalism and achieve socialism"; pivots primarily around *class*-struggles (10)
    * "For the Chinese, on the other hand, [...] anti-imperialism took precedence over anti-capitalism."; socialism became means to end imperialism and white supremacy; pivoting around *race*-struggles (10)
    * Cuba lead militant, anti-imperialist socialism, in concert with Algeria who both exported radical ideas and revolutionaries (10)
    * often esp Eastern European countries pursued their own *national* aims in lending socialist aid (12)
  * similarities between Western 'development aid' and Eastern 'socialist aid': (10-11)
    * subscribed to newly emerging system of international relations (via national sovereignty & interests)
    * saw own spread as success, saw world moving in 'their' direction
    * state would be main actor in development and planning
    * fostering industrialization
    * attempt pulling receiving countries into own trade networks (and thus spheres of influence)
  * differences in 'development aid' and 'socialist aid': (11)
    * quantitative, western aid trump socialist (~10x more material)
    * socialist aid thus "lacked substantial economic basis in the form of trade", and thus often marked by failures to satisfy both sides
    * relationships were short-lived (often intense), e.g. Western Africa (see Iandolo)
    * sites of exchange and strategies of knowledge transfer became significant; focus on cultural cooperation (12)
* *Crisis* (1973-1991)
  * to to economic restrictions, East had to increasingly pursue 'realist' trade policy, thereby perpetuating colonial international division of labor ("Soviet's allies and dependents never walked on their own feet", cf. Hobsbawn 1994), (13)
  * economic crisis of socialism was recognized relatively late; politically socialism was still winning into mid-1970s (especially Soviet version)
  * internal struggle with neoliberal ideas potentially taking hold *within* socialist world (15, see Pettinà and Kalinovsky, same issue; Johanna Bockmann 2013)
  * forced by "internal and external pressures to abandon socialism and initiate socioeconomic transformations [which] dismantled the pillars of socialist development and led to the abandonment of socialist ideals." (15)

> The crisis of socialism remains unresolved, and so do the structural contradictions -- social and global inequality, exploitation, unemployment, sexism and racism -- which socialist strategies of development have set out to overcome, with all their ambivalent outcomes, ranging from unprecedented advances in social welfare to mixed results in the political and economic realm, to outright humanitarian disasters. (16)

### Related

[1911011333 GHS Portals of Globalization.md](1911011333 GHS Portals of Globalization.md)
[1911011416 Goebel - The Capital of Men without Country](1911011416 Goebel - The Capital of Men without Country.md)
[1912030929 Manjapra 2010 - Communist Internationalism and Transcolonial Recognition.md](1912030929 Manjapra 2010 - Communist Internationalism and Transcolonial Recognition.md)

### References

Eric Burton (2017) 'Socialisms in development: revolution, divergence and crisis, 1917-1991', *Journal für Entwicklungspolitik* 33, 4--20

## Iandolo - The Rise and Fall of the Soviet Model of Development in West Africa

* Ghana and Guinea as tests for exporting the 'socialist model of development' (684)
* ultimate results: model unsuccessful, lasting impact on Soviet consideration of its policy in Third World
  * assumptions of failures, unreliability of local allies (and incompetence)
  * highlighting of 'hard power' necessary in building influence in Third World
  * reflects itself 10 years after end of model, in new Soviet militarist Third World policies in 70s
* *phase 1: engagement* (1957-59)
  * after confidence in Soviet economic strength, Khrushchev ambition to 'peaceful coexistence', favoring competition between economic models (685)
  * turn away from Stalinist fundamentalism in working together only with socialist leaders; akin to foreign policy ideological 'revolution'
  * After independence of Ghana, Soviet state turns attention to provide development replicating its own model of expanded agriculture to industrialization (687)
  * Britain & US exercise considerable pressure on Ghana government to keep distance to USSR (688)
    * London's influence remained strong anyhow: left-over modeling after British institutions, British local media, British general army commander, British-educated ruling class (689)
    * remained (in Soviet assessment) 'typically colonial' in its economy, export of single commodity (cocoa beans) making dependent on Britain (689)
    * slowly, became closer partner to Soviet state (694-695)
  * Guinea, after independence 1959, cut ties to France; seeks Soviet assistance which it gladly provides
    * US is hindered by French intervention and pressure (example of West-West blockade and powerplay, see [Bamba 2010](1911261841 Bamba 2010 - Triangulating a Modernization Experiment.md))
    * USSR became main economic partner, and international ally (693)
  * concept of development: modern, mechanized agriculture with collective and state farms; investments into infrastructure and industrial plants; protection of industry by limiting foreign capital and nationalizing enterprises - "state was to be the only engine of growth" (692)
* *phase 2: crisis and reassessment* (1960-61)
  * Congo crisis showed that West would use military efforts ('hard power') to prevent socialist aid (695-697)
    * esp Lumumba murdered after coup, in concert with CIA efforts
    * under the watch of UN peacekeepers;
  * this stifled belief that competition "had shifted from the military to the economic plain", removing confidence in development model as singular pillar (697)
  * Ghana wanted to orient closer to USSR, but they started offering worse (or rather, now relatively more fair, thus harsh for Ghana) trade deals and smaller development offers (699)
  * Guinea invested money into (so Soviet idea) prestige projects instead of economic development, ceased funds (699)
* *phase 3: disengagement* (1962-64)
  * Nkrumah (Ghana) followed 'Nkrumaism' as specific African form of socialism, which Soviet state regarded w/ suspicion (701)
  * Touré's (Guinea's) behavior showed Kremlin that economic cooperation with newly independent countries was difficult and costly (699)
  * Congo crisis demonstrated that West would not allow expansion of socialist model without intervention (699)
  * ties with Guinea long cut, dream to turn Ghana into example of economic model effectiveness also basically disappeared by 1964, due to now reigning skepticism of local leaderships (700)
  * rhetoric of development/modernization basically disappeared over time, 'social model of development' faded away with fading of Khrushchev era (701)
  * terms of struggle were reassessed: "no longer economic competition [...] but conventional warfare, where the West still had a clear advantage over the USSR." (703)
  * 2 key lessons for Third World:
    * "local allies in Third World had to be, in principle at least, Marxists" (704)
    * understanding of crucial role of conventional military strength as means of gaining influence (704)

> This project, however, failed because of a combination of local and global factors. Poor coordination with the West African governments, together with the 'ideological unreliability' of their leaders, drove up the costs of Soviet economic cooperation with Ghana and Guinea. Moreover, the US's renewed determination to compete with the Soviet Union in the Third World [...] showed Moscow that the West would not tolerate any significant expansion of Soviet influence in the Third World. In the face of growing expenses and increased risk of open conflict with the West, the Soviet Union chose to back down. (704)

### References

Alessandro Iandolo (2012) 'The rise and fall of the ‘Soviet model of development’ in West Africa, 1957–64', *Cold War History* 12, 683--704

# 8 - Postimperial Agendas

## General Assembly Resolution '74

* what is the structure of argumentation
* how can we apply this for our case / organizations?
* how are the logics of the international system changed (the 'rules of the game')
  * linking trade and aid / development - not just specific territories / people (possible similarity of post-colonial/socialist)

### text:

* structure
  * preambulatory clauses - goals / the *why* (and introduction existing resolutions)
  * operative clauses - implementation / the *how*
* the conundrums of the resolution lie in part respective to which framework has been less clearly established previously
  * operative clauses in e.g. UN where they refer back to existing resolutions
  * preambulatory in e.g. OECD where sometimes no clear existing resolutions exist
* make clear as a goal the redress of injustices, eliminating of widening gap *irrespective* of economic systems (??)
* adressing problem of system being created in times of inequality; but still connecting it staunchly to economic process (and system angleichung)

### conference:

* we may work more around preambulatory reconciliation than operative details ??
  * solving the responsibility of different organizations instead of implementation
* 'unfolding the potential of nuclear energy on the background of a changing climate'
  * Angola '88 -- south-south connection, illegal weapon exchanges & its restriction

# 9 - South-South Connections

## Hatzky - Cuban Teachers in Angola

* traces the military and civil aid operations of Cuba in Angola, from an ideological standpoint of furthering the socialist mission and its transformation into more pragmatically-based aid efforts which would constitute large part of Cuba's coming international cooperations


* Castro had to convince the Cuban populace to support the development (and aid) efforts in Angola, invoking 'blood relationships' (referring back to trans-Atlantic slave trade) and common colonial past (72)
  * invokes moral duty of Cubans to support Angola's independence as internationalists (also symbolically underlining, e.g. calling first military intervention 1975/6 'Operation Carlota') after African slave leading Cuban uprising 1843 (76)
* (Hatzky's previous research) highlighted the *active* role of the MPLA (Movimento Popular de Libertaçáo de Angola) as driving force of development (where other research mostly saw uni-directional aid from Cuba) (73)
* MPLA's situation on gaining independence: fighting rival liberation movements (FNLA, UNITA) and South African military intervention; needed (and expected) Cuban military support and ultimately comprehensive help to build new socialist society (74)
* July 1976 the two states signed framework for cooperation of civil reconstruction and further cooperation; began moving initially politically, ideologically motivated solidarity project into socialist development aid project (75)
* "post-colonial Angolan educational system was not a simple adoption of the Cuban system [but] the result of intensive [...] discussions between Cuban and Angolan specialists. [...] Ultimately the Angolans decided about the structures, contents and methods they wanted to implement." (77)
* Cuban teachers can be seen as agents of political, social, cultural change which also represented aims of MPLA, working essentially as 'representatives' of the MPLA government (78)
  * Cuban teachers put into practice new methods of teaching, developed in Cuba since 60s; encompassing all elements of socialist instruction to work "towards integration into a socialist society." (80)
  * engagement of Cuban teachers extended beyond the classroom to organizing leisure activities and institutionalized homework supervision (81)
* pragmatic to positive attitude toward teachers by Angolans (if not them, then no teachers at all), but clearly reflected on as agents of socialist rule (82)
* Cubans seen by some as "an important, powerful people" due to sending military and schooling, assumed underlying power hierarchy by some students (83)
  * generation educated by Cuban teachers are now ruling country -- most are middle/upper class, working as politicians, teachers, priests, businessmen (86)
* in Cuba, organizing this civilian aid cooperation on that scale "led to a professionalization and reorientation of all Cuban civil cooperations undertaken after 1991" (86)
  * this proved useful to Cuba after 1990, when they could provide, separated from ideological foundations, civilian aid operations as an important source of income (86)
* though relations strained after '91 withdrawal of Cuban troops and abandoning of socialism by Angola, since '07 new civil cooperations; now based on pragmatism instead of ideology (87)

## Monson - Africa's Freedom Railway

* the "Freedom Railway" between Tanzania and Zambia, supported by Chinese development efforts became a symbol of clashing ideologies and approaches to development and modernization, while never being completely enveloped within its ideological ambitions. It also provided an entirely new spatial configuration for the region and was a project of connection and contestations at multiple levels, of multiple countries and ideologies, which ultimately left it carrying a multiplicity of meanings and symbolic value -- as well as the very real economic value on the ground.

### Building and Ideology

* China supported building of Tazara ("Freedown Railway"), becoming "symbol of revolutionary solidarity and resistance to the forces of colonialism, neo-colonialism, and imperialism" (2)
* in Tanzania simultaneously was built a railway (funded through China) and a road (funded by the US) linking Dar es Salaam and Lusaka (1)
* came to clash between the two which symbolized struggle of two different ideologies
* read as state-lead, state-funded railway project (which was easier to dictate through state management)
* versus road symbolizing open market and individual entrepeneurial side-projects
  * in the end, the highway (finished 2yrs earlier) was used to ferry construction equipment for the railway, and tried, by the state, to be controlled and regulated for the transport industry; and the railway was used also by small-scale trade and entrepeneurs, so both supported the other ideology in a way (3)
* China building road was contradictory identity: termed as "the poor helping the poor", symbolizing common struggle against imperialism (acting as former colonized subject); but also being China's largest international infrastructure project, projecting its socialist influence (acting as Cold War player) (3-4)
* the railway also symbolizing pan-African ideas: bi-national governing authority (Tanzania&Zambia), English as official language, division of labor and management between the countries (4)
* railway became important nexus of connecting rural communities and outside world through travel and provided services and train itself in turn depended on agency of the (rural) users (4)

### Uses and Struggles

* especially "the Ordinary Train" (passenger train) became important for local communities
* small-scale business began using passenger train to ship goods with (in many parcels) (5)
* in the pursuit of efficiency to enable liberalization of route stations not serviced often were supposed to be closed down (5)
  * rural individuals & committees began protesting, re-using much of the socialist language instilled during the original building process, becoming active participants in shaping its development (5)
* Railway project, in building and use, was styled not as colonial encroachment, but as operating *alongside* African individuals, and interacting with them on a level (6-7)
  * as in Vietnamese case [@Schwenkel2014], relationships were actually hierarchical and highly regulated, but styled as egalitarian brotherhood, and often fondly remembered (7)
* in some ways replicates the colonial patriarchal model of helping local people 'come-of-age', "entry into a modern, masculine adulthood through wage labor and a disciplined work regime" (7)

### The Meaning of the Railway

* constituted "a new spatial orientation for agrarian production and rural commerce" (8)
* displays balance of commitment and contestation: (9-10)
  * planners as provision suppliers, producers, but also eyes of local security force
  * teaching by example instilling new skills, but also many leaving/fired due to harsh working conditions
  * hard work and discipline instilled in project, mirroring both post-colonial modernity and progress, but also conditions and ideas of previous colonial projects
  * opening of railway meaning possibility and connection for local ruralities, but also displacement and break of tradition (and destruction of environment)
  * providing social/economic mobility but also undermining sense of physical belonging
* shows how development projects sometimes took unexpected avenues, shaped by local participation and outer constraints in attempting to reach their goal, as well as how their meaning changes depending on who is asked (9)

## Bockman - Socialist Globalization against Capitalist Neocolonialism

* while UNCTAD and the Non-Aligned Movement sought to create a socialist global economy, built on solidarity and multilateral relations, the long-lasting debt crisis of the 1980s allowed US-backed institutions to take over the globalization project and replace the original ambition with one of neoliberal restructuring and a reaffirmation of neocolonial bilateral relations.

* UNCTAD = United Nations Conference on Trade and Development, socialist in its economic globalization approach
* Non-Aligned Movement and socialist world (i.e. Second and Third World) sought a break with colonial world economies and bilateral colonial relations, furthered by the UNCTAD economic agenda (121)
* They supplanted those with economic connections between countries that emphasized cooperation, solidarity, building the economy through free trade, structural adjustment, export-oriented production, market expansion, financial flows. (121)
* this approach faced resistance by primarily US-backed insitutions: GATT, World Bank, IMF -- and created phenomena like Americanization, neocolonialism, neoliberal capitalism, which are "in fact, not particularly global" (121)
* in "crisis of inclusion", expanded participation of Global South in production, trade, finance, consumption, increased interconnectivity -- together with lasting deb-crisis -- allowed neocolonial appropriation of this emergent economy (121)
* it was radically reinterpreted along neoliberal ideas, so that e.g. structural adjustments now primarily targeted poorer countries instead of developed countries, as was the original goal (121)
* in this change, increasing ownership by US implemented, instead of UNCTAD's socialist vision of interconnected economy, a reassertion of "Rostow's vision of modernization as a linear path of isolated countries within a slowly evolving neocolonial system." (122)
* in its wake came a turn from multilateral global connections and south-south collective self-reliance to return to north-south bilateral agreements (122)
* So Bockman, "in such ways, the long-lasting debt crisis resulted in deglobalization and the reassertion of the colonial economies in a new form." (122)

### References

[@Hatzky2011]

[@Monson2011]

[@Bockman2015]

# 10 - International Organizations

## ILO

* origins & history
  * 1919 Paris peace conference (before UN)
  * to define & promote just / non-discriminatory labour standards
  * as bulwark against rising 'communist threat'
  * transformation
    * first focus on western nations
    * 1944/45 transformation after Philadelphia Declaration (- technical assistance) (T)
    * 53 expanded focus on operating within 'Third-World' (T)
    * 1960s: decolonization and integration of newly independent countries as another actual big break of the working of the ILO itself
    * resistance to actual implementation in 60s - more and more concessions encroached into mission
    * 1969 - The begin of World Employment Program (approaching poverty as issue of international community)
* mission
  * every notion based on universal adherence to human rights (everyone on equal footing)
  * promotion of international labour standards as the foundation to achieving fundamental equality - definition of standards
* projects / activites
  * drafting & adoption of labour standards
  * pursued through non-binding, persuasive measures
  * adaption of mission (to enable continued relevancy?) into active development process
  * training (bottom-up)
  * integrated approach - based on social dialogue for promotion of labour
  * problems of integrating the standard setting into the actual implementation of development efforts
* members
  * 1947: 55 member states; 1965: 115
  * nationalization of de-colonized states > many new members
  * ILO also very much based on the acting of individual members
* structure
  * tripartite: government representatives / non-state actors (trade unions/workers' unions)
  * non-state / state actor composition
* financing & budget
  * budget came from individual member nations
  * > beginning very constrained budget, targeted operations
  * ('58) budget increases, mostly US-based > technical assistance
  * '74 > US leaves

## FAO

* promotion of mainly rural development (farming et)
* global standards regarding food safety, additives
* e.g. fishing communities in Angola (bottom-up?)
* agricultural knowledge transfer
* powerful actors regarding discourse
* based in Rome, the country may eventually reflect itself in the shaping of *inter*-national knowledge ordering
* SU founding member of FAO but never actively participating

## WHO

* 'freedom' through global health
* 1960s increased focus on population growth and family planning
* health as stabilizing factor and thus 'not fall to communist pressures'
* discussion of how development itself should function - what it should not be able to touch instead
* funded by member states
* development in connection with health of a state as fundamental need
* text: case study of Malaria problem in 1950s - 1960s
  * no Soviet Union in WHO - so moment of showing how capitalist system can 'help'
  * eradicating diseases through western systems
  * Socialist system: instead much more concerned with primary health needs of individuals and the society
  * Capitalist system: more targeted at eradicating issues like malaria etc - also to increase economic output; increase acreage of agricultural use
* integration of inter-war experts, then driving development of WHO itself

# International Labour Organization

## ILO Meeting Draft 2019-11-14

### Abbreviations used

ILO - International Labour Organization
WHO - World Health Organization
FAO - Food & Agriculture Organization
IOM - International Organization for Migration
UNDP - United Nations Development Programme
Rockefeller
UNCTAD - United Nations Conference on Trade and Development

### Our Research goals

* Current ambitions/goals - general framework of operation
* Major actors (within or outside)
* Structural makeup of ILO itself (which layers / who becomes decision-maker)
  * tripartite structure
  * annual conference / governing body / the office
* External entanglements - which institutions cooperate/ are in conflict with ILO
  t esp regarding WHO / FAO / IOM / UNDP / UNCTAD? / Rockefeller?

* ideally trace where these formations and structures come from historically
  * both internally - the initial constitution 1919; philadelphia declaration '49; principles and rights at work '98

### Research: Entry Points

* Rodgers, Lee, Swepston, Daele 2009 - The ILO and quest for social justice: 1919-2009 -- structural formation
  * take longer-form approach but very much only look at institution itself
  * appendices of the declarations / key source documents of its formation
* Antony Alcock 70s - History of ILO  -- idea history
  * extensively sourced, but very much product of its time
  * but it takes a long-term period into account in tracing the various origins of ILO (and the idea history of workers' rights/soc. just.)
* Maul, Daniel 2012 - Human Rights, Development, and Decolonization: The ILO 1940-70 -- developer/developed cleavages&entanglements
  * draws parallels between internal formation of structures and external influences
  * (attempts to) gives voice to those affected by the policies adopted
* Sandrine Kott, Joëlle Droux 2013 - Globalizing Social Rights: The ILO and beyond
  * scholarly work, but co-published BY ILO, so keep in mind

### Research: Signposts

* [Klaas Dykmann reviews Routledge History of International Organizations](https://www.connections.clio-online.net/searching/id/reb-12618?title=b-reinalda-hrsg-routledge-history-of-international-organizations&q=international%20labour%20organization&sort=newestPublished&fq=clio_contentTypeRelated_m_Text:%22ebc%22&total=8&recno=8&subType=reb) - Chapter on ILO, may flesh out smaller narratives
* [Klaas Dykman review: Unpacking International Organizations](https://www.connections.clio-online.net/searching/id/reb-15401?title=j-trondal-u-a-unpacking-international-organisations&q=international%20labour%20organization&sort=newestPublished&fq=clio_contentTypeRelated_m_Text:%22ebc%22&total=8&recno=7&subType=reb) - going beyond structuralism and the national/global agenda divide

### ILO Primary Actions

* Conventions
* Protocols
* Recommendations

### ILO Areas of Interest

Rodgers et al:

* Human Rights and Workers' Rights
* Working conditions
* Social Protection & a 'fair' globalization
* Poverty and Unemployment

WP:

* Forced Labor
* Child Labor
* Minimum Wage Law
* HIV workplace policies
* Migrant Workers/Domestic Workers
* Globalization (and work opportunities)
* "future of work"

### Questions

* how will the final session be structured?
  * will there be a specific scenario and we have to deal with it?
  * what time-frame did we end up specifying?
* this research, it should not *only* end up being targeted at the conference, right?
  * there would be different problems to look at, different questions to ask
  * if only for the conference we'd look at current transaction strategies and institutional pressure points, tensions & conflicts
  * otherwise we would extend the look at the historical heritage or break points from old paradigms - in other words contextualize
* in connection with that: we are doing this from a scholarly standpoint (i.e. our own) - not a policy recommendation pov?

## ILO - International Labour Organization for the Final Conference

### Preliminary Research

#### Structure and Approach

* *founding & transformation*
  * foundation and relation to League of Nations / new multilateral order
    contrast internal projects of legitimization (esp 1920-30s) to external analysis (~70s)
    * [@Kott2018] Sandrine Kott (2018) 'Towards a social history of international organisations: the ILO and the internationalisation of western social expertise (1919-1949)' in Miguel Bandeira Jerónimo & José Pedro Monteiro (Eds.) *Internationalism, Imperialism, and the Formation of the Contemporary World: The Pasts of the Present*, : Palgrave MacMillan, pp. 33--58
  * reformation into UN institution after WWII
    papers questioning state-allegiance vs NGO status / new positioning alongside US/GB in intl networks
    * [Kratochwil1986] F. Kratochwil and J. Ruggie, ‘‘International Organization: A State of the Art on an Art of the State’’, International Organization, 40 (1986), p. 755.
    * [Haas1964] E. Haas, Beyond the Nation-State: Functionalism and International Organization (Stanford, CA, 1964).
    * [@Maul2012] Maul, D. (2012) *Human rights, development and decolonization: the international labour organization, 1940-70*, : Palgrave Macmillan UK, p. 86-120.
  * neoliberal change / washington consensus
    follow dwindling action potential / agency - new methods employed by actors? new alignments
    importance of US withdrawing
    * ?? [Cox1977] R. Cox, ‘‘Labor and Hegemony’’, International Organization, 31 (1977), pp. 385–424.
* *mission & corresponding programs*
 [@Kott2013] Sandrine Kott & Joëlle Droux (2013) *Globalizing social rights: the international labour organization and beyond*, Houndmills, Basingstoke, Hampshire New York: Palgrave Macmillan UK
  * *types of projects & activities*
* *internal structure & decisionmaking*
  importance of tripartite structure & its stable nature; the primary actors/zones of conflict;
    * tripartite structure still under-analyzed, focus on trade unions almost nothing on employers [see @Daele2008, 510]
  ILO as SITE instead of actor itself
  * minutes/conference archives will be useful for discourse analysis (e.g. minutes below)
  * [@Kott2018] Sandrine Kott (2018) 'Towards a social history of international organisations: the ILO and the internationalisation of western social expertise (1919-1949)' in Miguel Bandeira Jerónimo & José Pedro Monteiro (Eds.) *Internationalism, Imperialism, and the Formation of the Contemporary World: The Pasts of the Present*, : Palgrave MacMillan, pp. 33--58
  * *members of org, key actors in staff*
    * memoirs of director general: e.g. Albert Thomas vs Bob Hawke in decision-making
* *position of the org in international network of development orgs*
  * [@Kott2016] Sandrine Kott (2016) 'Cold war internationalism' in Glenda Sluga & Patricia Clavin (Eds.) *Internationalisms: A Twentieth-Century History*, Cambridge: Cambridge University Press, pp. 340--362
  * [@Maul2012] Maul, D. (2012) *Human rights, development and decolonization: the international labour organization, 1940-70*, : Palgrave Macmillan UK

#### Potential Research Designs

* recommendations and enforcement
  * The state-sponsoring of ILO and the possibility of working within specific countries
  * Money and the possibility for development efforts: Leimgruber in [@Kott2013]
* Crisis of the welfare state and ILO's internal reconfigurations during the 80s
  * [@Maul2012] Ch7 - technical assistance & north-south conflict
  * technical assistance programs - [@Maul2012, pp. 121-] transformations from 70s-80s
  * crisis of the welfare state as removing the ground to work on
* [Maul2019] PartIV on shifting ground:
  * new leader ship, new actors and how it reflects in changing approach to 'efforts'
  * [Maul2019] actors and their different leadership methods
  * changing tripartite structures - are the people changing that can actually change the

#### Sources Todo-List

potential sources:

* ILO structure
  * [x] quick rundown - comparing it with WTO and hard/soft application of laws [@Staiger2003, 287]
  * [x] overview of ILO operating procedure and historical baggage (tripartite, anti-communism, proceduralism) [@Langille2016, 477]
  * [ ] detailed operation and law adherence, *how* things are done and in *what order* [@Servais2017]
* Angola
  * [x] Angolan (almost) post-war economy change -- analyzed mostly from above, in an effiency based manner [@Munslow1999]
  * [ ] The move towards 'democratization' of Angola questioned, as skimming of profits by elites [@Kibble2006]
  * [ ] labor patterns (manual construction) report, [@Benach2007]
  * [ ] WHO labor landscape Angola report [@Oya2019]
* ILO operations in the 80s
  * [ ] operation (and issues) in Argentine around same time [@Daele2010, 401ff, 423ff]
  * [ ] operation supporting Solidarity Movement in Poland ~1980s [@Standing2008]

### ILO Basic Information

* formed in 1919, one of (if not *the* oldest continuously operating IO)
* headquartered in Gevena, offices (nowadays) in >40 countries
* generally, reliance of soft law and persuasive measures for generating action (standards)
  * few binding instruments, unlike e.g. WTO's dispute settlements [@Maupain2013]
* ideas:
  * founded on principle of 'social dialogue between social partners in labor regulation', committing to the contestability of issues in labor
  * in favor of regulation as principle of satisfying contestability (historically as instrument of bulwark against radical worker mobilization, i.e. Communist threat 1919)
  * "an agency 'created of the capitalist system,' rather than an agency 'for the transformation of capitalism'", representing 'embedded liberalism' strain of development to counter-act Soviet model [see @Standing2008]
* structure:
  * tripartite structure: three-way split between state governments and 2 nongovernmental representatives: workers and employers
  * voting and consensus is handled on a 2:1:1, so 50% state actors, 50% non-state actors
  * non-state representatives are elected by the respective member states' unions and employer organizations
* procedure: [see @Langille2016, 477]
  * annual conferences (international convening parliament); out of which 'international labor standards' derive; which together create (by now ~200) international treaties called Conventions
  * when convention is ratified by a member state, it becomes 'binding' ILO law; and is usually (but not always!) adopted into domestic law
  * ILO contains a 'supervisory system' which reports on individual member states' adoption and application of the standards, supervises any complaints, and provides *support* [ILO Supervisory System/Mechanisms on ILO.org](https://www.ilo.org/global/about-the-ilo/how-the-ilo-works/ilo-supervisory-system-mechanism/lang--en/index.htm)
  * support consists of: technical assistance, providing labor training / administrative training and resources; assistance in drafting domestic laws
* challenges:
  * especially 1989 thus watershed moment for ILO, opened 'contestability' debate further (with opposition to Communism now removed), and firmly embedded market primacy into global economy; infusing many standard-setting procedures with new incoherence [@Langille2016, 479]
  * 'contestability' is *still* issue of stand-still, often re-framed as avoiding 'race to the bottom' dilemma in major debates
  * persuasion vs coercion: the question of how 'hard' labor standards violations should be punished is still not conclusively decided
  * tripartite crisis: especially in 50s, 70s, and from 2000s, often claimed as ineffective (or, variously, non-existent) functioning of tripartite; with ILO falling back into inter-governmental agency voting 'over' non governmental actors
  * missing 'narrative': with adoption of 1998 Declaration, local context is put over universal legal rules; following arguments often argue over need of new overarching narrative of ILO [@Langille2016, 481]

### Internal Structure of the ILO

from [@Staiger2003, 287ff.]

* basic credo: "to improve the working and living conditions of workers everywhere" [Morse1969, 96]
  * "social justice", contained in ILO Constitution Preamble symbolize its main objective [Johnston1970, 13]
* but disagreement over means: [Morse1969, 82-83]
  * standard-setting organization to defend rights of workers against exploitation in drive for growth, development, and industrialization
  * operational organization, which should concentrate efforts on promoting economic development
  * forum for tripartite discussion
  * assisting in training of labor force
  * unofficial, since conception, competitive effects on markets are also on mind of ILO creation [see @Staiger2003, 288]
* [Johnston1970, 88] sees 3 main purposes:
  * standard setting
  * information dissemination
  * technical assistance
* tripartite structure:
  * basic explanation: [@Langille2016, 477-483]
  * procedure explanation: [@Servais2017]
  * Jeffrey Harrod ciritical assessment in ILO:Past and Present conference 2007
  * Alcock 196ff explains the structure and its difficulty in adapting to post-war climate
  * creation of regional, field and liaison offices 1969 shortly explained [@Alcock1971, 240]
  * crisis of tripartite structure ~1955 [@Alcock1971, 300-305]
  * [@Maul2012, 278], new problems for the tripartite structure 60s+

### ILO and Angola

#### General Situation 2003, after (actual) end Civil War

* situation in Angola
  * 2003: no access to drinking water (60%), medical care (80%), high child death rate (30%), 100.000 children separated from parents
  * rural flight into urban locations
  * remaining landmines (and no government provisions for their casualties / victims); demining operations by HALO Trust, with help form foreign experts
  * *huge* amount of (internally) displaced persons
  * child soldiers and child marriages, UNITA esp. but not exclusively
* phrasing of position
  * TODO, see [@Standing2008] souruces?

#### Process toward End of War

* 22 Feb 2003, KIA of UNITA leader Jonas Savimbi (and directly succeeding leader 20 days later)
* unveiling of 15-point peace agenda by Angolan Armed Forces 13 March
  * called for cessation of all military offensives by FAA at midnight
  * ad-hoc meetings of field commanders to institute ceasefire
  * de-militarization and *reintegration* of UNITA troops
  * general amnesty for troops, to further reconciliation
* Memorandum of Understanding 4 April
  * not complete peace accord yet, but initiation of peace process and implementation of remaining issues from Lusaka Protocol
* technical group composed of FAA and UNITA members lead quartering and demilitarization by 15 Jun
  * quartering into 36 quartering areas for ~85,000 UNITA military personnel and ~240,000 family members
  * gov pledged reintegration:
    > "The international community will need to help with the integration of former combatants, including support for vocational training, employment-generation activities and reconciliation programmes. Apart from technical and managerial expertise and advice, the United Nations can offer its good offices to help prevent or resolve conflicts that may arise." [@UNSCRep2002, 3]
    > "[international organizations] have agreed on a common approach aimed at stabilizing the conditions of the most acutely affected populations, while simulateneously undertaking community-based initiatives to facilitate the return, resettlement and reintegration of displaced populations." [@UNSCRep2002, 6]

#### Labor conditions 1950-2000

* commodified labor force 1960 92percent male, women populating reciprocal sphere [@Vos2014, 380]
  > husbands and sonds, working as independent farmers or as paid employees of European planters and businessmen, were officially recognized as income-earners, whereas the labor of their wives and daughters was generally considered as a non-remunerated contribution to the maintenance of the household.
* from 2000 collapse of formal economy, thus wage labor; formal sector employment *only* increased within state (i.e. public administration, police, armed forces, state-owned companies)
* private sector rapid expansion of informal economy (esp urban due to displacement): "small businessmen employing small numbers of wage laborers and [...] self-employed workers, mostly trying to scrape an income together from petty trade." [@Vos2014, 381]
* oil revenues hinder commercial agriculture, many peasants thrown back to subsistence farming; distribution of "large plots of fertile land [...] to clients of the government, constituting a new class of big landowners (*fazendeiros*)" [@Vos2014, 381]
* largely support through informal economy, even public servants often used private sector activities to supplement dwindling real wages; same procedure "stimulated the self-employment of women in the informal sector." [Vos2014, 382]
* great degree of informality in (esp Chinese sub-contractor) low-skill construction worker & factory sites (16% with contract, 23% semi-skilled with contract); *but* most were informed verbally of wage expectations, working hours, leave policy, benefits [@Oya2019, 37]
* however, combination of (low but stable) cash wage and 'social wage' (living in accommodations by Chinese firm and food supplied) allowed some saving of money and intermittent escape from poverty trap [@Oya2019, 43-44]
* unionization, training, skill development ?? [@Oya2019, 47-50]

## Position Paper

Structure

* Preamble
  * problems of the country, reflected by "major sources of insecurity and anxiety, the answers are: civil disorder, armed violence, instability of governance system too often caused by unemployment, inequalities, poverty, discrimination and lack of participation."
  * these things [verändert für uns] led the people of these countries into too many years of sufferung from such sources of individual and collective insecurity.
  * The people of Angola do not deserve such continued conditions of deprivation and struggle, for they are a region blessed with an abundance of [human resources] and [other positive aspects]
  * Indeed, through thinking and acting collectively and in unison, we can address and overcome the pressing challenges facing the country of Angola at this moment into a future of stability and prosperity.
* Concrete Struggles
  * little access to drinking water, medicine, basic human needs being seen to -> [other agencies] we all need to see to the immediate improvement of this situation
* immediate objectives
  * creating wage employment opportunities for youth, for women, and displaced persons through [equality] use of local resources, labour-intensive infrastructural development to [erschaffen] a a system against vulnerabilities and with social protections for those who need it most
  * enabling social dialogue for labor through reintegration of erstwhile combatants who became displaced in the struggle, through vocational training, targeted training and administrative assistance, as well as an emphasis on strengthening unionizing possibilities for the [common worker]
  * preventing child labor, forced labor and positions of inadequate remuneration based on exploitation of status, gender or other fundamental []

* wage labor / better labor remuneration
  - Informal Labour mainly women
  - 1960s: 92% men waged labour - continuously going down over time
* Agrar
  - Private sector: agrar zusammengebrochen,
  * because most money exported through oil and diamonds
  - Land mines complicate work on fields --> famine, poverty
  - Cities overpopulated - high unemployment and informal labour
- Child labour (study on application of ILO conventions against child labour (portuguese)
  - Forced labour in diamond
- 2005: land grabbing (distribution of arable land to political and economic elites)
- Deep in 2000s: not many unions existing
- Reintegration of soldiers, vocational training
* Displaced persons:

## Resources

* [internal document server](https://www.ilo.org/wcmsp5/idcplg?IdcService=GET_DOC_PAGE&Action=GetTemplatePage&Page=HOME_PAGE)
* [ILO Minutes meeting 336](https://www.ilo.org/wcmsp5/groups/public/---ed_norm/---relconf/documents/meetingdocument/wcms_713459.pdf)
* [ILO Labordoc - Reports on Programme, budget and possibility & Recommendations and Enforcement & Director-Generals](https://libguides.ilo.org/documentation/ILC)
  * highlights diverging aims of different tripartite actors; & methods


## Documents on Angola

[Many Documents](https://www.ilo.org/africa/countries-covered/angola/lang--en/index.htm)
[Compiled Ratifications for Angola](https://www.ilo.org/dyn/normlex/en/f?p=NORMLEXPUB:11200:0::NO::P11200_INSTRUMENT_SORT,P11200_COUNTRY_ID:2,102999#Occupational_safety_and_health)
[Major Challenges to South African Labor Laws; *not* Angola specific](https://www.ilo.org/inst/publication/discussion-papers/WCMS_193513/lang--en/index.htm)

[Compiled Studies for Child Labor in Angola, based on IPEC](https://www.ilo.org/ipec/Regionsandcountries/Africa/angola/lang--en/index.htm)
[Study on Efficiency of ChildLabor Prevention Conventions (No. 138, 182), including Angola (PORTUGUESE)](https://www.ilo.org/ipec/Informationresources/WCMS_222484/lang--en/index.htm)

[Labor Inspection Profile Angola, i.e. enforcement](https://www.ilo.org/global/topics/safety-and-health-at-work/country-profiles/africa/angola/WCMS_151303/lang--en/index.htm)

("Angola has ratified Convention No. 81 in 1976.")
specific laws for "organization and functional composition"
* General Labour Law No. 2/00 of 11 February 2000
* Decree No. 31/94 of 5 August 1994
* Law on Inspection and Labour Administration No. 2/92 of 17 January 1992
* Internal regulations of the General Inspectorate of Labour, Decree No. 9/95 of 21 April 1995.

[OSHA Profile Angola, i.e. legislative](https://www.ilo.org/safework/countries/africa/angola/lang--en/index.htm)

### Angles for Angola

* general:
  * adoption of safety procedures for work in nuclear sector > Esp interesting a reformulation of *Industrial* Labor Inspection Convention
  * child-labor prevention adoption for this new sector
  * (contemporary?) gender-based promotion of work
* issue-based:
  * counteracting (casualization/informalization/externalization) - 'non-standard' and changing work patterns
  * lack of assets / capacities for *inspectors* to do their work; for safety & labor standards and laws (see Major Challenges)


## Van Daele - The ILO in Past and Present Research

### Why study the ILO

* 'trendsetter' among international organizations (485)
  * in standard-setting (by means of conventions/recommendations)
  * in technical cooperation and international expertise on labor
* unique tripartite structure (485-86)
  * representatives from government, employers, trade unions
  * seems stable in that it remained unchanged to today
* *oldest* international organization
  * founded in 1919 as part of League of Nations
  * only institution that has unbroken history since then

* analysis of ILO shifts with 3 surrounding transformations (486)
  * transformations within the ILO as an institution
  * transformations in intl political, economic, social context
  * transformations in the scientific disciplines (es. labor history, intl relations)
* from 2 angles (487)
  * 'inside' - by IL Office, Secretariat in Geneva, (former) ILO officials, often to justify its work
  * 'outside' - by academics in independent scientific position

* general hypothesis (486-87)
  * interest in international organizations is related to general importance placed on multilateral organizations and belief of effectiveness of international cooperation
  * aksing how 'global' research on ILO has been

### Literature overview: 1920s legitimization project

* early literature much more punctuated by 'inside' perspective - first-generation products of 1920s (488-89)
  * esp geared towards ILO director Albert Thomas
  * from international law / political science perspective in broader framework of international relations
  * outside literature still closely aligned to inside perspectives
  * also generally optimistic view, overview of policy methods etc (490)
  * EU centric, with understanding of 'club of like-minded states'; exception being India actively participating in orga (491)
  * "historical writing in the 1920s can be defined as an instrument of self-justification", as a legitimizing concept for the organization (491)

sources;

* self-legitimization: A. Thomas, ‘‘The International Labour Organization: Its Origins, Development and Future’’, International Labour Review [hereafter ILR], 1 (1921); this article was reprinted in ILR, 135 (1996), pp. 261–276, 263.
* connection with League of NAtions: George Nicoll Barnes (1859–1940), former trade-union Secretary, member of the Labour Party, and leader of the British delegation at the Paris Peace Conference in 1919; G.N. Barnes, History of the International Labour Office (London, 1926). See also his autobiography: idem, From Workshop to War Cabinet (London, 1923).
* ILO and eurocentrism (its self-understanding): K.N. Dahl, ‘‘The Role of ILO Standards Policy in the Global Integration Process’’, Journal of Peace Research, 5 (1968), p. 321.

### 1930s- 45

* generally continuing on from previous decade but different international context: mass unemployment, nationalization, economic crisis (492)
* fewer ratifications of ILO conventions; writings in defense of ILO (492)
* Albert Thomas death and biographical literature in same (defensive) vein (492)
* shift of ILO from dealing "with the social effects rather than with the causes of existing economic conditions" (493)
  * begins actively advocating "measures of monetary and credit policy, international trade, and public works" (493)
* debates center around state-connections and autonomy of ILO (esp after entry of US into ILO 1934)
  * much literature in front of New Deal measures of US
* in/after WWII, while analyses remained institutional, taking stock of past achievements & challenges major theme (495)
  * repositioning alongside US and GB to reconfirm its new UN umbrella

sources:

* questioning law-centricity and state-closeness from AFL: M. Woll, ‘‘The International Labour Office: A Criticism’’, Current History, 31 (1930), pp. 683–689. Matthew Woll was vice-president of the AFL.
* Shotwell with source-rich overview of funding of ILO: J.T. Shotwell, The Origins of the International Labour Organization (New York, 1934), 2 vols, pp. xxix–xxx.

### (late) 1940s - (mid) 1970s

* topics stayed on (internal) literature of 'self-identity', now in context of postwar international relations (496)
  * popular topics also how ILO remained in existence after WWII, and how its internal structures evolved
* Nobel Peace Price for 50th anniversary lead to explosion of historical reviews (496)
  * this is where Alcock was commissioned for a history (chronological, embedded in international historical context, as opposed to mostly internal institutional)
* 1950s and esp 1960s start academic review (critique/analysis) of ILO (497-98)
  * professionalization of the field - esp in historiography
  * from institutional aspects to actual decision=making process within ILO - "opening the black box"
  * esp Cox and Haas with major empirical research (499)
* increasing discrepancy between original constitutional design and daily practice becomes focus of analyses (500)

sources:

*  analysis of patterns shaping ILO outcomes: F. Kratochwil and J. Ruggie, ‘‘International Organization: A State of the Art on an Art of the State’’, International Organization, 40 (1986), p. 755.
* analysis of supervision of international standards: E.A. Landy, The Effectiveness of International Supervision: Thirty Years of ILO Experience (London [etc.], 1966); idem, ‘‘The Effectiveness of International Labour Standards: Possibilities and Performance’’, ILR, 101 (1970), pp. 555–604.
* analysis of tripartite structure:  T. Landelius, Workers, Employers and Governments: A Comparative Study of Delegations and Groups at the International Labour Conference, 1919–1964 (Stockholm, 1965); A. Suviranta, The Role of the Member State in the Unification Work of the International Labour Organization (Helsinki, 1966).
* **ILO as institutionalized interest politics**:  E. Haas, Beyond the Nation-State: Functionalism and International Organization (Stanford, CA, 1964).
* ILO as its own political system: R. Cox, ‘‘ILO: Limited Monarchy’’, in R. Cox, H. Jacobson, and G. Curzon (eds), The Anatomy of Influence: Decision Making in International Organization (New Haven, CT [etc.], 1973), pp. 102–138.

### 1970s - 1980s

* politicization and less autonomy of ILO due to cold war rifts (external) & quick changing Directors-General (internal) (502)
  * withdrawal of US from ILO over condemnation of Israel & admittance of PLO -> budget cuts (502)
* neo-liberal Washington consensus turn further immobilizes ILO (502)
  * can't play active role due to members' political divisions
  * liberalization & deregulation let social dialogue & social security/welfare take a backseat

sources:

* gramscian approach to intl-orgs as hegemonic power vehicle: R. Cox, ‘‘Labor and Hegemony’’, International Organization, 31 (1977), pp. 385–424.

### 1990s -

* renewed interest due to
  * nation-states looking to participate in multilateral structures (504)
  * changes in international society/global political order renewed role of international orgs (504)
* new approaches: constructivist gender, decolonization, human rights, indigenous/forced labor, epistemic communities, intellectuals/international expertise, social security, construction of welfare regimes, child labor, transnational NGO networks, ..

sources:

* historical perspective on new 'global' meaning of ILO: M. Nieves Roldan-Confesor, ‘‘Labour Relations and the ILO Core Labour Standards’’, in M. van der Linden and T. Koh (eds), Labour Relations in Asia and Europe (Singapore, 2000), pp. 19–52.
* multitude of sources 504-505 - different dimensions, no clear guide (article itself from '08)
* e.g. S. Kott, ‘‘Arbeit. Ein transnationales Objekt? Die Frage der Zwangsarbeit im ‘Jahrzehnt der Menschenrechte’’’, in C. Benninghaus et al. (eds), Unterwegs in Europa. Beiträge zu einer pluralen europäischen Geschichte (Frankfurt, 2008, forthcoming).


### References

Jasmien Van Daele (2008) 'The international labour organization (ILO) in past and present research', *International Review of Social History* 53, 485--511

## Kott - The ILO and the Internationalisation of Western Social Expertise

* argues for international organizations as "not so much *actors* in global governance as they are *sites* of internationalisation" (34)
* argues for using archival documents instead of official publications (35)
* 4 points: (35)
  * analyze ILO as site of coalescing NGO liberal West European networks
  * national/international dialectic of developing intl expertise
  * 'brown internationalism' and its conflicts with liberal internationalism
  * export and re-appropriation of liberal social normativity in peripheral spaces

sources:

* esp idea culture in institutions: Jasmien Van Daele, “Engineering Social Peace: Networks, Ideas, and the Founding of the International Labour Organization”, International 52 S. Kott Review for Social History 50 (2005), pp. 435–466; Madeleine Herren, Internationale Sozialpolitik vor dem Ersten Weltkrieg. Die Anfänge europäischer Kooperation aus der Sicht Frankreichs (Berlin: Duncker & Humboldt, 1993); Sandrine Kott, “From Transnational Reformist Network to International Organization: the International Association for Labour Legislation and the International Labour Organization (1900–1930s)”, in Shaping the Transnational Sphere, eds. Davide Rodogno, Bernhard Struck, and Jakob Vogel (New York: Berghahn Books, 2014), pp. 383–418.

\pagebreak

# Bibliography
